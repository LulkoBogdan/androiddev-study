package com.example.mydatabase.app;

import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

public class MainActivity extends ActionBarActivity implements FragmentStart.StartListener,
        FragmentTest.TestListener{

    public static final String DATABASE_GOODS = "db_goods";
    public static final String DATABASE_SELLERS = "db_sellers";
    public static final String DATABASE_BUYERS = "db_buyers";
    public static final String DATABASE_ORDERS = "db_orders";
    public static final int DATABASE_VERSION = 1;
    public static final String CREATE_DB_GOODS = "create table db_goods (_id integer primary key" +
            " autoincrement, name text, price float, availability boolean);";
    public static final String CREATE_DB_SELLERS = "create table db_sellers (_id integer primary " +
            "key autoincrement, name text not null, post text not null, salary float not null," +
            " experience integer not null);";
    public static final String CREATE_DB_BUYERS = "create table db_buyers (_id integer primary " +
            "key autoincrement, name text not null, wholesale boolean not null, age integer not" +
            " null, discount integer not null);";
    public static final String CREATE_DB_ORDERS = "create table db_orders (_id integer primary" +
            " key autoincrement, name text not null, amount integer not null, delivered boolean" +
            " not null, address text not null, date text not null);";

    DBHelper dbGoodsHelper;
    DBHelper dbSellersHelper;
    DBHelper dbBuyersHelper;
    DBHelper dbOrdersHelper;
    FragmentStart fragmentStart;
    FragmentTest fragmentTest;
    FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentStart = new FragmentStart();
        fragmentTest = new FragmentTest();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.relative_layout, fragmentStart);
        fragmentTransaction.commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStartClick() {
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.relative_layout, fragmentTest);
        fragmentTransaction.commit();
        dbGoodsHelper = new DBHelper(this, DATABASE_GOODS, DATABASE_VERSION, CREATE_DB_GOODS);
        dbSellersHelper = new DBHelper(this, DATABASE_SELLERS, DATABASE_VERSION, CREATE_DB_SELLERS);
        dbBuyersHelper = new DBHelper(this, DATABASE_BUYERS, DATABASE_VERSION, CREATE_DB_BUYERS);
        dbOrdersHelper = new DBHelper(this, DATABASE_ORDERS, DATABASE_VERSION, CREATE_DB_ORDERS);
        Toast.makeText(this, "Databases was created", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void testListen() {
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.remove(fragmentTest);
        fragmentTransaction.commit();
        Toast.makeText(this, "Look at the Log's", Toast.LENGTH_SHORT).show();
        String test = "test";
        boolean bool = true;
        int integer = 10;
        float floating = 1.1f;
        ContentValues contentValues = new ContentValues();
        SQLiteDatabase database = dbGoodsHelper.getWritableDatabase();
//        contentValues.put("_id", integer);
        contentValues.put("name", test);
        contentValues.put("price", floating);
        contentValues.put("availability", bool);
        long rowID = database.insert(DATABASE_GOODS, null, contentValues);
        Log.d("MyLogs", "It is new data in row number " + rowID);
        Log.d("MyLogs", "Waiting...");
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d("MyLogs", "After sleep");
        Cursor cursor = database.query(DATABASE_GOODS, null, null, null, null, null, null);
        if (cursor.moveToFirst()){
            int idColIndex = cursor.getColumnIndex("_id");
            int nameColIndex = cursor.getColumnIndex("name");
            int priceColIndex = cursor.getColumnIndex("price");
            int availabilityColIndex = cursor.getColumnIndex("availability");
            do {
                Log.d("MyLogs",
                        "ID = " + cursor.getInt(idColIndex) +
                                ", name = " + cursor.getString(nameColIndex) +
                                ", price = " + cursor.getString(priceColIndex)+
                                ", availability = " + cursor.getString(availabilityColIndex));
            } while (cursor.moveToNext());
        }else{
            Log.d("MyLogs", "It's empty");
        }
        database.delete(DATABASE_GOODS, null, null);

        cursor.close();
        database.close();
    }
}
