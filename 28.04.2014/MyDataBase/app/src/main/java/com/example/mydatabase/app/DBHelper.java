package com.example.mydatabase.app;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by androiddev9 on 28.04.14.
 */
public class DBHelper extends SQLiteOpenHelper {

    String request;

    public DBHelper(Context context, String name, int version, String request) {
        super(context, name, null, version);
        this.request = request;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(request);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }
}
