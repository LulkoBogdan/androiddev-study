package com.example.mydatabase.app;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by androiddev9 on 28.04.14.
 */
public class FragmentStart extends Fragment {

    public interface StartListener{
        public void onStartClick();
    }

    StartListener startListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            startListener = (StartListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement StartListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.start_fragment, null);
        Button btnStart = (Button) view.findViewById(R.id.buttonStart);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startListener.onStartClick();
            }
        });
        return view;
    }
}
