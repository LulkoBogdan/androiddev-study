package com.example.mydatabase.app;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by androiddev9 on 28.04.14.
 */
public class FragmentTest extends Fragment {

    public interface TestListener{
        public void testListen();
    }

    TestListener testListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            testListener = (TestListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement TestListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.test_fragment, null);
        Button btnTest = (Button) view.findViewById(R.id.buttonTest);
        btnTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                testListener.testListen();
            }
        });
        return view;
    }
}
