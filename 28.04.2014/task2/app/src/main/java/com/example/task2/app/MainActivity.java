package com.example.task2.app;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends ActionBarActivity {

    final static int CODE_LIST_VIEW = 1;
    final static int CODE_GRID_VIEW = 2;
    final static int CODE_SPINNER = 3;
    final static int CODE_EX_LIST_VIEW = 4;
    final static String KEY_WORD = "identify";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View v){
        Intent intent;
        intent = new Intent (this, SecondActivity.class);
        switch (v.getId()){
            case R.id.btnListView:
                intent.putExtra(KEY_WORD, CODE_LIST_VIEW);
                startActivity(intent);
                break;
            case R.id.btnGridView:
                intent.putExtra(KEY_WORD, CODE_GRID_VIEW);
                startActivity(intent);
                break;
            case R.id.btnSpinner:
                intent.putExtra(KEY_WORD, CODE_SPINNER);
                startActivity(intent);
                break;
            case R.id.btnExView:
                intent.putExtra(KEY_WORD, CODE_EX_LIST_VIEW);
                startActivity(intent);
                break;

        }
    }

}
