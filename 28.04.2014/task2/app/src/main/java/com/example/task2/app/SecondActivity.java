package com.example.task2.app;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import java.util.ArrayList;


public class SecondActivity extends ActionBarActivity {

    int type;
    String[] values = new String[] { "Android", "iPhone", "WindowsMobile",
            "Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X",
            "Linux", "OS/2" };
    String[] spinnerSubs = new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.rellay);
        Intent intent = getIntent();
        type = intent.getIntExtra(MainActivity.KEY_WORD, 0);
        switch (type){
            case MainActivity.CODE_LIST_VIEW:
                ListView listView = new ListView(this);
                MyListViewAdapter adapterListView = new MyListViewAdapter(this, values);
                listView.setAdapter(adapterListView);
                relativeLayout.addView(listView);
                break;
            case MainActivity.CODE_GRID_VIEW:
                GridView gridView = new GridView(this);
                MyGridViewAdapter adapterGridView = new MyGridViewAdapter(this, values);
                gridView.setAdapter(adapterGridView);
                relativeLayout.addView(gridView);
                gridView.setNumColumns(2);
                break;
            case MainActivity.CODE_SPINNER:
                Spinner spinner = new Spinner(this);
                MySpinnerAdapter mySpinnerAdapter = new MySpinnerAdapter(this,
                        values, spinnerSubs);
                spinner.setAdapter(mySpinnerAdapter);
                spinner.setSelection(0);
                relativeLayout.addView(spinner);
                break;
            case MainActivity.CODE_EX_LIST_VIEW:
                ExpandableListView expandableListView = new ExpandableListView(this);
                relativeLayout.addView(expandableListView);
                ArrayList<ArrayList<String>> groups = new ArrayList<ArrayList<String>>();
                ArrayList<String> children1 = new ArrayList<String>();
                ArrayList<String> children2 = new ArrayList<String>();
                children1.add("child_1");
                children1.add("child_2");
                groups.add(children1);
                children2.add("child_1");
                children2.add("child_2");
                children2.add("child_3");
                groups.add(children2);
                ExpListAdapter expListAdapter = new ExpListAdapter(getApplicationContext(), groups);
                expandableListView.setAdapter(expListAdapter);
                break;
            default:
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.second, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
