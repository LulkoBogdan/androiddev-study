package com.example.task2.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by androiddev9 on 28.04.14.
 */
public class MyGridViewAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final String[] values;

    public MyGridViewAdapter (Context context, String[] values){
        super(context, R.layout.my_layout, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context
                .LAYOUT_INFLATER_SERVICE);
        View gridLayout = inflater.inflate(R.layout.grid_layout, parent, false);
        TextView textView = (TextView) gridLayout.findViewById(R.id.textView);
        ImageView imageView = (ImageView) gridLayout.findViewById(R.id.icon);
        textView.setText(values[position]);
        return gridLayout;
    }

}
