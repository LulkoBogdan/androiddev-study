package com.example.task2.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by androiddev9 on 28.04.14.
 */
public class MySpinnerAdapter extends ArrayAdapter<String>{

    private final Context context;
    private final String[] values;
    private String[] spinnerSubs;
    public MySpinnerAdapter (Context context, String[] objects,String[] spinnerSubs){
           super(context, R.layout.spin_layout, R.id.text_main_seen, objects);
        this.context = context;
        this.values = objects;
        this.spinnerSubs = spinnerSubs;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context
                .LAYOUT_INFLATER_SERVICE);
        View mySpinner = inflater.inflate(R.layout.spin_layout, parent,false);
        TextView main_text = (TextView) mySpinner.findViewById(R.id.text_main_seen);
        main_text.setText(values[position]);
        TextView subSpinner = (TextView) mySpinner.findViewById(R.id.sub_text_seen);
        subSpinner.setText(spinnerSubs[position]);

        return mySpinner;
    }
}
