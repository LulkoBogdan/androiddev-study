package com.example.task1.app;

import android.annotation.TargetApi;
import android.app.FragmentTransaction;
import android.app.ListActivity;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleExpandableListAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SecondActivity extends ListActivity {

    int type;
    String[] names = {"Aa", "Bb", "Cc", "Dd", "Ee", "Ff", "Gg", "Hh", "Ii", "Jj"};
    String[] name = {"One", "Two", "Three", "Four"};
    String[] Aa = {"A", "a"};
    String[] Bb = {"B", "b"};
    String[] Cc = {"C", "c"};
    String[] Dd = {"D", "d"};

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.frl);
        Intent intent = getIntent();
        type = intent.getIntExtra(MainActivity.KEY_WORD, 0);
        switch (type){
            case MainActivity.CODE_LIST_VIEW:
                ListView listView = new ListView(this);
                ArrayAdapter<String> adepter1 = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, names);
                listView.setAdapter(adepter1);
                relativeLayout.addView(listView);
                break;
            case MainActivity.CODE_GRID_VIEW:
                GridView gridView = new GridView(this);
                ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, R.layout.item,
                        R.id.textView, names);
                gridView.setAdapter(adapter2);
                relativeLayout.addView(gridView);
                gridView.setNumColumns(2);
                break;
            case MainActivity.CODE_SPINNER:
                Spinner spinner = new Spinner(this);
                ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, names);
                adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter3);
                spinner.setPrompt("Title");
                spinner.setSelection(4);
                relativeLayout.addView(spinner);
                break;
            case MainActivity.CODE_LIST_ACTIVITY:
                ArrayAdapter adapter = new ArrayAdapter(this,
                        android.R.layout.simple_list_item_1, names);
                setListAdapter(adapter);
                break;
            case MainActivity.CODE_LIST_FRAGMENT:
                ListFragment listFragment = new ListFragment(){
                    @Override
                    public void onActivityCreated(Bundle savedInstanceState) {
                        super.onActivityCreated(savedInstanceState);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_list_item_1, names);
                        setListAdapter(adapter);
                    }
                };
                FragmentTransaction ft;
                ft = getFragmentManager().beginTransaction();
                ft.add(R.id.frl, listFragment);
                ft.commit();
                break;
            case MainActivity.CODE_EX_LIST_VIEW:
                ExpandableListView expandableListView = new ExpandableListView(this);
                relativeLayout.addView(expandableListView);
                ArrayList<Map<String, String>> groupData;
                ArrayList<Map<String, String>> childDataItem;
                ArrayList<ArrayList<Map<String, String>>> childData;
                Map<String, String> m;
                groupData = new ArrayList<Map<String, String>>();
                for (String group : name) {
                    m = new HashMap<String, String>();
                    m.put("groupName", group);
                    groupData.add(m);
                }

                String groupFrom[] = new String[] { "groupName" };
                int groupTo[] = new int[] { android.R.id.text1 };

                childData = new ArrayList<ArrayList<Map<String, String>>>();
                childDataItem = new ArrayList<Map<String, String>>();
                for (String month : Aa) {
                    m = new HashMap<String, String>();
                    m.put("name", month);
                    childDataItem.add(m);
                }
                childData.add(childDataItem);
                childDataItem = new ArrayList<Map<String, String>>();
                for (String month : Bb) {
                    m = new HashMap<String, String>();
                    m.put("name", month);
                    childDataItem.add(m);
                }
                childData.add(childDataItem);
                childDataItem = new ArrayList<Map<String, String>>();
                for (String month : Cc) {
                    m = new HashMap<String, String>();
                    m.put("name", month);
                    childDataItem.add(m);
                }
                childData.add(childDataItem);
                childDataItem = new ArrayList<Map<String, String>>();
                for (String month : Dd) {
                    m = new HashMap<String, String>();
                    m.put("name", month);
                    childDataItem.add(m);
                }
                childData.add(childDataItem);
                String childFrom[] = new String[] { "name" };
                int childTo[] = new int[] { android.R.id.text1 };

                SimpleExpandableListAdapter adapter4 = new SimpleExpandableListAdapter(
                        this, groupData,
                        android.R.layout.simple_expandable_list_item_1, groupFrom,
                        groupTo, childData, android.R.layout.simple_list_item_1,
                        childFrom, childTo);

                expandableListView.setAdapter(adapter4);

                break;
            default:
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.second, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
