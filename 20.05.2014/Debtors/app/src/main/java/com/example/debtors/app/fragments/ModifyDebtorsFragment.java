package com.example.debtors.app.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.debtors.app.MainActivity;
import com.example.debtors.app.R;
import com.example.debtors.app.common.Constants;
import com.example.debtors.app.common.DBHelper;
import com.example.debtors.app.common.Debtors;
import com.example.debtors.app.helpers.KeyboardHelper;
import com.example.debtors.app.helpers.Typefaces;
import com.example.debtors.app.services.MainService;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

import java.io.File;
import java.lang.reflect.Field;

/**
 * Created by androiddev9 on 21.05.14.
 */
public class ModifyDebtorsFragment extends Fragment implements View.OnClickListener{

    final int REQUEST_CODE_PHOTO = 1;
    final int RESULT_LOAD_IMAGE = 3;

    Button btnOk, btnCancel;
    EditText etName, etSurname, etPhone, etEmail;
    DBHelper dbDebts;
    Debtors debtor;
    File fileSave;
    File directory;
    ImageView imageView;
    long id;
    Intent intent;
    String path = null;
    DisplayImageOptions options;
    boolean mode = false;
    String objectId = "";
    Debtors debtors;
    MODorDialog moDorDialog;

    public ModifyDebtorsFragment(boolean mode) {
        this.mode = mode;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.modify_debtors, container, false);
        btnCancel = (Button) view.findViewById(R.id.buttonAddDebtorsCancel);
        btnOk = (Button) view.findViewById(R.id.buttonAddDebtorsOk);
        etName = (EditText) view.findViewById(R.id.editTextAddDebtorsName);
        etSurname = (EditText) view.findViewById(R.id.editTextAddDebtorsSurname);
        etPhone = (EditText) view.findViewById(R.id.editTextAddDebtorsPhone);
        etEmail = (EditText) view.findViewById(R.id.editTextAddDebtorsEmail);
        imageView = (ImageView) view.findViewById(R.id.imageViewDebtorsPhoto);
        imageView.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnOk.setOnClickListener(this);
        Typeface font = Typefaces.get(getActivity(), "8-BIT");
        btnOk.setTypeface(font);
        btnCancel.setTypeface(font);
        dbDebts = DBHelper.getInstance(getActivity());
        createDirectory();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(android.R.drawable.button_onoff_indicator_on)
                .showImageForEmptyUri(android.R.drawable.ic_menu_close_clear_cancel)
                .showImageOnFail(android.R.drawable.btn_dialog)
                .resetViewBeforeLoading(false)
                .cacheInMemory(false)
                .cacheOnDisc(true)
                .considerExifParams(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new SimpleBitmapDisplayer())
                .handler(new Handler())
                .build();
        ActionBarActivity actionBarActivity = (ActionBarActivity) getActivity();
        ActionBar actionBar = actionBarActivity.getSupportActionBar();
        actionBar.setTitle(R.string.ab_modify_debtors);
        actionBar.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_holo_dark);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        if(mode){
            btnOk.setText(R.string.update_debtor);
            new AsyncTask<Void, Void, Debtors>(){
                @Override
                protected Debtors doInBackground(Void... params) {
                    return dbDebts.getDebtors(dbDebts.MODIFY_ID);
                }
                @Override
                protected void onPostExecute(Debtors debtor) {
                    super.onPostExecute(debtor);
                    debtors = debtor;
                    path = debtors.getPhoto();
                    ImageLoader imageLoader = ImageLoader.getInstance();
                    imageLoader.displayImage(debtors.getPhoto(), imageView, options);
                    etName.setText(debtors.getName());
                    etSurname.setText(debtors.getSecondName());
                    etEmail.setText(debtors.getEmail());
                    etPhone.setText(debtors.getPhoneNumber());
                    objectId = debtors.getObjectId();
                }
            }.execute();
        }
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            moDorDialog = (MODorDialog) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement MODorDialog");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonAddDebtorsOk:
                onOkClick();
                break;
            case R.id.buttonAddDebtorsCancel:
                KeyboardHelper.hideKeyBoard(getActivity(), etEmail);
                getActivity().onBackPressed();
                break;
            case R.id.imageViewDebtorsPhoto:
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.photo_source);
                builder.setMessage(R.string.photo_type);
                builder.setIcon(android.R.drawable.ic_dialog_info);
                builder.setPositiveButton(R.string.make_photo, myClickListener);
                builder.setNegativeButton(R.string.from_gallery, myClickListener);
                builder.setNeutralButton(R.string.cancel, myClickListener);
                builder.setCancelable(true);
                AlertDialog dialog = builder.create();
                dialog.show();
                break;
        }
    }

    DialogInterface.OnClickListener myClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    fileSave = generateFileUri();
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileSave));
                    startActivityForResult(intent, REQUEST_CODE_PHOTO);
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    Intent i = new Intent(
                    Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, RESULT_LOAD_IMAGE);
                    break;
                case Dialog.BUTTON_NEUTRAL:
                    path = null;
                    ImageLoader imageLoader = ImageLoader.getInstance();
                    imageLoader.displayImage(path, imageView, options);
                    break;
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE_PHOTO){
            if(resultCode == MainActivity.RESULT_OK){
                path = fileSave.getAbsolutePath();
                String buf = "file://";
                buf = buf.concat(path);
                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.displayImage(buf, imageView, options);
            }
        }else if(requestCode == RESULT_LOAD_IMAGE){
            if(resultCode == MainActivity.RESULT_OK){
                if(data != null){
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };

                    Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    if(cursor!=null){
                        if(cursor.moveToFirst()){
                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            String picturePath = cursor.getString(columnIndex);
                            cursor.close();
                            String buf = "file://";
                            path = picturePath;
                            buf = buf.concat(path);
                            ImageLoader imageLoader = ImageLoader.getInstance();
                            imageLoader.displayImage(buf, imageView, options);
                        }
                        cursor.close();
                    }
                }
            }
        }
    }

    private File generateFileUri() {
        File file;
        file = new File(directory.getPath() + "/" + "photo_" + System.currentTimeMillis() + ".jpg");
        return file;
    }

    private void createDirectory() {
        directory = new File(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"FolderDebtors");
        if (!directory.exists())
            directory.mkdirs();
    }

    private void onOkClick(){
        boolean trouble = false;
        String phoneNumber = etPhone.getText().toString();
        if(!TextUtils.isEmpty(phoneNumber)){
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            try {
                Phonenumber.PhoneNumber swissNumberProto = phoneUtil.parse(phoneNumber, "UA");
                boolean isValid = phoneUtil.isValidNumber(swissNumberProto);
                if(!isValid){
                    Toast.makeText(getActivity(), R.string.error_in_phone, Toast.LENGTH_SHORT).show();
                    trouble = true;
                }
            } catch (NumberParseException e) {
                System.err.println("NumberParseException was thrown: " + e.toString());
            }
        }else{
            phoneNumber = "";
        }
        String name = etName.getText().toString();
        String surname = etSurname.getText().toString();
        String email = etEmail.getText().toString();
        if(!TextUtils.isEmpty(email)){
            if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                Toast.makeText(getActivity(), R.string.error_in_email, Toast.LENGTH_SHORT).show();
                trouble = true;
            }
        }
        if(TextUtils.isEmpty(name) && TextUtils.isEmpty(surname) && !trouble){
            Toast.makeText(getActivity(), R.string.error_no_name, Toast.LENGTH_SHORT).show();
            trouble = true;
        }
        String master = Constants.MASTER;
        if(!trouble){
            KeyboardHelper.hideKeyBoard(getActivity(), etEmail);
            debtor = new Debtors();
            debtor.setPhoneNumber(phoneNumber);
            debtor.setName(name);
            debtor.setSecondName(surname);
            debtor.setEmail(email);
            debtor.setMaster(master);
            debtor.setPhoto(path);
            debtor.setObjectId(objectId);
            debtor.setUploded(false);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.PARAM_DEBTOR, debtor);
            intent = new Intent(getActivity(), MainService.class);
            intent.putExtra(Constants.PARAM_DEBTOR, bundle);
            if(mode){
                debtor.setId(debtors.getId());
                new AsyncTask<Debtors, Void, Void>(){
                    @Override
                    protected Void doInBackground(Debtors... params) {
                        Debtors debtor = params[0];
                        dbDebts.updateDebtors(debtor);
                        return null;
                    }
                }.execute(debtor);
                intent = new Intent(getActivity(), MainService.class);
                moDorDialog.showProgressDialog(Constants.CODE_DIALOG_UPDATING);
                PendingIntent pendingIntent = getActivity().createPendingResult(
                        Constants.CODE_SERVICE_UPDATE_DEBTORS, intent, 0);
                intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                bundle = new Bundle();
                bundle.putSerializable(Constants.PARAM_DEBTOR, debtor);
                intent.putExtra(Constants.PARAM_DEBTOR, bundle);
                intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_UPDATE_DEBTORS);
                getActivity().startService(intent);
            }else {
                if(MainActivity.isNetworkConnected(getActivity())) {
                    new AsyncTask<Debtors, Void, Long>() {
                        @Override
                        protected Long doInBackground(Debtors... params) {
                            Debtors debtor = params[0];
                            return dbDebts.createDebtors(debtor);
                        }
                        @Override
                        protected void onPostExecute(Long aLong) {
                            super.onPostExecute(aLong);
                            if (MainActivity.isNetworkConnected(getActivity())) {
                                id = aLong;
                                Bundle bundle = new Bundle();
                                bundle.putSerializable(Constants.PARAM_DEBTOR, debtor);
                                intent.putExtra(Constants.PARAM_DEBTOR, bundle);
                                intent.putExtra(Constants.PARAM_PATH, path);
                                intent.putExtra(Constants.PARAM_ID, id);
                                if (path != null) {
                                    PendingIntent pendingIntent = getActivity().createPendingResult
                                            (Constants.CODE_SERVICE_PHOTO_DEBTOR, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                                    intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                                    intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_PHOTO_DEBTORS);
                                    moDorDialog.showProgressDialog(Constants.CODE_DIALOG_UPLOADING);
                                    getActivity().startService(intent);
                                } else {
                                    PendingIntent pendingIntent = getActivity().createPendingResult(
                                            Constants.CODE_SERVICE_CREATE_DEBTOR, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                                    intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                                    intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_CREATE_DEBTORS);
                                    getActivity().startService(intent);
                                    getActivity().onBackPressed();
                                }
                            }
                        }
                    }.execute(debtor);
                }else{
                    String buf = "file://";
                    if(path!=null) {
                        buf = buf.concat(path);
                        debtor.setPhoto(buf);
                    }
                    new AsyncTask<Debtors, Void, Void>(){
                        @Override
                        protected Void doInBackground(Debtors... params) {
                            Debtors debtor = params[0];
                            dbDebts.createDebtors(debtor);
                            return null;
                        }
                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            getActivity().onBackPressed();
                        }
                    }.execute(debtor);
                }
            }
        }
    }

    public interface MODorDialog{
        public void showProgressDialog(int i);
    }
}
