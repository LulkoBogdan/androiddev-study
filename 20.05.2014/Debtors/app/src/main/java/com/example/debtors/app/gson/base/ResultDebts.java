package com.example.debtors.app.gson.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Богдан on 23.05.14.
 */
public class ResultDebts extends BaseDebtors{

    @Expose
    @SerializedName("amount")
    public int amount;

    @Expose
    @SerializedName("date")
    public String date;

    @Expose
    @SerializedName("type")
    public boolean type;

    @Expose
    @SerializedName("photo")
    public String photo;

    @Expose
    @SerializedName("master")
    public String master;

    @Expose
    @SerializedName("terms")
    public String terms;

    @Expose
    @SerializedName("updatedAt")
    public String updatedAt;

    @Expose
    @SerializedName("debtors_id")
    public String debtors_id;

    @Expose
    @SerializedName("did")
    public long did;

}
