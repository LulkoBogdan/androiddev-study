package com.example.debtors.app.fragments;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.debtors.app.MainActivity;
import com.example.debtors.app.R;
import com.example.debtors.app.common.Constants;
import com.example.debtors.app.common.DBHelper;
import com.example.debtors.app.common.Debts;
import com.example.debtors.app.helpers.CursorHelper;
import com.example.debtors.app.helpers.ScreenHelper;
import com.example.debtors.app.helpers.Typefaces;
import com.example.debtors.app.services.MainService;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;
import java.lang.reflect.Field;

/**
 * Created by androiddev9 on 21.05.14.
 */
public class DebtsListFragment extends Fragment {

    DBHelper dbDebts;
    ListView listView;
    ImageLoader loader = ImageLoader.getInstance();
    DisplayImageOptions options;
    private CustomCursorAdapter customAdapter;
    private final int CM_DELETE_DEBT = 4;
    private final int CM_UPDATE_DEBT = 5;
    protected String[] mPlanetTitles;
    protected DrawerLayout mDrawerLayout;
    protected ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    protected int numMenu;
    View view;
    boolean open = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list, container, false);
        ActionBarActivity actionBarActivity = (ActionBarActivity) getActivity();
        ActionBar actionBar = actionBarActivity.getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
//        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

//        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        dbDebts = DBHelper.getInstance(getActivity());
        setHasOptionsMenu(true);
        listView = (ListView) view.findViewById(R.id.listViewDebts);
        registerForContextMenu(listView);
        listView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.add(0, CM_DELETE_DEBT, 0, R.string.delete_debt);
                menu.add(0, CM_UPDATE_DEBT, 0, R.string.update_debt);
            }
        });
        File cacheDir = StorageUtils.getCacheDirectory(getActivity());
        Point size = ScreenHelper.getScreenSize(getActivity());
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .discCacheExtraOptions(size.x, size.y, Bitmap.CompressFormat.JPEG, 75, null) // width, height, compress format, quality
                .threadPoolSize(5)
                .threadPriority(Thread.NORM_PRIORITY - 1)
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .denyCacheImageMultipleSizesInMemory()
                .discCache(new UnlimitedDiscCache(cacheDir))
                .imageDownloader(new BaseImageDownloader(getActivity()))
                .imageDecoder(new BaseImageDecoder(true))
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .writeDebugLogs()
                .build();
        loader.init(config);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(android.R.drawable.button_onoff_indicator_on)
                .showImageForEmptyUri(android.R.drawable.ic_menu_close_clear_cancel)
                .showImageOnFail(android.R.drawable.btn_dialog)
                .resetViewBeforeLoading(false)
                .cacheInMemory(false)
                .cacheOnDisc(true)
                .considerExifParams(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new SimpleBitmapDisplayer())
                .handler(new Handler())
                .build();

        new AsyncTask<Void, Void, Cursor>(){
            @Override
            protected Cursor doInBackground(Void... params) {
                return dbDebts.getAllDataDebts(Constants.MASTER);
            }
            @Override
            protected void onPostExecute(Cursor cursor) {
                super.onPostExecute(cursor);
                if(cursor != null) {
                    customAdapter = new CustomCursorAdapter(getActivity(), cursor);
                    listView.setAdapter(customAdapter);
                }
            }
        }.execute();
        listView.setEmptyView(view.findViewById(android.R.id.empty));
        createNavigationDrawer(view, actionBar  );
        numMenu = R.menu.menu_all;
        return view;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        long id = acmi.id;
        if(MainActivity.MODE == 0) {
            Debts debts = dbDebts.getDebts(id);
            dbDebts.MODIFY_ID = debts.getId();
            if (item.getItemId() == CM_DELETE_DEBT) {
                new AsyncTask<Void, Void, Debts>() {
                    @Override
                    protected Debts doInBackground(Void... params) {
                        return dbDebts.getDebts(dbDebts.MODIFY_ID);
                    }

                    @Override
                    protected void onPostExecute(Debts debts) {
                        super.onPostExecute(debts);
                        Intent intent = new Intent(getActivity(), MainService.class);
                        intent.putExtra(Constants.PARAM_OBJECT_ID, debts.getObjectId());
                        PendingIntent pendingIntent = getActivity().createPendingResult(Constants.CODE_SERVICE_DELETE_DEBTS, intent, 0);
                        intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                        intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_DELETE_DEBTS);
                        getActivity().startService(intent);
                    }
                }.execute();
                dbDebts.deleteDebts(dbDebts.MODIFY_ID);
                new AsyncTask<Void, Void, Cursor>() {
                    @Override
                    protected Cursor doInBackground(Void... params) {
                        return dbDebts.getAllDataDebts(Constants.MASTER);
                    }

                    @Override
                    protected void onPostExecute(Cursor cursor) {
                        super.onPostExecute(cursor);
                        customAdapter.swapCursor(cursor);
                        customAdapter.notifyDataSetChanged();
                    }
                }.execute();
                return true;
            } else if (item.getItemId() == CM_UPDATE_DEBT) {
                ModifyDebtsFragment modifyDebtsFragment = new ModifyDebtsFragment(true);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_window, modifyDebtsFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                return true;
            }
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(numMenu, menu);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            open = !open;
            return true;
        }
        if (id == R.id.menu_all_add_debt) {
            ModifyDebtsFragment modifyDebtsFragment = new ModifyDebtsFragment(false);
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main_window, modifyDebtsFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            return true;
        }
        if (id == R.id.menu_all_exit) {
            getActivity().deleteDatabase(DBHelper.DATABASE_NAME);
            dbDebts.close();
            SharedPreferences sharedPreferences = getActivity().getPreferences(MainActivity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(Constants.PREF_PASS, "");
            editor.putString(Constants.PREF_LOGIN, "");
            editor.commit();
            Constants.MASTER = "";
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            LoginFragment loginFragment = new LoginFragment();
            fragmentTransaction.replace(R.id.main_window, loginFragment);
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentTransaction.commit();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDrawerToggle.syncState();
    }

    public void setNewCursor(Cursor cursor){
        customAdapter.swapCursor(cursor);
        customAdapter.notifyDataSetChanged();
    }

    public boolean isDrawerLayoutOpen(){
        if(mDrawerLayout != null) {
            //if (open){
            if(mDrawerLayout.isDrawerOpen(mDrawerList)) {
                return true;
            } else {
                return false;
            }
        }else{
            return false;
        }
    }

    public void reverse(){
        open = false;
    }

    public void closeMyDrawers(){
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    public void createNavigationDrawer(View view, ActionBar actionBar){
        mPlanetTitles = getResources().getStringArray(R.array.nav_drawer_items);
        mDrawerLayout = (DrawerLayout) view.findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) view.findViewById(R.id.left_drawer);
        mDrawerList.setAdapter(new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, mPlanetTitles));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener(actionBar));
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout,
                R.drawable.ic_drawer, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ){
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getActivity().invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }

    public class CustomCursorAdapter extends CursorAdapter {

        private class ViewHolder {
            public TextView tvType;
            public ImageView image;
            public TextView tvTerms;
            public TextView tvDebtorsId;
        }

        public CustomCursorAdapter(Context context, Cursor c) {
            super(context, c);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(context);
            return inflater.inflate(R.layout.item_lisview_debts, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            long dId = 0;
            Debts debts = CursorHelper.getDebtsFromCursor(cursor);
            String tp;
            if(debts.isType()){
                tp = getString(R.string.goods);
            }else{
                tp = getString(R.string.money);
            }
            Cursor cur = dbDebts.findDebtorByObjectId(debts.getDebtors_id());
            if(cur!=null){
                if(cur.moveToFirst()){
                    dId = cur.getLong(cur.getColumnIndex(DBHelper.KEY_ID));
                }
                cur.close();
            }
            final ViewHolder holder;
            holder = new ViewHolder();
            holder.tvType = (TextView) view.findViewById(R.id.textViewItemDebtsType);
            holder.image = (ImageView) view.findViewById(R.id.image);
            holder.tvDebtorsId = (TextView) view.findViewById(R.id.textViewItemDebtsIdDebtors);
            holder.tvTerms = (TextView) view.findViewById(R.id.textViewItemDebtsTerms);
            view.setTag(holder);
            Typeface font = Typefaces.get(getActivity(), "fonts/Oswald-Stencbab");
            holder.tvDebtorsId.setTypeface(font);
            holder.tvType.setTypeface(font);
            holder.tvTerms.setTypeface(font);
            holder.tvType.setText(debts.getId() + ".  " + tp + "\t" + getString(R.string.amount_string) + debts.getAmount());
            holder.tvTerms.setText(getString(R.string.from_string) + debts.getDate() + "\t" + getString(R.string.till_string) + debts.getTerms());
            holder.tvDebtorsId.setText(getString(R.string.debtors_id_string) + dId);
            loader.displayImage(debts.getPhoto(), holder.image, options);
        }
    }

    public class DrawerItemClickListener implements AdapterView.OnItemClickListener {

        private ActionBar actionBar;

        public DrawerItemClickListener(ActionBar actionBar) {
            this.actionBar = actionBar;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            int sp = actionBar.getSelectedNavigationIndex();
            mDrawerLayout.closeDrawer(mDrawerList);
            if(position != sp){
                if(position == 1){
                    actionBar.setSelectedNavigationItem(1);
                }
                if(position == 0){
                    actionBar.setSelectedNavigationItem(0);
                }
            }
        }
    }
}
