package com.example.debtors.app.common;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;

import com.example.debtors.app.helpers.CursorHelper;

/**
 * Created by androiddev9 on 20.05.14.
 */
public class DBHelper extends SQLiteOpenHelper{

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "databaseDebtors7";
    //tables names
    public static final String TABLE_DEBTORS = "table_debtors";
    public static final String TABLE_DEBTS = "table_debts";
    //common columns
    public static final String KEY_ID = "_id";
    public static final String KEY_MASTER = "master";
    public static final String KEY_PHOTO = "photo";
    public static final String KEY_OBJECT_ID = "object_id";
    public static final String KEY_UPLOADED = "uploaded";
    //debts columns
    public static final String KEY_TYPE = "type";
    public static final String KEY_DATE = "date";
    public static final String KEY_AMOUNT = "amount";
    public static final String KEY_TERMS = "terms";
    public static final String KEY_DEBTORS_ID = "debtors_id";
    public static final String KEY_DID = "debtors_key_id";
    //debtors columns
    public static final String KEY_SECOND_NAME = "second_name";
    public static final String KEY_NAME = "name";
    public static final String KEY_PHONE_NUMBER = "phone_number";
    public static final String KEY_EMAIL = "email";

    public static final String CREATE_TABLE_DEBTORS = "CREATE TABLE " + TABLE_DEBTORS + "(" + KEY_ID
            + " INTEGER PRIMARY KEY, " + KEY_MASTER + " TEXT, " + KEY_SECOND_NAME + " TEXT, " +
            KEY_NAME + " TEXT, " + KEY_PHONE_NUMBER + " TEXT, " + KEY_EMAIL + " TEXT, " +
            KEY_OBJECT_ID + " TEXT, " + KEY_PHOTO + " TEXT, " + KEY_UPLOADED + " INTEGER" + ");";
    public static final String CREATE_TABLE_DEBTS = "CREATE TABLE " + TABLE_DEBTS + "(" + KEY_ID +
            " INTEGER PRIMARY KEY, " + KEY_MASTER + " TEXT, " + KEY_TYPE + " INTEGER, " + KEY_DATE +
            " TEXT, " + KEY_AMOUNT + " INTEGER, " + KEY_TERMS + " TEXT, " + KEY_OBJECT_ID + " TEXT, "
            + KEY_PHOTO + " TEXT, " + KEY_DEBTORS_ID + " TEXT, " + KEY_DID + " INTEGER, " +
            KEY_UPLOADED + " INTEGER" + ", FOREIGN KEY(debtors_key_id) REFERENCES table_debtors(_id) ON DELETE CASCADE);";

    private static DBHelper instance;
    DBHelper dbDebts;
    public long MODIFY_ID;

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DBHelper getInstance(Context context){
        if(instance == null){
            instance = new DBHelper(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_DEBTORS);
        sqLiteDatabase.execSQL(CREATE_TABLE_DEBTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_DEBTORS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_DEBTS);
        onCreate(sqLiteDatabase);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys = ON;");
        }
    }


    public long createDebts(Debts debts){
        dbDebts = instance;
        SQLiteDatabase sqLiteDatabase = dbDebts.getWritableDatabase();
        ContentValues contentValues = CursorHelper.setDebtsValues(debts);
        return sqLiteDatabase.insert(TABLE_DEBTS, null, contentValues);
    }

    public Debts getDebts(long debts_id){
        dbDebts = instance;
        SQLiteDatabase sqLiteDatabase = dbDebts.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_DEBTS + " WHERE " + KEY_ID + " = " + debts_id;
        Debts debts = new Debts();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if(cursor != null){
            if(cursor.moveToFirst()) {
                debts = CursorHelper.getDebtsFromCursor(cursor);
            }
        }
        return debts;
    }

    public int updateDebts(Debts debts){
        dbDebts = instance;
        SQLiteDatabase sqLiteDatabase = dbDebts.getWritableDatabase();
        ContentValues contentValues = CursorHelper.setDebtsValues(debts);
        return sqLiteDatabase.update(TABLE_DEBTS, contentValues, KEY_ID + " = ?",
                new String[] { String.valueOf(debts.getId()) });
    }

    public Cursor findDebtByObjectId(String objectId){
        dbDebts = instance;
        SQLiteDatabase sqLiteDatabase = dbDebts.getReadableDatabase();
        String request = KEY_OBJECT_ID + " = ?";
        String[] param = {objectId};
        return sqLiteDatabase.query(TABLE_DEBTS, null, request, param, null ,null, null);
    }

    public void deleteDebts(long debts_id){
        AsyncDeleteDebts asyncDeleteDebts = new AsyncDeleteDebts();
        asyncDeleteDebts.execute(debts_id);
    }

    public Cursor getAllDataDebts(String master) {
        dbDebts = instance;
        SQLiteDatabase sqLiteDatabase = dbDebts.getReadableDatabase();
        return sqLiteDatabase.query(TABLE_DEBTS, null, KEY_MASTER + " = ?", new String[]{master}, null, null, null);
    }

    public long createDebtors(Debtors debtors){
        dbDebts = instance;
        ContentValues contentValues = CursorHelper.setDebtorsValues(debtors);
        SQLiteDatabase sqLiteDatabase = dbDebts.getWritableDatabase();
        return sqLiteDatabase.insert(TABLE_DEBTORS, null, contentValues);
    }

    public Debtors getDebtors(long debtors_id){
        dbDebts = instance;
        SQLiteDatabase sqLiteDatabase = dbDebts.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_DEBTORS + " WHERE " + KEY_ID + " = " + debtors_id;
        Debtors debtors = new Debtors();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if(cursor != null){
            if(cursor.moveToFirst()) {
                debtors = CursorHelper.getDebtorsFromCursor(cursor);
            }
        }
        return debtors;
    }

    public int updateDebtors(Debtors debtors){
        dbDebts = instance;
        SQLiteDatabase sqLiteDatabase = dbDebts.getWritableDatabase();
        ContentValues contentValues = CursorHelper.setDebtorsValues(debtors);
        return sqLiteDatabase.update(TABLE_DEBTORS, contentValues, KEY_ID + " = ?",
                new String[] { String.valueOf(debtors.getId()) });
    }

    public void deleteDebtors(long debtors_id){
        AsyncDeleteDebtors asyncDeleteDebtors = new AsyncDeleteDebtors();
        asyncDeleteDebtors.execute(debtors_id);
    }

    public Cursor getAllDataDebtors(String master) {
        dbDebts = instance;
        SQLiteDatabase sqLiteDatabase = dbDebts.getReadableDatabase();
        return sqLiteDatabase.query(TABLE_DEBTORS, null, KEY_MASTER + " = ?", new String[]{master}, null, null, null);
    }

    public Cursor findDebtorByObjectId(String objectId){
        dbDebts = instance;
        SQLiteDatabase sqLiteDatabase = dbDebts.getReadableDatabase();
        String request = KEY_OBJECT_ID + " = ?";
        String[] param = {objectId};
        return sqLiteDatabase.query(TABLE_DEBTORS, null, request, param, null ,null, null);
    }


    class AsyncDeleteDebts extends AsyncTask<Long, Void, Void>{

        @Override
        protected Void doInBackground(Long... params) {
            dbDebts = instance;
            long debts_id = params[0];
            SQLiteDatabase sqLiteDatabase = dbDebts.getWritableDatabase();
            sqLiteDatabase.delete(TABLE_DEBTS, KEY_ID + " = ?", new String[]{String.valueOf(debts_id)});
            return null;
        }
    }

    class AsyncDeleteDebtors extends AsyncTask<Long, Void, Void>{

        @Override
        protected Void doInBackground(Long... params) {
            dbDebts = instance;
            long debtors_id = params[0];
            SQLiteDatabase sqLiteDatabase = dbDebts.getWritableDatabase();
            sqLiteDatabase.delete(TABLE_DEBTORS, KEY_ID + " = ?", new String[]{String.valueOf(debtors_id)});
            return null;
        }
    }
}
