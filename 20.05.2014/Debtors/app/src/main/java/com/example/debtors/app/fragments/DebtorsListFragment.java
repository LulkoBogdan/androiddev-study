package com.example.debtors.app.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.debtors.app.MainActivity;
import com.example.debtors.app.R;
import com.example.debtors.app.common.Constants;
import com.example.debtors.app.common.DBHelper;
import com.example.debtors.app.common.Debtors;
import com.example.debtors.app.helpers.CursorHelper;
import com.example.debtors.app.helpers.ScreenHelper;
import com.example.debtors.app.helpers.Typefaces;
import com.example.debtors.app.services.MainService;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by androiddev9 on 21.05.14.
 */
public class DebtorsListFragment extends DebtsListFragment {

    final int REQUEST_CODE_CONTACT = 2;
    private final int CM_ADD_TO_PHONE = 1;
    private final int CM_DELETE_DEBTOR = 2;
    private final int CM_UPDATE_DEBTOR = 3;

    ListView listView;
    DBHelper dbDebts;
    ImageLoader loader = ImageLoader.getInstance();
    DisplayImageOptions options;
    public CustomCursorAdapter customAdapter;
    AlertDialog dialog;
    long id;
    Intent intent;
    DLDialog dlDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lis_debtors, container, false);
        ActionBarActivity actionBarActivity = (ActionBarActivity) getActivity();
        ActionBar actionBar = actionBarActivity.getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
//        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

//        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        dbDebts = DBHelper.getInstance(getActivity());
        listView = (ListView)view.findViewById(R.id.listViewDebtors);
        registerForContextMenu(listView);
        listView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.add(0, CM_ADD_TO_PHONE, 0, R.string.menu_add_debtor_to_phone);
                menu.add(0, CM_DELETE_DEBTOR, 0, R.string.delete_debtor);
                menu.add(0, CM_UPDATE_DEBTOR, 0, R.string.update_debtor);
            }
        });
        setHasOptionsMenu(true);
        if(loader.isInited()){
            loader.destroy();
        }
        File cacheDir = StorageUtils.getCacheDirectory(getActivity());
        Point size = ScreenHelper.getScreenSize(getActivity());
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .discCacheExtraOptions(size.x, size.y, Bitmap.CompressFormat.JPEG, 75, null) // width, height, compress format, quality
                .threadPoolSize(5)
                .threadPriority(Thread.NORM_PRIORITY - 1)
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .denyCacheImageMultipleSizesInMemory()
                .discCache(new UnlimitedDiscCache(cacheDir))
                .imageDownloader(new BaseImageDownloader(getActivity()))
                .imageDecoder(new BaseImageDecoder(true))
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .writeDebugLogs()
                .build();
        loader.init(config);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(android.R.drawable.button_onoff_indicator_on)
                .showImageForEmptyUri(android.R.drawable.ic_menu_close_clear_cancel)
                .showImageOnFail(android.R.drawable.btn_dialog)
                .resetViewBeforeLoading(false)
                .cacheInMemory(false)
                .cacheOnDisc(true)
                .considerExifParams(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new SimpleBitmapDisplayer())
                .handler(new Handler())
                .build();

        new AsyncTask<Void, Void, Cursor>(){
            @Override
            protected Cursor doInBackground(Void... params) {
                return dbDebts.getAllDataDebtors(Constants.MASTER);
            }
            @Override
            protected void onPostExecute(Cursor cursor) {
                super.onPostExecute(cursor);
                if(cursor != null) {
                    customAdapter = new CustomCursorAdapter(getActivity(), cursor);
                    listView.setAdapter(customAdapter);
                }
            }
        }.execute();
        listView.setEmptyView(view.findViewById(android.R.id.empty));
        createNavigationDrawer(view, actionBar);
        numMenu = R.menu.menu_debtors;
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            dlDialog = (DLDialog) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement DLDialog");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        id = acmi.id;
        Debtors debtors = dbDebts.getDebtors(id);
        dbDebts.MODIFY_ID = debtors.getId();
        if (item.getItemId() == CM_ADD_TO_PHONE) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.are_you_sure);
            builder.setMessage(R.string.add_to_phone);
            builder.setIcon(android.R.drawable.ic_dialog_info);
            builder.setPositiveButton(R.string.ok, myClickListener);
            builder.setNegativeButton(R.string.cancel, myClickListener);
            builder.setCancelable(true);
            dialog = builder.create();
            dialog.show();
            return true;
        } else if(item.getItemId() == CM_DELETE_DEBTOR){
            new AsyncTask<Void, Void, Debtors>(){
                @Override
                protected Debtors doInBackground(Void... params) {
                    Debtors debtors = dbDebts.getDebtors(dbDebts.MODIFY_ID);
                    String objectId = debtors.getObjectId();
                    Cursor cursor = dbDebts.getAllDataDebts(Constants.MASTER);
                    if(cursor!=null){
                        if(cursor.moveToFirst()){
                            do{
                                String debtors_id = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_DEBTORS_ID));
                                String debts_id = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_OBJECT_ID));
                                if(debtors_id.equals(objectId)){
                                    intent = new Intent(getActivity(), MainService.class);
                                    intent.putExtra(Constants.PARAM_OBJECT_ID, debts_id);
                                    PendingIntent pendingIntent = getActivity().createPendingResult(Constants.CODE_SERVICE_DELETE_DEBTS, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                                    intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                                    intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_DELETE_DEBTS);
                                    getActivity().startService(intent);
                                }
                            }while (cursor.moveToNext());
                        }
                        cursor.close();
                    }
                    intent = new Intent(getActivity(), MainService.class);
                    intent.putExtra(Constants.PARAM_OBJECT_ID, objectId);
                    PendingIntent pendingIntent = getActivity().createPendingResult(Constants.CODE_SERVICE_DELETE_DEBTORS, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                    intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_DELETE_DEBTORS);
                    getActivity().startService(intent);
                    return null;
                }
            }.execute();
            dbDebts.deleteDebtors(dbDebts.MODIFY_ID);
            new AsyncTask<Void, Void, Cursor>(){
                @Override
                protected Cursor doInBackground(Void... params) {
                    return dbDebts.getAllDataDebtors(Constants.MASTER);
                }
                @Override
                protected void onPostExecute(Cursor cursor) {
                    super.onPostExecute(cursor);
                    customAdapter.swapCursor(cursor);
                    customAdapter.notifyDataSetChanged();
                }
            }.execute();
        } else if(item.getItemId() == CM_UPDATE_DEBTOR){
            ModifyDebtorsFragment modifyDebtorsFragment = new ModifyDebtorsFragment(true);
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main_window, modifyDebtorsFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
        return super.onContextItemSelected(item);
    }

    DialogInterface.OnClickListener myClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    new AsyncTask<Long, Void, Bundle>(){
                        @Override
                        protected Bundle doInBackground(Long... params) {
                            Bundle bundle = new Bundle();
                            Debtors debtors = dbDebts.getDebtors(id);
                            URL url;
                            try {
                                url = new URL(debtors.getPhoto());
                                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                                connection.setDoInput(true);
                                connection.connect();
                                InputStream input = connection.getInputStream();
                                Bitmap bitmap = BitmapFactory.decodeStream(input);
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth()/4, bitmap.getHeight()/4, true);
                                bitmap.compress(Bitmap.CompressFormat.PNG , 75, stream);
                                byte[] b = stream.toByteArray();
                                bundle.putByteArray(Constants.PARAM_PHOTO, b);
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            bundle.putSerializable(Constants.PARAM_DEBTOR, debtors);
                            return bundle;
                        }
                        @Override
                        protected void onPostExecute(Bundle bundle) {
                            super.onPostExecute(bundle);
                            Debtors debtor = (Debtors) bundle.getSerializable(Constants.PARAM_DEBTOR);
                            byte[] b = bundle.getByteArray(Constants.PARAM_PHOTO);
                            if(debtor != null){
                                final ArrayList<ContentProviderOperation> contentProviderOperation = new ArrayList<ContentProviderOperation>();

                                contentProviderOperation.add(ContentProviderOperation
                                        .newInsert(ContactsContract.RawContacts.CONTENT_URI)
                                        .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                                        .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                                        .build());
                                if (debtor.getName() != null && debtor.getSecondName() != null) {
                                    String DisplayName = debtor.getName().concat(" " + debtor.getSecondName());
                                    contentProviderOperation.add(ContentProviderOperation
                                            .newInsert(ContactsContract.Data.CONTENT_URI)
                                            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                                            .withValue(ContactsContract.Data.MIMETYPE,ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                                            .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, DisplayName).build());
                                }
                                if (debtor.getPhoneNumber() != null) {
                                    contentProviderOperation.add(ContentProviderOperation
                                            .newInsert(ContactsContract.Data.CONTENT_URI)
                                            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                                            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                                            .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, debtor.getPhoneNumber())
                                            .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                                            .build());
                                }
                                if (debtor.getEmail() != null) {
                                    contentProviderOperation.add(ContentProviderOperation
                                            .newInsert(ContactsContract.Data.CONTENT_URI)
                                            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                                            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                                            .withValue(ContactsContract.CommonDataKinds.Email.DATA, debtor.getEmail())
                                            .withValue(ContactsContract.CommonDataKinds.Email.TYPE,ContactsContract.CommonDataKinds.Email.TYPE_HOME)
                                            .build());
                                    contentProviderOperation.add(ContentProviderOperation
                                            .newInsert(ContactsContract.Data.CONTENT_URI)
                                            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                                            .withValue(ContactsContract.Data.MIMETYPE,ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                                            .withValue(ContactsContract.CommonDataKinds.Email.DATA,debtor.getEmail())
                                            .withValue(ContactsContract.CommonDataKinds.Email.TYPE,ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                                            .build());
                                }
                                if(debtor.getPhoto() != null){
                                    if(b!=null){
                                        contentProviderOperation.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                                                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
                                                .withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, b)
                                                .build());
                                    }
                                }
                                try {
                                    getActivity().getContentResolver().applyBatch(ContactsContract.AUTHORITY, contentProviderOperation);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }.execute(id);
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_all_add_debtor) {
            ModifyDebtorsFragment modifyDebtorsFragment = new ModifyDebtorsFragment(false);
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main_window, modifyDebtorsFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            return true;
        }
        if (id == R.id.menu_all_exit) {
            getActivity().deleteDatabase(DBHelper.DATABASE_NAME);
            dbDebts.close();
            SharedPreferences sharedPreferences = getActivity().getPreferences(MainActivity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(Constants.PREF_PASS, "");
            editor.putString(Constants.PREF_LOGIN, "");
            editor.commit();
            Constants.MASTER = "";
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            LoginFragment loginFragment = new LoginFragment();
            fragmentTransaction.replace(R.id.main_window, loginFragment);
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentTransaction.commit();
            return true;
        }
        if(id == R.id.menu_add_debtor_from_phone){
            Intent intent = new Intent (Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
            startActivityForResult(intent, REQUEST_CODE_CONTACT);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        if(requestCode == REQUEST_CODE_CONTACT){
            if(resultCode == MainActivity.RESULT_OK){
                Uri contactUri = intent.getData();
                String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER,
                        ContactsContract.CommonDataKinds.Phone.PHOTO_URI, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME};
                Cursor cursor = getActivity().getContentResolver().query(contactUri, projection, null, null, null);
                if(cursor != null){
                    int columnNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    int columnPath = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI);
                    int columnName = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                    if (cursor.moveToFirst()) {
                        String number = cursor.getString(columnNumber);
                        String path = cursor.getString(columnPath);
                        String name = cursor.getString(columnName);
                        String email = "";
                        projection = new String[] {ContactsContract.Data.DISPLAY_NAME,ContactsContract.Contacts.Data.DATA1,
                                ContactsContract.Contacts.Data.MIMETYPE };
                        cursor = getActivity().getContentResolver().query(
                                ContactsContract.Data.CONTENT_URI, projection,
                                ContactsContract.Data.DISPLAY_NAME + " = ?",
                                new String[] { name },
                                null);
                        if (cursor.moveToFirst()) {
                            int mimeIdx = cursor.getColumnIndex(
                                    ContactsContract.Contacts.Data.MIMETYPE);
                            int dataIdx = cursor.getColumnIndex(
                                    ContactsContract.Contacts.Data.DATA1);
                            do {
                                String mime = cursor.getString(mimeIdx);
                                if (ContactsContract.CommonDataKinds.Email
                                        .CONTENT_ITEM_TYPE.equalsIgnoreCase(mime)) {
                                    email = cursor.getString(dataIdx);
                                    Log.d("MyLogs", "email = " + email);
                                }
                            } while (cursor.moveToNext());
                        }
                        cursor.close();
                        String[] fullName = name.split(" ");
                        Debtors debtor = new Debtors();
                        debtor.setPhoto(path);
                        debtor.setMaster(Constants.MASTER);
                        debtor.setEmail(email);
                        debtor.setPhoneNumber(number);
                        debtor.setName(fullName[0]);
                        if(fullName.length > 1) {
                            debtor.setSecondName(fullName[1]);
                        }else{
                            debtor.setSecondName("");
                        }
                        long id = dbDebts.createDebtors(debtor);
                        new AsyncTask<Void, Void, Cursor>(){
                            @Override
                            protected Cursor doInBackground(Void... params) {
                                return dbDebts.getAllDataDebtors(Constants.MASTER);
                            }
                            @Override
                            protected void onPostExecute(Cursor cursor) {
                                super.onPostExecute(cursor);
                                customAdapter.swapCursor(cursor);
                                customAdapter.notifyDataSetChanged();
                            }
                        }.execute();
                        Intent in = new Intent(getActivity(), MainService.class);
                        in.putExtra(Constants.PARAM_ID, id);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(Constants.PARAM_DEBTOR, debtor);
                        in.putExtra(Constants.PARAM_DEBTOR, bundle);
                        if(TextUtils.isEmpty(path)) {
                            PendingIntent pendingIntent = getActivity().createPendingResult(Constants.CODE_SERVICE_CREATE_DEBTOR, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                            in.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                            in.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_CREATE_DEBTORS);
                            getActivity().startService(in);
                        } else {
                            in.putExtra(Constants.PARAM_PATH, path);
                            PendingIntent pendingIntent = getActivity().createPendingResult
                                    (Constants.CODE_SERVICE_PHOTO_DEBTOR, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                            in.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                            in.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_PHOTO_DEBTORS);
                            dlDialog.showProgressDialog(Constants.CODE_DIALOG_UPLOADING);
                            getActivity().startService(in);
                        }
                    }
                    cursor.close();
                }
            }
        }
    }

    public CustomCursorAdapter getCustomAdapter(){
        return customAdapter;
    }

    public class CustomCursorAdapter extends CursorAdapter {

        private class ViewHolder {
            public TextView text;
            public ImageView image;
            public TextView phone;
            public TextView email;
        }

        public CustomCursorAdapter(Context context, Cursor c) {
            super(context, c);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(context);
            return inflater.inflate(R.layout.item_listview_debtors, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            Debtors debtor = CursorHelper.getDebtorsFromCursor(cursor);
            final ViewHolder holder;
            holder = new ViewHolder();
            holder.text = (TextView) view.findViewById(R.id.textViewItemDebtorsName);
            holder.image = (ImageView) view.findViewById(R.id.image);
            holder.email = (TextView) view.findViewById(R.id.textViewItemDebtorsEmail);
            holder.phone = (TextView) view.findViewById(R.id.textViewItemDebtorsPhone);
            view.setTag(holder);
            Typeface font = Typefaces.get(getActivity(), "gtw");
            holder.email.setTypeface(font);
            holder.text.setTypeface(font);
            holder.phone.setTypeface(font);
            if(!TextUtils.isEmpty(debtor.getName())) {
                holder.text.setText(debtor.getId() + ". " + debtor.getName() + " " + debtor.getSecondName());
            }else{
                holder.text.setText(debtor.getId() + ". " + debtor.getSecondName());
            }
            holder.email.setText(getString(R.string.email_string) + debtor.getEmail());
            holder.phone.setText(getString(R.string.phone_string) + debtor.getPhoneNumber());
            loader.displayImage(debtor.getPhoto(), holder.image, options);
        }
    }

    public interface DLDialog{
        public void showProgressDialog(int i);
    }
}
