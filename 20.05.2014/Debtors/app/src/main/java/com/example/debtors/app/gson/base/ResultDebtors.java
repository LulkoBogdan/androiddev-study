package com.example.debtors.app.gson.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Богдан on 25.05.14.
 */
public class ResultDebtors extends BaseDebtors{

    @Expose
    @SerializedName("name")
    public String name = null;

    @Expose
    @SerializedName("surname")
    public String surname = null;

    @Expose
    @SerializedName("phone")
    public String phone = null;

    @Expose
    @SerializedName("email")
    public String email = null;

    @Expose
    @SerializedName("photo")
    public String photo = null;

    @Expose
    @SerializedName("master")
    public String master = null;

    @Expose
    @SerializedName("updatedAt")
    public String updatedAt;

}
