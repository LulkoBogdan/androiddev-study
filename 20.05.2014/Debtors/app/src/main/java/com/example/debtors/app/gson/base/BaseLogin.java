package com.example.debtors.app.gson.base;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Богдан on 27.05.14.
 */
public class BaseLogin extends BaseDebtors{

    @SerializedName("sessionToken")
    public String sessionToken = null;

    @SerializedName("updatedAt")
    public String updstedAT = null;

    @SerializedName("username")
    public String username = null;

}
