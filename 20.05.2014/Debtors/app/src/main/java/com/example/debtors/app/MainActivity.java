package com.example.debtors.app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.debtors.app.common.Constants;
import com.example.debtors.app.common.DBHelper;
import com.example.debtors.app.common.Debtors;
import com.example.debtors.app.common.Debts;
import com.example.debtors.app.fragments.DebtorsListFragment;
import com.example.debtors.app.fragments.DebtsListFragment;
import com.example.debtors.app.fragments.LoginFragment;
import com.example.debtors.app.fragments.ModifyDebtorsFragment;
import com.example.debtors.app.fragments.ModifyDebtsFragment;
import com.example.debtors.app.fragments.SignUpFragment;
import com.example.debtors.app.gson.base.ResultDebtors;
import com.example.debtors.app.gson.base.ResultDebts;
import com.example.debtors.app.gson.base.SearchResponse;
import com.example.debtors.app.gson.base.SearchResponseDebtors;
import com.example.debtors.app.services.MainService;
import com.google.gson.Gson;

import java.util.List;

public class MainActivity extends ActionBarActivity implements ActionBar.OnNavigationListener,
        LoginFragment.LFDialog, ModifyDebtsFragment.MDFDialog, SignUpFragment.SUFDialog,
        ModifyDebtorsFragment.MODorDialog, DebtorsListFragment.DLDialog, ActionBar.TabListener{

    public static int MODE = 0;

    DBHelper dbDebts;
    LoginFragment loginFragment;
    SharedPreferences sharedPreferences;
    ProgressDialog progressDialog;
    String[] data;
    AlertDialog dialog;
    DebtsListFragment debtsListFragment;
    DebtorsListFragment debtorsListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data = new String[] {getString(R.string.debts), getString(R.string.debtors)};
        setContentView(R.layout.activity_main);
        ActionBar acBar = getSupportActionBar();
//        acBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, data);
//        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
//        acBar.setListNavigationCallbacks(adapter, this);

//        acBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        ActionBar.Tab tab1 = acBar.newTab();
        tab1.setText(R.string.debts);
        tab1.setTabListener(this);
        acBar.addTab(tab1);
        ActionBar.Tab tab2 = acBar.newTab();
        tab2.setText(R.string.debtors);
        tab2.setTabListener(this);
        acBar.addTab(tab2);

        sharedPreferences = getPreferences(MODE_PRIVATE);
        String pass = sharedPreferences.getString(Constants.PREF_PASS, "");
        String login = sharedPreferences.getString(Constants.PREF_LOGIN, "");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.connection_failed);
        builder.setMessage(R.string.what_to_do);
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setPositiveButton(R.string.ok, myClickListener);
        builder.setNegativeButton(R.string.exit, myClickListener);
        builder.setCancelable(true);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        dbDebts = DBHelper.getInstance(this);
        dialog = builder.create();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        loginFragment = new LoginFragment();
        fragmentTransaction.add(R.id.main_window, loginFragment);
        fragmentTransaction.commit();
        if(!isNetworkConnected(this)){
            dialog.show();
        }else{
            if(!TextUtils.isEmpty(pass) && !TextUtils.isEmpty(login)){
                Intent intent = new Intent(this, MainService.class);
                progressDialog = new ProgressDialog(this);
                progressDialog.setCancelable(false);
                progressDialog.setTitle(R.string.connecting);
                progressDialog.setMessage(getString(R.string.please_wait));
                progressDialog.show();
                PendingIntent pendingIntent = createPendingResult(Constants.CODE_SERVICE_LOG_IN, intent, 0);
                intent.putExtra(Constants.PARAM_LOGIN, login);
                intent.putExtra(Constants.PARAM_PASS, pass);
                intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_LOGIN);
                startService(intent);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(dbDebts!=null){
            dbDebts.close();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(android.R.id.home == id) {
            if(getSupportFragmentManager().getBackStackEntryCount() != 0){
                onBackPressed();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    DialogInterface.OnClickListener myClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    finish();
                    break;
            }
        }
    };

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && data!=null){
            int result = data.getIntExtra(Constants.PARAM_RESULT, 0);
            if(progressDialog != null ){
                progressDialog.dismiss();
            }
            switch (requestCode){
                case Constants.CODE_SERVICE_SIGN_UP:
                    signUpResult(result);
                    break;
                case Constants.CODE_SERVICE_LOG_IN:
                    logInResult(result, data);
                    break;
                case Constants.CODE_SERVICE_CREATE_DEBTOR:
                    createDebtorResult(result, data);
                    break;
                case Constants.CODE_SERVICE_CREATE_DEBT:
                    createDebtResult(result, data);
                    break;
                case Constants.CODE_SERVICE_SYNC_DEBTS:
                    syncDebtResult(result, data);
                    break;
                case Constants.CODE_SERVICE_SYNC_DEBTORS:
                    syncDebtorResult(result, data);
                    break;
                case Constants.CODE_SERVICE_PHOTO_DEBTOR:
                    createPhotoDebtorResult(result, data);
                    break;
                case Constants.CODE_SERVICE_PHOTO_DEBT:
                    createPhotoDebtResult(result, data);
                    break;
                case Constants.CODE_SERVICE_UPDATE_DEBTORS:
                    updateDebtorResult(result, data);
                    break;
                case Constants.CODE_SERVICE_UPDATE_DEBTS:
                    updateDebtResult(result, data);
                    break;
                case Constants.CODE_SERVICE_DELETE_DEBTORS:
                    Log.d("MyLogs", "CODE_SERVICE_DELETE_DEBTORS: responseCode = " + result);
                    break;
                case Constants.CODE_SERVICE_DELETE_DEBTS:
                    Log.d("MyLogs", "CODE_SERVICE_DELETE_DEBTS: responseCode = " + result);
                    break;
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(int i, long l) {
        MODE = i;
        if(i == 0 && !TextUtils.isEmpty(Constants.MASTER)){
            debtsListFragment = new DebtsListFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main_window, debtsListFragment);
            fragmentTransaction.commit();
        } else if(i == 1 && !TextUtils.isEmpty(Constants.MASTER)){
            debtorsListFragment = new DebtorsListFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main_window, debtorsListFragment);
            fragmentTransaction.commit();
        } else {
            Toast.makeText(this, getString(R.string.login_first), Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    @Override
    public void showProgressDialog(int i) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        if(i == Constants.CODE_DIALOG_CONNECTING){
            progressDialog.setTitle(R.string.connecting);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.show();
        } else if(i == Constants.CODE_DIALOG_UPLOADING) {
            progressDialog.setTitle(R.string.uploading_photo);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.show();
        } else if(i == Constants.CODE_DIALOG_UPDATING) {
            progressDialog.setTitle(R.string.updating_data);
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.show();
        }
    }

    @Override
    public void showAlertDialog() {
        dialog.show();
    }

    private void signUpResult(int result){
        if(result == Constants.MESSAGE_CREATED) {
            Toast.makeText(this, R.string.sign_up_finish, Toast.LENGTH_SHORT).show();
            onBackPressed();
        }else if(result == Constants.MESSAGE_IN_USE){
            Toast.makeText(this, R.string.login_in_use, Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this, R.string.sign_up_failed, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        ActionBar acBar = getSupportActionBar();
        if(acBar.getNavigationMode() == ActionBar.NAVIGATION_MODE_LIST) {
            if (acBar.getSelectedNavigationIndex() == 1 && debtorsListFragment.isDrawerLayoutOpen()) {
                debtorsListFragment.closeMyDrawers();
            } else if (acBar.getSelectedNavigationIndex() == 0 && debtsListFragment.isDrawerLayoutOpen()) {
                debtsListFragment.closeMyDrawers();
            } else {
                super.onBackPressed();
            }
        }else if(acBar.getNavigationMode() == ActionBar.NAVIGATION_MODE_TABS){
            ActionBar.Tab tab = acBar.getSelectedTab();
            if(tab.getPosition() == 0 && debtsListFragment.isDrawerLayoutOpen()){
                debtsListFragment.closeMyDrawers();
                debtsListFragment.reverse();
            }else if(tab.getPosition() == 1 && debtorsListFragment.isDrawerLayoutOpen()){
                debtorsListFragment.closeMyDrawers();
                debtorsListFragment.reverse();
            }else{
                super.onBackPressed();
            }
        }else{
            if(getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
                acBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
            }else{
                finish();
            }
        }
    }

    private void logInResult(int result, Intent data){
        sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if(result == Constants.MESSAGE_OK){
            Constants.MASTER = data.getStringExtra(Constants.PARAM_MASTER);
            Constants.SESSION = data.getStringExtra(Constants.PARAM_SESSION);
//            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            editor.putString(Constants.PREF_PASS, data.getStringExtra(Constants.PARAM_PASS));
            editor.putString(Constants.PREF_LOGIN, data.getStringExtra(Constants.PARAM_LOGIN));
            editor.commit();
            getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
            getSupportActionBar().setSelectedNavigationItem(MODE);
//            if(MODE == 0){
//                debtsListFragment = new DebtsListFragment();
//                fragmentTransaction.replace(R.id.main_window, debtsListFragment);
//            } else if(MODE == 1){
//                debtorsListFragment = new DebtorsListFragment();
//                fragmentTransaction.replace(R.id.main_window, debtorsListFragment);
//            }
            Intent intent = new Intent(this, MainService.class);
            PendingIntent pendingIntent = createPendingResult(Constants.CODE_SERVICE_SYNC_DEBTORS, intent, 0);
            intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_SYNC_DEBTORS);
            intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
            startService(intent);
            if(isNetworkConnected(this)) {
                new AsyncTask<Void, Void, Cursor>() {
                    @Override
                    protected Cursor doInBackground(Void... params) {
                        return dbDebts.getAllDataDebts(Constants.MASTER);
                    }

                    @Override
                    protected void onPostExecute(Cursor cursor) {
                        super.onPostExecute(cursor);
                        if (cursor != null) {
                            if (cursor.moveToFirst()) {
                                do {
                                    boolean uploaded = cursor.getInt(cursor.getColumnIndex(DBHelper.KEY_UPLOADED)) > 0;
                                    String path = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_PHOTO));
                                    String debtorsId = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_DEBTORS_ID));
                                    long did = cursor.getLong(cursor.getColumnIndex(DBHelper.KEY_DID));
                                    long id = cursor.getLong(cursor.getColumnIndex(DBHelper.KEY_ID));
                                    if (!uploaded) {
                                        Intent intent = new Intent(MainActivity.this, MainService.class);
                                        Debts debts = dbDebts.getDebts(id);
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable(Constants.PARAM_DEBT, debts);
                                        intent.putExtra(Constants.PARAM_DEBT, bundle);
                                        intent.putExtra(Constants.PARAM_DEBTORS_ID, debtorsId);
                                        intent.putExtra(Constants.PARAM_DID, did);
                                        intent.putExtra(Constants.PARAM_ID, id);
                                        intent.putExtra(Constants.PARAM_TIME, (long)0);
                                        if (!TextUtils.isEmpty(path) && !path.contains("http://")) {
                                            PendingIntent pendingIntent = MainActivity.this.createPendingResult(Constants.CODE_SERVICE_PHOTO_DEBT, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                                            intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_PHOTO_DEBTS);
                                            intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                                            intent.putExtra(Constants.PARAM_PATH, path);
                                            progressDialog.setTitle(R.string.uploading_photo);
                                            progressDialog.setMessage(getString(R.string.please_wait));
                                            progressDialog.show();
                                            MainActivity.this.startService(intent);
                                        } else {
                                            PendingIntent pendingIntent = MainActivity.this.createPendingResult(Constants.CODE_SERVICE_CREATE_DEBT, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                                            intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_CREATE_DEBTS);
                                            intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                                            MainActivity.this.startService(intent);
                                        }
                                    }
                                } while (cursor.moveToNext());
                            }
                            cursor.close();
                        }
                    }
                }.execute();

                new AsyncTask<Void, Void, Cursor>() {
                    @Override
                    protected Cursor doInBackground(Void... params) {
                        return dbDebts.getAllDataDebtors(Constants.MASTER);
                    }

                    @Override
                    protected void onPostExecute(Cursor cursor) {
                        super.onPostExecute(cursor);
                        if (cursor != null) {
                            if (cursor.moveToFirst()) {
                                do {
                                    String path = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_PHOTO));
                                    long id = cursor.getLong(cursor.getColumnIndex(DBHelper.KEY_ID));
                                    boolean uploaded = cursor.getInt(cursor.getColumnIndex(DBHelper.KEY_UPLOADED)) > 0;
                                    Debtors debtors = dbDebts.getDebtors(id);
                                    if (!uploaded) {
                                        Intent intent = new Intent(MainActivity.this, MainService.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable(Constants.PARAM_DEBTOR, debtors);
                                        intent.putExtra(Constants.PARAM_DEBTOR, bundle);
                                        intent.putExtra(Constants.PARAM_PATH, path);
                                        intent.putExtra(Constants.PARAM_ID, id);
                                        if (!TextUtils.isEmpty(path) && !path.contains("http://")) {
                                            PendingIntent pendingIntent = MainActivity.this.createPendingResult
                                                    (Constants.CODE_SERVICE_PHOTO_DEBTOR, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                                            intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                                            intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_PHOTO_DEBTORS);
                                            progressDialog.setTitle(R.string.uploading_photo);
                                            progressDialog.setMessage(getString(R.string.please_wait));
                                            progressDialog.show();
                                            MainActivity.this.startService(intent);
                                        } else {
                                            PendingIntent pendingIntent = MainActivity.this.createPendingResult(
                                                    Constants.CODE_SERVICE_CREATE_DEBTOR, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                                            intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                                            intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_CREATE_DEBTORS);
                                            MainActivity.this.startService(intent);
                                        }
                                    }
                                } while (cursor.moveToNext());
                            }
                            cursor.close();
                        }
                    }
                }.execute();
            }
            getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
//            fragmentTransaction.commit();
        } else {
            Toast.makeText(this, R.string.logging_failed, Toast.LENGTH_SHORT).show();
            editor.putString(Constants.PREF_PASS, data.getStringExtra(""));
            editor.putString(Constants.PREF_LOGIN, data.getStringExtra(""));
            editor.commit();
        }
    }

    private void createDebtResult(int result, Intent data){
        if(result == Constants.MESSAGE_CREATED){
            Log.d("MyLogs", getString(R.string.add_debt_finish));
            new AsyncTask<Intent, Void, Void>(){
                @Override
                protected Void doInBackground(Intent... params) {
                    Intent data = params[0];
                    String obj_id = data.getStringExtra(Constants.PARAM_OBJECT_ID);
                    long id = data.getLongExtra(Constants.PARAM_ID, 0);
                    Debts debt = dbDebts.getDebts(id);
                    debt.setObjectId(obj_id);
                    debt.setUploaded(true);
                    dbDebts.updateDebts(debt);
                    return null;
                }
            }.execute(data);
            long left = data.getLongExtra(Constants.PARAM_TIME, 0);
            Log.d("MyLogs", "createDebtResult: left = " + left);
            if(left > 0) {
                startActivity(createIntentCalender(left));
            }
            if(getSupportFragmentManager().getBackStackEntryCount() != 0) {
                onBackPressed();
            }
        } else {
            Log.d("MyLogs", getString(R.string.add_debt_failed));
        }
    }

    private void createDebtorResult(int result, Intent data){
        if(result == Constants.MESSAGE_CREATED){
            Log.d("MyLogs", getString(R.string.add_debtor_finish));
            new AsyncTask<Intent, Void, Void>(){
                @Override
                protected Void doInBackground(Intent... params) {
                    Intent data = params[0];
                    String obj_id = data.getStringExtra(Constants.PARAM_OBJECT_ID);
                    long id = data.getLongExtra(Constants.PARAM_ID, 0);
                    Debtors debtor = dbDebts.getDebtors(id);
                    debtor.setObjectId(obj_id);
                    debtor.setUploded(true);
                    dbDebts.updateDebtors(debtor);
                    return null;
                }
            }.execute(data);
        } else {
            Log.d("MyLogs", getString(R.string.add_debtor_failed));
        }
    }

    private void createPhotoDebtorResult(int result, Intent data){
        if(result == Constants.MESSAGE_CREATED || result == Constants.MESSAGE_OK){
            new AsyncTask<Intent, Void, Void>(){
                @Override
                protected Void doInBackground(Intent... params) {
                    Intent data = params[0];
                    long id = data.getLongExtra(Constants.PARAM_ID, 0);
                    String buf = "file://";
                    String uri = data.getStringExtra(Constants.PARAM_PATH);
                    String objectId = data.getStringExtra(Constants.PARAM_OBJECT_ID);
                    if(!uri.contains("http://") && !uri.contains("file://")){
                        uri = buf.concat(uri);
                    }
                    Debtors debtor = dbDebts.getDebtors(id);
                    debtor.setObjectId(objectId);
                    debtor.setPhoto(uri);
                    debtor.setUploded(true);
                    dbDebts.updateDebtors(debtor);
                    return null;
                }
            }.execute(data);
            if(getSupportFragmentManager().getBackStackEntryCount() != 0) {
                onBackPressed();
            }
        } else {
            Log.d("MyLogs", getString(R.string.add_photo_failed));
        }
    }

    private void createPhotoDebtResult(int result, Intent data){
        if(result == Constants.MESSAGE_CREATED || result == Constants.MESSAGE_OK){
            new AsyncTask<Intent, Void, Void>(){
                @Override
                protected Void doInBackground(Intent... params) {
                    Intent data = params[0];
                    long id = data.getLongExtra(Constants.PARAM_ID, 0);
                    String uri = data.getStringExtra(Constants.PARAM_PATH);
                    String objectId = data.getStringExtra(Constants.PARAM_OBJECT_ID);
                    Debts debt = dbDebts.getDebts(id);
                    debt.setObjectId(objectId);
                    debt.setPhoto(uri);
                    debt.setUploaded(true);
                    dbDebts.updateDebts(debt);
                    return null;
                }
            }.execute(data);
            long left = data.getLongExtra(Constants.PARAM_TIME, 0);
            if(left > 0) {
                startActivity(createIntentCalender(left));
            }
            if(getSupportFragmentManager().getBackStackEntryCount() != 0) {
                onBackPressed();
            }
        } else {
            Log.d("MyLogs", getString(R.string.add_photo_failed));
        }
    }

    private void updateDebtorResult(int result, Intent data){
        if(result == Constants.MESSAGE_CREATED || result == Constants.MESSAGE_OK){
            new AsyncTask<Intent, Void, Void>(){
                @Override
                protected Void doInBackground(Intent... params) {
                    Intent data = params[0];
                    int id = data.getIntExtra(Constants.PARAM_ID, 0);
                    String uri = data.getStringExtra(Constants.PARAM_PATH);
                    Debtors debtor = dbDebts.getDebtors(id);
                    debtor.setPhoto(uri);
                    debtor.setUploded(true);
                    dbDebts.updateDebtors(debtor);
                    return null;
                }
            }.execute(data);
            if(getSupportFragmentManager().getBackStackEntryCount() != 0) {
                onBackPressed();
            }
        } else {
            Log.d("MyLogs", "Update Debtors failed");
        }
    }

    private void updateDebtResult(int result, Intent data){
        if(result == Constants.MESSAGE_CREATED || result == Constants.MESSAGE_OK){
            new AsyncTask<Intent, Void, Void>(){
                @Override
                protected Void doInBackground(Intent... params) {
                    Intent data = params[0];
                    int id = data.getIntExtra(Constants.PARAM_ID, 0);
                    String uri = data.getStringExtra(Constants.PARAM_PATH);
                    Debts debt = dbDebts.getDebts(id);
                    debt.setPhoto(uri);
                    debt.setUploaded(true);
                    dbDebts.updateDebts(debt);
                    return null;
                }
            }.execute(data);
            if(getSupportFragmentManager().getBackStackEntryCount() != 0) {
                onBackPressed();
            }
        } else {
            Log.d("MyLogs", "Update Debts failed");
        }
    }

    private void syncDebtorResult(int result, Intent data){
        if(result == Constants.MESSAGE_OK){
            String out = data.getStringExtra(Constants.PARAM_OUT_STRING);
            if(!TextUtils.isEmpty(out)){
                Log.d("MyLogs", "SyncDebtors.out_string = " + out);
                Gson gson = new Gson();
                SearchResponseDebtors searchResponse = gson.fromJson(out, SearchResponseDebtors.class);
                List<ResultDebtors> results = searchResponse.results;
                GetDebtorsRes getDebtorsRes = new GetDebtorsRes(results);
                getDebtorsRes.start();
                Intent intent = new Intent(this, MainService.class);
                PendingIntent pendingIntent = createPendingResult(Constants.CODE_SERVICE_SYNC_DEBTS, intent, 0);
                intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_SYNC_DEBTS);
                intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                startService(intent);
            }
        } else {
            Log.d("MyLogs", getString(R.string.download_debtor_failed));
        }
    }

    private void syncDebtResult(int result, Intent data){
        if(result == Constants.MESSAGE_OK){
            String out = data.getStringExtra(Constants.PARAM_OUT_STRING);
            if(!TextUtils.isEmpty(out)){
                Log.d("MyLogs", "SyncDebts.out_string = " + out);
                Gson gson = new Gson();
                SearchResponse searchResponse = gson.fromJson(out, SearchResponse.class);
                List<ResultDebts> results = searchResponse.results;
                GetDebtsRes getDebtsRes = new GetDebtsRes(results);
                getDebtsRes.start();
            }
        } else {
            Log.d("MyLogs", getString(R.string.download_debt_failed));
        }
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        MODE = tab.getPosition();
        if(MODE == 0 && !TextUtils.isEmpty(Constants.MASTER)){
            debtsListFragment = new DebtsListFragment();
            fragmentTransaction.replace(R.id.main_window, debtsListFragment);
        } else if(MODE == 1 && !TextUtils.isEmpty(Constants.MASTER)){
            debtorsListFragment = new DebtorsListFragment();
            fragmentTransaction.replace(R.id.main_window, debtorsListFragment);
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    private class GetDebtorsRes extends Thread{

        List<ResultDebtors> results;

        private GetDebtorsRes(List<ResultDebtors> results) {
            this.results = results;
        }

        @Override
        public void run() {
            for(ResultDebtors res : results){
                Debtors debtor = new Debtors(res);
                Cursor cursor = dbDebts.findDebtorByObjectId(debtor.getObjectId());
                if(cursor == null || cursor.getCount() == 0){
                    dbDebts.createDebtors(debtor);
                } else {
                    Log.d("MyLogs", "Debtors.cursor != null && cursor.getCount != 0");
                }
                if (cursor != null) {
                    cursor.close();
                }
            }
            if(debtorsListFragment != null) {
                new AsyncTask<Void, Void, Cursor>() {
                    @Override
                    protected Cursor doInBackground(Void... params) {
                        return dbDebts.getAllDataDebtors(Constants.MASTER);
                    }
                    @Override
                    protected void onPostExecute(Cursor cursor) {
                        super.onPostExecute(cursor);
                        DebtorsListFragment.CustomCursorAdapter customAdapter = debtorsListFragment.getCustomAdapter();
                        customAdapter.swapCursor(cursor);
                        customAdapter.notifyDataSetChanged();
                    }
                }.execute();
            }
        }
    }

    private class GetDebtsRes extends Thread{

        List<ResultDebts> results;

        private GetDebtsRes(List<ResultDebts> results) {
            this.results = results;
        }

        @Override
        public void run() {
            for(ResultDebts res : results){
                String debtors_id = res.debtors_id;
                Cursor cursor = dbDebts.findDebtorByObjectId(debtors_id);
                if(cursor == null || cursor.getCount()==0){
                    Log.d("MyLogs", "There is no such Debtor with debtorsId = " + debtors_id);
                    Log.d("MyLogs", getString(R.string.download_debt_failed));
                } else {
                    if(cursor.moveToFirst()){
                        int did = cursor.getInt(cursor.getColumnIndex(DBHelper.KEY_ID));
                        Debts debts = new Debts(res);
                        debts.setDebtors_id(debtors_id);
                        debts.setDid(did);
                        if(res.type){
                            debts.setType(true);
                        }else{
                            debts.setType(false);
                        }
                            cursor = dbDebts.findDebtByObjectId(res.objectId);
                        if(cursor == null || cursor.getCount() == 0){
                            Log.d("MyLogs", "Debts.cursor == null || cursor.getCount == 0");
                            dbDebts.createDebts(debts);
                        } else {
                            Log.d("MyLogs", "Debts.cursor != null && cursor.getCount != 0");
                        }
                    }
                }
                if(cursor!=null) {
                    cursor.close();
                }
            }
            new AsyncTask<Void, Void, Cursor>(){
                @Override
                protected Cursor doInBackground(Void... params) {
                    return dbDebts.getAllDataDebts(Constants.MASTER);
                }
                @Override
                protected void onPostExecute(Cursor cursor) {
                    super.onPostExecute(cursor);
                    debtsListFragment.setNewCursor(cursor);
                }
            }.execute();
        }
    }

    private Intent createIntentCalender(long left){
        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("beginTime", left + 1000 * 60 * 60 * 10);
        intent.putExtra("allDay", true);
        intent.putExtra("rule", "FREQ=YEARLY");
        intent.putExtra("endTime", left + 60 * 60 * 1000 * 11);
        intent.putExtra("title", getString(R.string.title_text));
        return intent;
    }
}
