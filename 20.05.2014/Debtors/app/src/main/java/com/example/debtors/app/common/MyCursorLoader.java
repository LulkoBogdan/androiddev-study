package com.example.debtors.app.common;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

import com.example.debtors.app.MainActivity;

/**
 * Created by androiddev9 on 22.05.14.
 */
public class MyCursorLoader extends CursorLoader {
    DBHelper db;
    int id;

    public MyCursorLoader(Context context, DBHelper db, int id) {
        super(context);
        this.db = db;
        this.id = id;
    }

    @Override
    public Cursor loadInBackground() {
        if(id == 1) {
            Cursor cursor = db.getAllDataDebtors(Constants.MASTER);
            return cursor;
        }
        return null;
    }
}
