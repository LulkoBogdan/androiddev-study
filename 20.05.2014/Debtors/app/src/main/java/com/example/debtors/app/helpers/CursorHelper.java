package com.example.debtors.app.helpers;

import android.content.ContentValues;
import android.database.Cursor;

import com.example.debtors.app.common.DBHelper;
import com.example.debtors.app.common.Debtors;
import com.example.debtors.app.common.Debts;

/**
 * Created by androiddev9 on 19.06.14.
 */
public class CursorHelper {

    public static Debts getDebtsFromCursor(Cursor cursor){
        Debts debts = new Debts();
        debts.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.KEY_ID)));
        debts.setType(cursor.getInt(cursor.getColumnIndex(DBHelper.KEY_TYPE)) > 0);
        debts.setDate(cursor.getString(cursor.getColumnIndex(DBHelper.KEY_DATE)));
        debts.setAmount(cursor.getInt(cursor.getColumnIndex(DBHelper.KEY_AMOUNT)));
        debts.setTerms(cursor.getString(cursor.getColumnIndex(DBHelper.KEY_TERMS)));
        debts.setPhoto(cursor.getString(cursor.getColumnIndex(DBHelper.KEY_PHOTO)));
        debts.setDebtors_id(cursor.getString(cursor.getColumnIndex(DBHelper.KEY_DEBTORS_ID)));
        debts.setMaster(cursor.getString(cursor.getColumnIndex(DBHelper.KEY_MASTER)));
        debts.setObjectId(cursor.getString(cursor.getColumnIndex(DBHelper.KEY_OBJECT_ID)));
        debts.setDid(cursor.getLong(cursor.getColumnIndex(DBHelper.KEY_DID)));
        debts.setUploaded(cursor.getInt(cursor.getColumnIndex(DBHelper.KEY_UPLOADED)) > 0);
        return debts;
    }

    public static Debtors getDebtorsFromCursor(Cursor cursor){
        Debtors debtors = new Debtors();
        debtors.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.KEY_ID)));
        debtors.setSecondName(cursor.getString(cursor.getColumnIndex(DBHelper.KEY_SECOND_NAME)));
        debtors.setName(cursor.getString(cursor.getColumnIndex(DBHelper.KEY_NAME)));
        debtors.setPhoneNumber(cursor.getString(cursor.getColumnIndex(DBHelper.KEY_PHONE_NUMBER)));
        debtors.setEmail(cursor.getString(cursor.getColumnIndex(DBHelper.KEY_EMAIL)));
        debtors.setPhoto(cursor.getString(cursor.getColumnIndex(DBHelper.KEY_PHOTO)));
        debtors.setMaster(cursor.getString(cursor.getColumnIndex(DBHelper.KEY_MASTER)));
        debtors.setObjectId(cursor.getString(cursor.getColumnIndex(DBHelper.KEY_OBJECT_ID)));
        debtors.setUploded(cursor.getInt(cursor.getColumnIndex(DBHelper.KEY_UPLOADED)) > 0);
        return debtors;
    }

    public static ContentValues setDebtsValues(Debts debts){
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.KEY_TYPE, debts.isType());
        contentValues.put(DBHelper.KEY_DATE, debts.getDate());
        contentValues.put(DBHelper.KEY_AMOUNT, debts.getAmount());
        contentValues.put(DBHelper.KEY_TERMS, debts.getTerms());
        contentValues.put(DBHelper.KEY_PHOTO, debts.getPhoto());
        contentValues.put(DBHelper.KEY_DEBTORS_ID, debts.getDebtors_id());
        contentValues.put(DBHelper.KEY_MASTER, debts.getMaster());
        contentValues.put(DBHelper.KEY_OBJECT_ID, debts.getObjectId());
        contentValues.put(DBHelper.KEY_DID, debts.getDid());
        contentValues.put(DBHelper.KEY_UPLOADED, debts.isUploaded());
        return contentValues;
    }

    public static ContentValues setDebtorsValues(Debtors debtors){
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.KEY_SECOND_NAME, debtors.getSecondName());
        contentValues.put(DBHelper.KEY_NAME, debtors.getName());
        contentValues.put(DBHelper.KEY_PHONE_NUMBER, debtors.getPhoneNumber());
        contentValues.put(DBHelper.KEY_EMAIL, debtors.getEmail());
        contentValues.put(DBHelper.KEY_PHOTO, debtors.getPhoto());
        contentValues.put(DBHelper.KEY_MASTER, debtors.getMaster());
        contentValues.put(DBHelper.KEY_OBJECT_ID, debtors.getObjectId());
        contentValues.put(DBHelper.KEY_UPLOADED, debtors.isUploded());
        return contentValues;
    }
}
