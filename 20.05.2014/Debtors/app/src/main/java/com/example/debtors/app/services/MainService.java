package com.example.debtors.app.services;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;

import com.example.debtors.app.MainActivity;
import com.example.debtors.app.common.Constants;
import com.example.debtors.app.common.Debtors;
import com.example.debtors.app.common.Debts;
import com.example.debtors.app.gson.base.BaseDebtors;
import com.example.debtors.app.gson.base.BaseForSigUp;
import com.example.debtors.app.gson.base.BaseLogin;
import com.example.debtors.app.gson.base.BasePhoto;
import com.example.debtors.app.gson.base.ResultDebtors;
import com.example.debtors.app.gson.base.ResultDebts;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Богдан on 26.05.14.
 */
public class MainService extends Service{

    ExecutorService es;

    public void onCreate() {
        super.onCreate();
        es = Executors.newFixedThreadPool(5);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        int type = 0;
        if(intent != null) {
            type = intent.getIntExtra(Constants.PARAM_SERVICE_TYPE, 0);
        }
        String login;
        String pass;
        String photo;
        String master;
        String name;
        String surname;
        String phone;
        String email;
        String path;
        String date;
        String terms;
        String debtorsId;
        PendingIntent pi;
        Debtors debtors;
        Debts debts;
        Bundle bundle;
        int amount;
        long id;
        long did;
        long left;
        boolean type_;
        switch (type){
            case Constants.TYPE_LOGIN:
                login = intent.getStringExtra(Constants.PARAM_LOGIN);
                pass = intent.getStringExtra(Constants.PARAM_PASS);
                pi = intent.getParcelableExtra(Constants.PARAM_PINTENT);
                ThreadLogin tLogin = new ThreadLogin(startId, pi, pass, login);
                es.execute(tLogin);
                break;
            case Constants.TYPE_SIGNUP:
                login = intent.getStringExtra(Constants.PARAM_LOGIN);
                pass = intent.getStringExtra(Constants.PARAM_PASS);
                pi = intent.getParcelableExtra(Constants.PARAM_PINTENT);
                ThreadSignUp tSignUp = new ThreadSignUp(startId, pi, pass, login);
                es.execute(tSignUp);
                break;
            case Constants.TYPE_CREATE_DEBTS:
                bundle = intent.getBundleExtra(Constants.PARAM_DEBT);
                if(bundle != null){
                    debts = (Debts) bundle.getSerializable(Constants.PARAM_DEBT);
                    if(debts != null){
                        left = intent.getLongExtra(Constants.PARAM_TIME, 0);
                        type_ = debts.isType();
                        date = debts.getDate();
                        amount = debts.getAmount();
                        terms = debts.getTerms();
                        photo = debts.getPhoto();
                        master = debts.getMaster();
                        debtorsId = intent.getStringExtra(Constants.PARAM_DEBTORS_ID);
                        id = intent.getLongExtra(Constants.PARAM_ID, 0);
                        did = intent.getLongExtra(Constants.PARAM_DID, 0);
                        pi = intent.getParcelableExtra(Constants.PARAM_PINTENT);
                        ThreadCreateDebts tCreateDebts = new ThreadCreateDebts(startId, pi, type_, date, amount, terms, debtorsId, photo, master, id, did, left);
                        es.execute(tCreateDebts);
                    }
                }
                break;
            case Constants.TYPE_DELETE_DEBTS:
                String objectId = intent.getStringExtra(Constants.PARAM_OBJECT_ID);
                pi = intent.getParcelableExtra(Constants.PARAM_PINTENT);
                ThreadDeleteDebts threadDeleteDebts = new ThreadDeleteDebts(startId, pi, objectId);
                es.execute(threadDeleteDebts);
                break;
            case Constants.TYPE_UPDATE_DEBTS:
                bundle = intent.getBundleExtra(Constants.PARAM_DEBT);
                if(bundle != null){
                    debts =(Debts) bundle.getSerializable(Constants.PARAM_DEBT);
                    pi = intent.getParcelableExtra(Constants.PARAM_PINTENT);
                    ThreadUpdateDebts threadUpdateDebts = new ThreadUpdateDebts(debts, pi, startId);
                    es.execute(threadUpdateDebts);
                }
                break;
            case Constants.TYPE_SYNC_DEBTS:
                pi = intent.getParcelableExtra(Constants.PARAM_PINTENT);
                ThreadSyncDebts tSyncDebts = new ThreadSyncDebts(startId, pi);
                es.execute(tSyncDebts);
                break;
            case Constants.TYPE_CREATE_DEBTORS:
                bundle = intent.getBundleExtra(Constants.PARAM_DEBTOR);
                if(bundle!=null){
                    debtors = (Debtors) bundle.getSerializable(Constants.PARAM_DEBTOR);
                    if(debtors != null){
                        name = debtors.getName();
                        surname = debtors.getSecondName();
                        phone = debtors.getPhoneNumber();
                        email = debtors.getEmail();
                        id = intent.getLongExtra(Constants.PARAM_ID, 0);
                        photo = debtors.getPhoto();
                        master = debtors.getMaster();
                        pi = intent.getParcelableExtra(Constants.PARAM_PINTENT);
                        ThreadCreateDebtors tCreateDebtors = new ThreadCreateDebtors(startId, pi, name, surname, phone, email, photo, master, id);
                        es.execute(tCreateDebtors);
                    }
                }
                break;
            case Constants.TYPE_DELETE_DEBTORS:
                objectId = intent.getStringExtra(Constants.PARAM_OBJECT_ID);
                pi = intent.getParcelableExtra(Constants.PARAM_PINTENT);
                ThreadDeleteDebtors threadDeleteDebtors = new ThreadDeleteDebtors(startId, pi, objectId);
                es.execute(threadDeleteDebtors);
                break;
            case Constants.TYPE_UPDATE_DEBTORS:
                bundle = intent.getBundleExtra(Constants.PARAM_DEBTOR);
                if(bundle != null){
                    debtors =(Debtors) bundle.getSerializable(Constants.PARAM_DEBTOR);
                    pi = intent.getParcelableExtra(Constants.PARAM_PINTENT);
                    ThreadUpdateDebtors threadUpdateDebtors = new ThreadUpdateDebtors(debtors, pi, startId);
                    es.execute(threadUpdateDebtors);
                }
                break;
            case Constants.TYPE_SYNC_DEBTORS:
                pi = intent.getParcelableExtra(Constants.PARAM_PINTENT);
                ThreadSyncDebtors tSyncDebtors = new ThreadSyncDebtors(startId, pi);
                es.execute(tSyncDebtors);
                break;
            case Constants.TYPE_PHOTO_DEBTORS:
                bundle = intent.getBundleExtra(Constants.PARAM_DEBTOR);
                if(bundle != null){
                    debtors = (Debtors) bundle.getSerializable(Constants.PARAM_DEBTOR);
                    if(debtors != null){
                        name = debtors.getName();
                        surname = debtors.getSecondName();
                        phone = debtors.getPhoneNumber();
                        email = debtors.getEmail();
                        photo = debtors.getPhoto();
                        master = debtors.getMaster();
                        id = intent.getLongExtra(Constants.PARAM_ID, 0);
                        pi = intent.getParcelableExtra(Constants.PARAM_PINTENT);
                        path = intent.getStringExtra(Constants.PARAM_PATH);
                        ThreadPhotoDebtors threadPhotoDebtors = new ThreadPhotoDebtors(startId, pi, name, surname, phone, email, photo, master, id, path);
                        es.execute(threadPhotoDebtors);
                    }
                }
                break;
            case Constants.TYPE_PHOTO_DEBTS:
                bundle = intent.getBundleExtra(Constants.PARAM_DEBT);
                if(bundle != null){
                    debts = (Debts) bundle.getSerializable(Constants.PARAM_DEBT);
                    if(debts != null){
                        left = intent.getLongExtra(Constants.PARAM_TIME, 0);
                        debtorsId = intent.getStringExtra(Constants.PARAM_DEBTORS_ID);
                        id = intent.getLongExtra(Constants.PARAM_ID, 0);
                        did = intent.getLongExtra(Constants.PARAM_DID, 0);
                        pi = intent.getParcelableExtra(Constants.PARAM_PINTENT);
                        path = intent.getStringExtra(Constants.PARAM_PATH);
                        type_ = debts.isType();
                        date = debts.getDate();
                        amount = debts.getAmount();
                        terms = debts.getTerms();
                        photo = debts.getPhoto();
                        master = debts.getMaster();
                        ThreadPhotoDebts threadPhotoDebts = new ThreadPhotoDebts(startId, pi, type_, date, amount, terms, debtorsId, photo, master, id, did, path, left);
                        es.execute(threadPhotoDebts);
                    }
                }
                break;
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    private String nameGenerator(){
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    abstract class BaseThread extends Thread{

        String url = "https://api.parse.com/1/";
        HttpResponse httpResponse;
        InputStream instream;
        String response = "";
        HttpEntity entity;
        HttpClient client;
        PendingIntent pi;
        Intent intent;
        String message;
        Gson gson;
        int startId;
        int responseCode;
        long id;
        @Override
        public void run() {
            client = new DefaultHttpClient();
            gson = new Gson();
            intent = new Intent();
        }
        void stopService() {
            stopSelfResult(startId);
        }
    }

    class ThreadLogin extends BaseThread {

        String pass;
        String login;

        public ThreadLogin(int startId, PendingIntent pi, String pass, String login) {
            this.startId = startId;
            this.pi = pi;
            this.pass = pass;
            this.login = login;
        }

        public void run() {
            try {
                super.run();
                url = "https://api.parse.com/1/login?username=" + login + "&password=" + pass;
                HttpGet request = new HttpGet(url);
                request.setHeader(Constants.PARSE_APPLICATION_ID_HEADER, Constants.PARSE_APPLICATION_ID);
                request.setHeader(Constants.PARSE_REST_API_KEY_HEADER, Constants.PARSE_REST_API_KEY);

                httpResponse = client.execute(request);
                responseCode = httpResponse.getStatusLine().getStatusCode();
                message = httpResponse.getStatusLine().getReasonPhrase();
                entity = httpResponse.getEntity();
                if (entity != null) {
                    instream = entity.getContent();
                    response = convertStreamToString(instream);
                    instream.close();
                    BaseLogin baseLogin = gson.fromJson(response, BaseLogin.class);
                    intent.putExtra(Constants.PARAM_SESSION, baseLogin.sessionToken);
                }
                intent.putExtra(Constants.PARAM_RESULT, responseCode);
                intent.putExtra(Constants.PARAM_PASS, pass);
                intent.putExtra(Constants.PARAM_LOGIN, login);
                intent.putExtra(Constants.PARAM_MASTER, login);
                pi.send(MainService.this, MainActivity.RESULT_OK, intent);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            stopService();
        }
    }

    class ThreadSignUp extends BaseThread {

        String pass;
        String login;

        public ThreadSignUp(int startId, PendingIntent pi, String pass, String login) {
            this.startId = startId;
            this.pi = pi;
            this.pass = pass;
            this.login = login;
        }

        public void run() {
            try {
                super.run();
                url = "https://api.parse.com/1/users";
                HttpPost request = new HttpPost(url);
                request.setHeader(Constants.PARSE_APPLICATION_ID_HEADER, Constants.PARSE_APPLICATION_ID);
                request.setHeader(Constants.PARSE_REST_API_KEY_HEADER, Constants.PARSE_REST_API_KEY);
                request.setHeader(Constants.PARSE_CONTENT_TYPE_HEADER, Constants.PARSE_CONTENT_TYPE_JSON_OBJ);
                BaseForSigUp base = new BaseForSigUp();
                base.setUsername(login);
                base.setPassword(pass);
                gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                String req = gson.toJson(base);
                byte[] bytes = req.getBytes();
                request.setEntity(new ByteArrayEntity(bytes));
                httpResponse = client.execute(request);
                responseCode = httpResponse.getStatusLine().getStatusCode();
                message = httpResponse.getStatusLine().getReasonPhrase();
                entity = httpResponse.getEntity();
                if (entity != null) {
                    instream = entity.getContent();
                    String response = convertStreamToString(instream);
                    instream.close();
                }
                intent = new Intent().putExtra(Constants.PARAM_RESULT, responseCode);
                pi.send(MainService.this, MainActivity.RESULT_OK, intent);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            stopService();
        }
    }

    class ThreadCreateDebts extends BaseThread {

        boolean type;
        int amount;
        String debtorsId;
        String date;
        String terms;
        String photo;
        String master;
        long did;
        long left;

        public ThreadCreateDebts(int startId, PendingIntent pi, boolean type, String date , int amount, String terms, String debtorsId, String photo, String master, long id, long did, long left) {
            this.startId = startId;
            this.pi = pi;
            this.amount = amount;
            this.debtorsId = debtorsId;
            this.date = date;
            this.terms = terms;
            this.photo = photo;
            this.master = master;
            this.type = type;
            this.id = id;
            this.did = did;
            this.left = left;
        }

        public void run() {
            try {
                super.run();
                url = "https://api.parse.com/1/classes/Debts";
                HttpPost request = new HttpPost(url);
                request.setHeader(Constants.PARSE_APPLICATION_ID_HEADER, Constants.PARSE_APPLICATION_ID);
                request.setHeader(Constants.PARSE_REST_API_KEY_HEADER, Constants.PARSE_REST_API_KEY);
                request.setHeader(Constants.PARSE_CONTENT_TYPE_HEADER, Constants.PARSE_CONTENT_TYPE_JSON_OBJ);
                ResultDebts base = setDataDebts(master, type, amount, date, photo, debtorsId, terms, did);
                gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                String req = gson.toJson(base);
                byte[] bytes = req.getBytes();
                request.setEntity(new ByteArrayEntity(bytes));
                httpResponse = client.execute(request);
                responseCode = httpResponse.getStatusLine().getStatusCode();
                message = httpResponse.getStatusLine().getReasonPhrase();
                entity = httpResponse.getEntity();
                if (entity != null) {
                    instream = entity.getContent();
                    response = convertStreamToString(instream);
                    instream.close();
                }
                gson = new Gson();
                BaseDebtors baseDebtors = gson.fromJson(response, BaseDebtors.class);
                intent = new Intent().putExtra(Constants.PARAM_RESULT, responseCode);
                intent.putExtra(Constants.PARAM_ID, id);
                intent.putExtra(Constants.PARAM_OBJECT_ID, baseDebtors.objectId);
                intent.putExtra(Constants.PARAM_TIME, left);
                pi.send(MainService.this, MainActivity.RESULT_OK, intent);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            stopService();
        }
    }

    class ThreadCreateDebtors extends BaseThread {

        String name;
        String surname;
        String phone;
        String email;
        String photo;
        String master;

        public ThreadCreateDebtors(int startId, PendingIntent pi, String name, String surname , String phone, String email, String photo, String master, long id) {
            this.startId = startId;
            this.pi = pi;
            this.name = name;
            this.surname = surname;
            this.phone = phone;
            this.email = email;
            this.photo = photo;
            this.master = master;
            this.id = id;
        }

        public void run() {
            try {
                super.run();
                url = "https://api.parse.com/1/classes/Debtors";
                HttpPost request = new HttpPost(url);
                request.setHeader(Constants.PARSE_APPLICATION_ID_HEADER, Constants.PARSE_APPLICATION_ID);
                request.setHeader(Constants.PARSE_REST_API_KEY_HEADER, Constants.PARSE_REST_API_KEY);
                request.setHeader(Constants.PARSE_CONTENT_TYPE_HEADER, Constants.PARSE_CONTENT_TYPE_JSON_OBJ);
                ResultDebtors base = setDataDebtors(photo, phone, master, email, name, surname);
                gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                String req = gson.toJson(base);
                byte[] bytes = req.getBytes();
                request.setEntity(new ByteArrayEntity(bytes));
                httpResponse = client.execute(request);
                responseCode = httpResponse.getStatusLine().getStatusCode();
                message = httpResponse.getStatusLine().getReasonPhrase();
                entity = httpResponse.getEntity();
                if (entity != null) {
                    instream = entity.getContent();
                    response = convertStreamToString(instream);
                    instream.close();
                }
                gson = new Gson();
                BaseDebtors baseDebtors = gson.fromJson(response, BaseDebtors.class);
                intent = new Intent().putExtra(Constants.PARAM_RESULT, responseCode);

                intent.putExtra(Constants.PARAM_OBJECT_ID, baseDebtors.objectId);
                intent.putExtra(Constants.PARAM_ID, id);
                pi.send(MainService.this, MainActivity.RESULT_OK, intent);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            stopService();
        }
    }

    class ThreadDeleteDebts extends BaseThread {

        String objectId;

        ThreadDeleteDebts(int startId, PendingIntent pi, String objectId) {
            this.startId = startId;
            this.pi = pi;
            this.objectId = objectId;
        }

        @Override
        public void run() {
            super.run();
            try {
                url = "https://api.parse.com/1/classes/Debts/" + objectId;
                HttpDelete request = new HttpDelete(url);
                request.setHeader(Constants.PARSE_APPLICATION_ID_HEADER, Constants.PARSE_APPLICATION_ID);
                request.setHeader(Constants.PARSE_REST_API_KEY_HEADER, Constants.PARSE_REST_API_KEY);
                httpResponse = client.execute(request);
                responseCode = httpResponse.getStatusLine().getStatusCode();
                message = httpResponse.getStatusLine().getReasonPhrase();
                entity = httpResponse.getEntity();
                if (entity != null) {
                    instream = entity.getContent();
                    response = convertStreamToString(instream);
                    instream.close();
                }
                intent.putExtra(Constants.PARAM_RESULT, responseCode);
                pi.send(MainService.this, MainActivity.RESULT_OK, intent);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            stopService();
        }
    }

    class ThreadDeleteDebtors extends BaseThread {

        String objectId;

        ThreadDeleteDebtors(int startId, PendingIntent pi, String objectId) {
            this.startId = startId;
            this.pi = pi;
            this.objectId = objectId;
        }

        @Override
        public void run() {
            try {
                super.run();
                url = "https://api.parse.com/1/classes/Debtors/" + objectId;
                HttpDelete request = new HttpDelete(url);
                request.setHeader(Constants.PARSE_APPLICATION_ID_HEADER, Constants.PARSE_APPLICATION_ID);
                request.setHeader(Constants.PARSE_REST_API_KEY_HEADER, Constants.PARSE_REST_API_KEY);
                httpResponse = client.execute(request);
                responseCode = httpResponse.getStatusLine().getStatusCode();
                message = httpResponse.getStatusLine().getReasonPhrase();
                entity = httpResponse.getEntity();
                if (entity != null) {
                    instream = entity.getContent();
                    response = convertStreamToString(instream);
                    instream.close();
                }
                intent.putExtra(Constants.PARAM_RESULT, responseCode);
                pi.send(MainService.this, MainActivity.RESULT_OK, intent);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            stopService();
        }
    }

    class ThreadUpdateDebts extends BaseThread {

        Debts debts;

        ThreadUpdateDebts(Debts debts, PendingIntent pi, int startId) {
            this.debts = debts;
            this.pi = pi;
            this.startId = startId;
        }

        @Override
        public void run() {
            super.run();
            int amount = debts.getAmount();
            String date = debts.getDate();
            String term = debts.getTerms();
            String debtorsId = debts.getDebtors_id();
            long did = debts.getDid();
            String master = debts.getMaster();
            String path = debts.getPhoto();
            String objectId = debts.getObjectId();
            boolean type = debts.isType();
            intent.putExtra(Constants.PARAM_PATH, path);
            if(!TextUtils.isEmpty(path)){
                if(!path.contains("http://")){
                    try{
                        String fName = nameGenerator();
                        url = "https://api.parse.com/1/files/" + fName + ".jpg";
                        HttpPost request = new HttpPost(url);
                        String s = path.replace("file://", "");
                        InputStream is = new FileInputStream(new File(s));//path));
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        byte[] b = new byte[1024];
                        int bytesRead;
                        while((bytesRead = is.read(b))!= -1){
                            bos.write(b, 0, bytesRead);
                        }
                        byte[] bytes = bos.toByteArray();
                        request.setHeader(Constants.PARSE_APPLICATION_ID_HEADER, Constants.PARSE_APPLICATION_ID);
                        request.setHeader(Constants.PARSE_REST_API_KEY_HEADER, Constants.PARSE_REST_API_KEY);
                        request.setHeader(Constants.PARSE_SESSION_HEADER, Constants.SESSION);
                        request.setHeader(Constants.PARSE_CONTENT_TYPE_HEADER, Constants.PARSE_CONTENT_TYPE_JPEG);
                        request.setEntity(new ByteArrayEntity(bytes));
                        httpResponse = client.execute(request);
                        responseCode = httpResponse.getStatusLine().getStatusCode();
                        message = httpResponse.getStatusLine().getReasonPhrase();
                        entity = httpResponse.getEntity();
                        if (entity != null) {
                            instream = entity.getContent();
                            response = convertStreamToString(instream);
                            instream.close();
                        }
                        if(responseCode == Constants.MESSAGE_CREATED){
                            BasePhoto basePhoto = gson.fromJson(response, BasePhoto.class);
                            path = basePhoto.url;
                            intent.putExtra(Constants.PARAM_PATH, basePhoto.url);
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (ClientProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }else{
                path = "";
            }
            try {
                url = "https://api.parse.com/1/classes/Debts/" + objectId;
                HttpPut request = new HttpPut(url);
                request.setHeader(Constants.PARSE_APPLICATION_ID_HEADER, Constants.PARSE_APPLICATION_ID);
                request.setHeader(Constants.PARSE_REST_API_KEY_HEADER, Constants.PARSE_REST_API_KEY);
                request.setHeader(Constants.PARSE_CONTENT_TYPE_HEADER, Constants.PARSE_CONTENT_TYPE_JSON_OBJ);
                ResultDebts base = setDataDebts(master, type, amount, date, path, debtorsId, term, did);
                gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                String req = gson.toJson(base);
                byte[] bytes = req.getBytes();
                request.setEntity(new ByteArrayEntity(bytes));
                httpResponse = client.execute(request);
                responseCode = httpResponse.getStatusLine().getStatusCode();
                message = httpResponse.getStatusLine().getReasonPhrase();
                entity = httpResponse.getEntity();
                if (entity != null) {
                    instream = entity.getContent();
                    response = convertStreamToString(instream);
                    instream.close();
                }
                intent.putExtra(Constants.PARAM_RESULT, responseCode);
                intent.putExtra(Constants.PARAM_ID, debts.getId());
                pi.send(MainService.this, MainActivity.RESULT_OK, intent);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            stopService();
        }
    }

    class ThreadUpdateDebtors extends BaseThread {

        Debtors debtors;

        ThreadUpdateDebtors(Debtors debtors, PendingIntent pi, int startId) {
            this.debtors = debtors;
            this.pi = pi;
            this.startId = startId;
        }

        @Override
        public void run() {
            super.run();
            String path = debtors.getPhoto();
            String phone = debtors.getPhoneNumber();
            String master = debtors.getMaster();
            String email = debtors.getEmail();
            String name = debtors.getName();
            String surname = debtors.getSecondName();
            String objectId = debtors.getObjectId();
            intent.putExtra(Constants.PARAM_PATH, path);
            if(!TextUtils.isEmpty(path)){
                if(!path.contains("http://")){
                    try{
                        String fName = nameGenerator();
                        url = "https://api.parse.com/1/files/" + fName + ".jpg";
                        HttpPost request = new HttpPost(url);
                        String s = path.replace("file://", "");
                        InputStream is = new FileInputStream(new File(s));
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        byte[] b = new byte[1024];
                        int bytesRead;
                        while((bytesRead = is.read(b))!= -1){
                            bos.write(b, 0, bytesRead);
                        }
                        byte[] bytes = bos.toByteArray();
                        request.setHeader(Constants.PARSE_APPLICATION_ID_HEADER, Constants.PARSE_APPLICATION_ID);
                        request.setHeader(Constants.PARSE_REST_API_KEY_HEADER, Constants.PARSE_REST_API_KEY);
                        request.setHeader(Constants.PARSE_SESSION_HEADER, Constants.SESSION);
                        request.setHeader(Constants.PARSE_CONTENT_TYPE_HEADER, Constants.PARSE_CONTENT_TYPE_JPEG);
                        request.setEntity(new ByteArrayEntity(bytes));
                        httpResponse = client.execute(request);
                        responseCode = httpResponse.getStatusLine().getStatusCode();
                        message = httpResponse.getStatusLine().getReasonPhrase();
                        entity = httpResponse.getEntity();
                        if (entity != null) {
                            instream = entity.getContent();
                            response = convertStreamToString(instream);
                            instream.close();
                        }
                        if(responseCode == Constants.MESSAGE_CREATED){
                            BasePhoto basePhoto = gson.fromJson(response, BasePhoto.class);
                            path = basePhoto.url;
                            intent.putExtra(Constants.PARAM_PATH, basePhoto.url);
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (ClientProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                path = "";
            }
            try {
                url = "https://api.parse.com/1/classes/Debtors/" + objectId;
                HttpPut request = new HttpPut(url);
                request.setHeader(Constants.PARSE_APPLICATION_ID_HEADER, Constants.PARSE_APPLICATION_ID);
                request.setHeader(Constants.PARSE_REST_API_KEY_HEADER, Constants.PARSE_REST_API_KEY);
                request.setHeader(Constants.PARSE_CONTENT_TYPE_HEADER, Constants.PARSE_CONTENT_TYPE_JSON_OBJ);
                ResultDebtors base = setDataDebtors(path, phone, master, email, name, surname);
                gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                String req = gson.toJson(base);
                byte[] bytes = req.getBytes();
                request.setEntity(new ByteArrayEntity(bytes));
                httpResponse = client.execute(request);
                responseCode = httpResponse.getStatusLine().getStatusCode();
                message = httpResponse.getStatusLine().getReasonPhrase();
                entity = httpResponse.getEntity();
                if (entity != null) {
                    instream = entity.getContent();
                    response = convertStreamToString(instream);
                    instream.close();
                }
                intent.putExtra(Constants.PARAM_RESULT, responseCode);
                intent.putExtra(Constants.PARAM_ID, debtors.getId());
                pi.send(MainService.this, MainActivity.RESULT_OK, intent);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class ThreadSyncDebts extends BaseThread {

        public ThreadSyncDebts(int startId, PendingIntent pi) {
            this.startId = startId;
            this.pi = pi;
        }

        public void run() {
            try {
                super.run();
                url = url + "classes/Debts?where=" + URLEncoder.encode("{\"master\":\"" + Constants.MASTER + "\"}", "UTF-8");
                HttpGet request = new HttpGet(url);
                request.setHeader(Constants.PARSE_APPLICATION_ID_HEADER, Constants.PARSE_APPLICATION_ID);
                request.setHeader(Constants.PARSE_REST_API_KEY_HEADER, Constants.PARSE_REST_API_KEY);

                httpResponse = client.execute(request);
                responseCode = httpResponse.getStatusLine().getStatusCode();
                message = httpResponse.getStatusLine().getReasonPhrase();
                entity = httpResponse.getEntity();
                if (entity != null) {
                    instream = entity.getContent();
                    response = convertStreamToString(instream);
                    instream.close();
                }
                intent = new Intent();
                intent.putExtra(Constants.PARAM_RESULT, responseCode);
                intent.putExtra(Constants.PARAM_OUT_STRING, response);
                pi.send(MainService.this, MainActivity.RESULT_OK, intent);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            stopService();
        }
    }

    class ThreadSyncDebtors extends BaseThread {

        public ThreadSyncDebtors(int startId, PendingIntent pi) {
            this.startId = startId;
            this.pi = pi;
        }

        public void run() {
            try {
                super.run();
                url = url + "classes/Debtors?where=" + URLEncoder.encode("{\"master\":\"" + Constants.MASTER + "\"}", "UTF-8");
                HttpGet request = new HttpGet(url);
                request.setHeader(Constants.PARSE_APPLICATION_ID_HEADER, Constants.PARSE_APPLICATION_ID);
                request.setHeader(Constants.PARSE_REST_API_KEY_HEADER, Constants.PARSE_REST_API_KEY);
                httpResponse = client.execute(request);
                responseCode = httpResponse.getStatusLine().getStatusCode();
                message = httpResponse.getStatusLine().getReasonPhrase();
                entity = httpResponse.getEntity();
                if (entity != null) {
                    instream = entity.getContent();
                    response = convertStreamToString(instream);
                    instream.close();
                }
                intent = new Intent();
                intent.putExtra(Constants.PARAM_RESULT, responseCode);
                intent.putExtra(Constants.PARAM_OUT_STRING, response);
                pi.send(MainService.this, MainActivity.RESULT_OK, intent);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            stopService();
        }
    }

    class ThreadPhotoDebtors extends BaseThread{

        String name;
        String surname;
        String phone;
        String email;
        String photo;
        String master;
        String path;

        public ThreadPhotoDebtors(int startId, PendingIntent pi, String name, String surname,
                                  String phone, String email, String photo, String master, long id,
                                  String path) {
            this.startId = startId;
            this.pi = pi;
            this.name = name;
            this.surname = surname;
            this.phone = phone;
            this.email = email;
            this.photo = photo;
            this.master = master;
            this.id = id;
            this.path = path;
        }

        @Override
        public void run() {
            try {
                super.run();
                String fName = nameGenerator();
                url = url + "files/" + fName + ".jpg";
                HttpPost request = new HttpPost(url);
                String s = path.replace("file://", "");
                InputStream is;
                if(!s.contains("content://")) {
                    is = new FileInputStream(new File(s));//path));
                } else {
                    is = getApplicationContext().getContentResolver().openInputStream(Uri.parse(s));
                }
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                byte[] b = new byte[1024];
                int bytesRead;
                while((bytesRead = is.read(b))!= -1){
                    bos.write(b, 0, bytesRead);
                }
                byte[] bytes = bos.toByteArray();

                request.setHeader(Constants.PARSE_APPLICATION_ID_HEADER, Constants.PARSE_APPLICATION_ID);
                request.setHeader(Constants.PARSE_REST_API_KEY_HEADER, Constants.PARSE_REST_API_KEY);
                request.setHeader(Constants.PARSE_SESSION_HEADER, Constants.SESSION);
                request.setHeader(Constants.PARSE_CONTENT_TYPE_HEADER, Constants.PARSE_CONTENT_TYPE_JPEG);
                request.setEntity(new ByteArrayEntity(bytes));
                httpResponse = client.execute(request);
                responseCode = httpResponse.getStatusLine().getStatusCode();
                message = httpResponse.getStatusLine().getReasonPhrase();
                entity = httpResponse.getEntity();
                if (entity != null) {
                    instream = entity.getContent();
                    response = convertStreamToString(instream);
                    instream.close();
                }
                if(responseCode == Constants.MESSAGE_CREATED){
                    BasePhoto basePhoto = gson.fromJson(response, BasePhoto.class);
                    intent.putExtra(Constants.PARAM_ID, id);
                    intent.putExtra(Constants.PARAM_PATH, basePhoto.url);
                    client = new DefaultHttpClient();
                    url = "https://api.parse.com/1/classes/Debtors";
                    request = new HttpPost(url);
                    request.setHeader(Constants.PARSE_APPLICATION_ID_HEADER, Constants.PARSE_APPLICATION_ID);
                    request.setHeader(Constants.PARSE_REST_API_KEY_HEADER, Constants.PARSE_REST_API_KEY);
                    request.setHeader(Constants.PARSE_CONTENT_TYPE_HEADER, Constants.PARSE_CONTENT_TYPE_JSON_OBJ);
                    ResultDebtors base = setDataDebtors(basePhoto.url
                            , phone, master, email, name, surname);
                    gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                    String req = gson.toJson(base);
                    bytes = req.getBytes();
                    request.setEntity(new ByteArrayEntity(bytes));
                    httpResponse = client.execute(request);
                    responseCode = httpResponse.getStatusLine().getStatusCode();
                    message = httpResponse.getStatusLine().getReasonPhrase();
                    entity = httpResponse.getEntity();
                    if (entity != null) {
                        instream = entity.getContent();
                        response = convertStreamToString(instream);
                        instream.close();
                    }
                    gson = new Gson();
                    BaseDebtors baseDebtors = gson.fromJson(response, BaseDebtors.class);
                    intent.putExtra(Constants.PARAM_OBJECT_ID, baseDebtors.objectId);
                    intent.putExtra(Constants.PARAM_RESULT, responseCode);
                    pi.send(MainService.this, MainActivity.RESULT_OK, intent);
                }
            } catch (ClientProtocolException e)  {
                client.getConnectionManager().shutdown();
                e.printStackTrace();
            } catch (IOException e) {
                client.getConnectionManager().shutdown();
                e.printStackTrace();
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }
            stopService();
        }
    }

    class ThreadPhotoDebts extends BaseThread{

        boolean type;
        int amount;
        String debtorsId;
        String date;
        String terms;
        String photo;
        String master;
        String path;
        long did;
        long left;

        public ThreadPhotoDebts(int startId, PendingIntent pi, boolean type, String date,
                                int amount, String terms, String debtorsId, String photo,
                                String master, long id, long did, String path, long left) {
            this.startId = startId;
            this.pi = pi;
            this.amount = amount;
            this.debtorsId = debtorsId;
            this.date = date;
            this.terms = terms;
            this.photo = photo;
            this.master = master;
            this.type = type;
            this.id = id;
            this.did = did;
            this.path = path;
            this.left = left;
        }

        @Override
        public void run() {
            super.run();
            try {
                String fName = nameGenerator();
                url = url + "files/" + fName + ".jpg";
                HttpPost request = new HttpPost(url);
                String s = path.replace("file://", "");
                InputStream is = new FileInputStream(new File(s));
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                byte[] b = new byte[1024];
                int bytesRead;
                while((bytesRead = is.read(b))!= -1){
                    bos.write(b, 0, bytesRead);
                }
                byte[] bytes = bos.toByteArray();
                request.setHeader(Constants.PARSE_APPLICATION_ID_HEADER, Constants.PARSE_APPLICATION_ID);
                request.setHeader(Constants.PARSE_REST_API_KEY_HEADER, Constants.PARSE_REST_API_KEY);
                request.setHeader(Constants.PARSE_SESSION_HEADER, Constants.SESSION);
                request.setHeader(Constants.PARSE_CONTENT_TYPE_HEADER, Constants.PARSE_CONTENT_TYPE_JPEG);
                request.setEntity(new ByteArrayEntity(bytes));
                httpResponse = client.execute(request);
                responseCode = httpResponse.getStatusLine().getStatusCode();
                message = httpResponse.getStatusLine().getReasonPhrase();
                entity = httpResponse.getEntity();
                if (entity != null) {
                    instream = entity.getContent();
                    response = convertStreamToString(instream);
                    instream.close();
                }
                if(responseCode == Constants.MESSAGE_CREATED){
                    BasePhoto basePhoto = gson.fromJson(response, BasePhoto.class);
                    intent.putExtra(Constants.PARAM_ID, id);
                    intent.putExtra(Constants.PARAM_PATH, basePhoto.url);
                    client = new DefaultHttpClient();
                    url = "https://api.parse.com/1/classes/Debts";
                    request = new HttpPost(url);
                    request.setHeader(Constants.PARSE_APPLICATION_ID_HEADER, Constants.PARSE_APPLICATION_ID);
                    request.setHeader(Constants.PARSE_REST_API_KEY_HEADER, Constants.PARSE_REST_API_KEY);
                    request.setHeader(Constants.PARSE_CONTENT_TYPE_HEADER, Constants.PARSE_CONTENT_TYPE_JSON_OBJ);
                    ResultDebts base = setDataDebts(master, type, amount, date, basePhoto.url, debtorsId, terms, did);
                    gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                    String req = gson.toJson(base);
                    bytes = req.getBytes();
                    request.setEntity(new ByteArrayEntity(bytes));
                    httpResponse = client.execute(request);
                    responseCode = httpResponse.getStatusLine().getStatusCode();
                    message = httpResponse.getStatusLine().getReasonPhrase();
                    entity = httpResponse.getEntity();
                    if (entity != null) {
                        instream = entity.getContent();
                        response = convertStreamToString(instream);
                        instream.close();
                    }
                    gson = new Gson();
                    BaseDebtors baseDebtors = gson.fromJson(response, BaseDebtors.class);
                    intent.putExtra(Constants.PARAM_OBJECT_ID, baseDebtors.objectId);
                    intent.putExtra(Constants.PARAM_RESULT, responseCode);
                    intent.putExtra(Constants.PARAM_TIME, left);
                    pi.send(MainService.this, MainActivity.RESULT_OK, intent);
                }
            } catch (ClientProtocolException e)  {
                client.getConnectionManager().shutdown();
                e.printStackTrace();
            } catch (IOException e) {
                client.getConnectionManager().shutdown();
                e.printStackTrace();
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }
            stopService();
        }
    }

    protected ResultDebts setDataDebts(String master, boolean type, int amount, String date, String photo, String debtorsId, String terms, long did){
        ResultDebts base = new ResultDebts();
        base.master = master;
        base.type = type;
        base.amount = amount;
        base.date = date;
        base.debtors_id = debtorsId;
        base.photo = photo;
        base.terms = terms;
        base.did = did;
        return base;
    }

    protected ResultDebtors setDataDebtors(String photo, String phone, String master, String email, String name, String surname){
        ResultDebtors base = new ResultDebtors();
        base.photo = photo;
        base.phone = phone;
        base.master = master;
        base.email = email;
        base.name = name;
        base.surname = surname;
        return base;
    }
}