package com.example.debtors.app.gson.base;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Богдан on 27.05.14.
 */
public class BasePhoto {

    @SerializedName("url")
    public String url = null;

    @SerializedName("name")
    public String name = null;

}
