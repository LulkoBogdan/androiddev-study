package com.example.debtors.app.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.debtors.app.MainActivity;
import com.example.debtors.app.R;
import com.example.debtors.app.common.Constants;
import com.example.debtors.app.common.DBHelper;
import com.example.debtors.app.common.Debts;
import com.example.debtors.app.common.MyCursorLoader;
import com.example.debtors.app.helpers.KeyboardHelper;
import com.example.debtors.app.helpers.Typefaces;
import com.example.debtors.app.services.MainService;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

import java.io.File;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by androiddev9 on 21.05.14.
 */
public class ModifyDebtsFragment extends Fragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    final int REQUEST_CODE_PHOTO_DEBT = 2;
    final int RESULT_LOAD_IMAGE = 4;

    SimpleCursorAdapter scSpinner;
    Button btnOk, btnCancel;
    EditText etAmount;
    TextView tvBeginDate, tvEndDate, tvAvailable;
    Spinner spinner, spinnerType;
    ImageView imageView;
    DBHelper dbDebts;
    Debts debt;
    Debts debts;
    String path = null;
    String bufPath = null;
    File fileSave, directory;
    long id;
    String fromSpinner[];
    int toSpinner[];
    Intent intent;
    String[] type;
    int years = 1900;
    int months = 1;
    int days = 1;
    int begYears, begMonths, begDays, endYears, endMonths, endDays;
    DisplayImageOptions options;
    boolean mode = false;
    Bundle bundle;
    MDFDialog mdfDialog;

    public ModifyDebtsFragment(boolean mode) {
        this.mode = mode;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.modify_debts, container, false);
        type = new String[] {getString(R.string.money), getString(R.string.goods)};
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(android.R.drawable.button_onoff_indicator_on)
                .showImageForEmptyUri(android.R.drawable.ic_menu_close_clear_cancel)
                .showImageOnFail(android.R.drawable.btn_dialog)
                .resetViewBeforeLoading(false)
                .cacheInMemory(false)
                .cacheOnDisc(true)
                .considerExifParams(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new SimpleBitmapDisplayer())
                .handler(new Handler())
                .build();

        ActionBarActivity actionBarActivity = (ActionBarActivity) getActivity();
        ActionBar actionBar = actionBarActivity.getSupportActionBar();
        actionBar.setTitle(R.string.ab_modify_debts);
        actionBar.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_holo_dark);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        btnCancel = (Button) view.findViewById(R.id.buttonAddDebtsCancel);
        btnOk = (Button) view.findViewById(R.id.buttonAddDebtsOk);
        etAmount = (EditText) view.findViewById(R.id.editTextAddDebtsAmount);
        tvBeginDate = (TextView) view.findViewById(R.id.textViewDateBegin);
        tvEndDate = (TextView) view.findViewById(R.id.textViewDAteEnd);
        tvAvailable = (TextView) view.findViewById(R.id.textViewAvailable);
        imageView = (ImageView) view.findViewById(R.id.imageViewDebtPhoto);
        spinner = (Spinner) view.findViewById(R.id.spinnerAddDebtsId);
        btnCancel.setOnClickListener(this);
        btnOk.setOnClickListener(this);
        imageView.setOnClickListener(this);
        tvBeginDate.setOnClickListener(this);
        tvEndDate.setOnClickListener(this);
        imageView.setEnabled(false);
        Typeface font = Typefaces.get(getActivity(), "8-BIT");
        btnOk.setTypeface(font);
        btnCancel.setTypeface(font);
        dbDebts = DBHelper.getInstance(getActivity());
        createDirectory();
        fromSpinner = new String[] {DBHelper.KEY_NAME, DBHelper.KEY_SECOND_NAME};
        toSpinner = new int[] {R.id.textViewSpinnerName, R.id.textViewSpinnerSurname};
        scSpinner = new SimpleCursorAdapter(getActivity(), R.layout.item_spinner, null, fromSpinner, toSpinner, 0);
        getActivity().getSupportLoaderManager().initLoader(1, null, this);
        spinner.setAdapter(scSpinner);
        spinnerType = (Spinner) view.findViewById(R.id.spinnerDebtorType);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, type);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType.setAdapter(adapter);
        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    imageView.setEnabled(false);
                    tvAvailable.setText(R.string.not_available);
                    bufPath = path;
                    path = null;
                    ImageLoader imageLoader = ImageLoader.getInstance();
                    imageLoader.displayImage(path, imageView, options);
                }else if(position == 1) {
                    if(bufPath != null) {
                        path = bufPath;
                        String buf = "file://";
                        buf = buf.concat(path);
                        ImageLoader imageLoader = ImageLoader.getInstance();
                        imageLoader.displayImage(buf, imageView, options);
                    }
                    imageView.setEnabled(true);
                    tvAvailable.setText(R.string.available);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        Calendar calendar = Calendar.getInstance();
        begYears = endYears = years = calendar.get(Calendar.YEAR);
        months = calendar.get(Calendar.MONTH)+1;
        begMonths = endMonths= months;
        begDays = days = calendar.get(Calendar.DATE);
        endDays = days + 1;
        tvBeginDate.setText("" + begDays + "." + begMonths + "." + begYears);
        tvEndDate.setText("" + endDays + "." + endMonths + "." +endYears);
        if(mode){
            btnOk.setText(R.string.update_debt);
            new AsyncTask<Void, Void, Debts>(){
                @Override
                protected Debts doInBackground(Void... params) {
                    return dbDebts.getDebts(dbDebts.MODIFY_ID);
                }
                @Override
                protected void onPostExecute(Debts debt) {
                    super.onPostExecute(debt);
                    debts = debt;
                    SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                    Calendar calendar = Calendar.getInstance();
                    try {
                        calendar.setTime(format.parse(debt.getDate()));
                        begDays = calendar.get(Calendar.DAY_OF_MONTH);
                        begMonths = calendar.get(Calendar.MONTH) + 1;
                        begYears = calendar.get(Calendar.YEAR);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    try {
                        calendar.setTime(format.parse(debt.getTerms()));
                        endDays = calendar.get(Calendar.DAY_OF_MONTH);
                        endMonths = calendar.get(Calendar.MONTH) + 1;
                        endYears = calendar.get(Calendar.YEAR);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    tvBeginDate.setText(debt.getDate());
                    tvEndDate.setText(debt.getTerms());
                    if (debt.isType()){
                        spinnerType.setSelection(1);
                    }else{
                        spinnerType.setSelection(0);
                    }
                    new AsyncTask<Long, Void, Integer>(){
                        @Override
                        protected Integer doInBackground(Long... params) {
                            Cursor cursor = dbDebts.getAllDataDebtors(Constants.MASTER);
                            long did = params[0];
                            int i = 0;
                            if(cursor!=null){
                                if(cursor.moveToFirst()){
                                    do{
                                        i++;
                                        long di = cursor.getLong(cursor.getColumnIndex(DBHelper.KEY_ID));
                                        Log.d("MyLogs", "did = " + did + " id = "+ di + " i = " + i);
                                        if(di == did){
                                            break;
                                        }
                                    }while (cursor.moveToNext());
                                }
                            }
                            return i;
                        }

                        @Override
                        protected void onPostExecute(Integer integer) {
                            super.onPostExecute(integer);
                            spinner.setSelection(integer - 1);
                        }
                    }.execute(debt.getDid());
                    etAmount.setText(String.valueOf(debt.getAmount()));
                    path = debt.getPhoto();
                    if(!TextUtils.isEmpty(debt.getPhoto())){
                        imageView.setEnabled(true);
                        ImageLoader imageLoader = ImageLoader.getInstance();
                        imageLoader.displayImage(path, imageView, options);
                    }
                }
            }.execute();
        }
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mdfDialog = (MDFDialog) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement MDFDialog");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getSupportLoaderManager().getLoader(1).forceLoad();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonAddDebtsCancel:
                KeyboardHelper.hideKeyBoard(getActivity(), etAmount);
                getActivity().onBackPressed();
                break;
            case R.id.buttonAddDebtsOk:
                onOkClick();
                break;
            case R.id.textViewDateBegin:
                onDateBeginClick();
                break;
            case R.id.textViewDAteEnd:
                onDateEndClick();
                break;
            case R.id.imageViewDebtPhoto:
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.photo_source);
                builder.setMessage(R.string.photo_type);
                builder.setIcon(android.R.drawable.ic_dialog_info);
                builder.setPositiveButton(R.string.make_photo, myClickListener);
                builder.setNegativeButton(R.string.from_gallery, myClickListener);
                builder.setNeutralButton(R.string.cancel, myClickListener);
                builder.setCancelable(true);
                AlertDialog dialog = builder.create();
                dialog.show();
                break;
        }
    }

    DialogInterface.OnClickListener myClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    fileSave = generateFileUri();
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileSave));
                    startActivityForResult(intent, REQUEST_CODE_PHOTO_DEBT);
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, RESULT_LOAD_IMAGE);
                    break;
                case Dialog.BUTTON_NEUTRAL:
                    path = null;
                    ImageLoader imageLoader = ImageLoader.getInstance();
                    imageLoader.displayImage(path, imageView, options);
                    break;
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE_PHOTO_DEBT){
            if(resultCode == MainActivity.RESULT_OK){
                path = fileSave.getAbsolutePath();
                String buf = "file://";
                buf = buf.concat(path);
                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.displayImage(buf, imageView, options);
            }
        }else if(requestCode == RESULT_LOAD_IMAGE){
            if(resultCode == MainActivity.RESULT_OK){
                if(data != null){
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    if(cursor!=null){
                        if(cursor.moveToFirst()){
                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            String picturePath = cursor.getString(columnIndex);
                            cursor.close();
                            path = picturePath;
                            String buf = "file://";
                            buf = buf.concat(path);
                            ImageLoader imageLoader = ImageLoader.getInstance();
                            imageLoader.displayImage(buf, imageView, options);
                        }
                        cursor.close();
                    }
                }
            }
        }
    }

    private File generateFileUri() {
        File file;
        file = new File(directory.getPath() + "/" + "photo_" + System.currentTimeMillis() + ".jpg");
        return file;
    }

    private void createDirectory() {
        directory = new File(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"FolderDebts");
        if (!directory.exists())
            directory.mkdirs();
    }

    private void onOkClick(){
        boolean type = false;
        boolean trouble = false;
        Cursor cursor;
        String id_debtors = "";
        long did = 0;
        int am = 0;
        if(spinnerType.getSelectedItemPosition() == 0){
            //Money
            type = false;
        }else if(spinnerType.getSelectedItemPosition() == 1){
            //Goods
            type = true;
        }
        String amount = etAmount.getText().toString();
        if(TextUtils.isEmpty(amount)){
            Toast.makeText(getActivity(), R.string.empty_amount, Toast.LENGTH_SHORT).show();
            trouble = true;
        } else {
            try {
                am = Integer.parseInt(amount);
                if(am <= 0){
                    trouble = true;
                    Toast.makeText(getActivity(), R.string.amount_wrong_value, Toast.LENGTH_SHORT).show();
                }
            }catch (NumberFormatException e){
                Toast.makeText(getActivity(), R.string.error_in_amount, Toast.LENGTH_SHORT).show();
                trouble = true;
            }
        }
        if(spinner.getSelectedItem() != null && !trouble){
            cursor = (Cursor) spinner.getSelectedItem();
            id_debtors = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_OBJECT_ID));
            did = cursor.getLong(cursor.getColumnIndex(DBHelper.KEY_ID));
        } else {
            Toast.makeText(getActivity(), R.string.empty_spinner, Toast.LENGTH_SHORT).show();
            trouble = true;
        }
        if(!trouble){
            KeyboardHelper.hideKeyBoard(getActivity(), etAmount);
            debt = new Debts();
            debt.setMaster(Constants.MASTER);
            debt.setDebtors_id(id_debtors);
            debt.setDid(did);
            debt.setTerms(tvEndDate.getText().toString());
            debt.setAmount(am);
            debt.setDate(tvBeginDate.getText().toString());
            debt.setType(type);
            debt.setPhoto(path);
            debt.setUploaded(false);
            Log.d("MyLogs", "ModifyDebtsFragment: path = " + path + ";   debt.getPhoto() = " + debt.getPhoto());
            GregorianCalendar gc = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
            gc.clear();
            gc.set(endYears, endMonths, endDays);
            long left = gc.getTimeInMillis();
            intent = new Intent(getActivity(), MainService.class);
            if(mode){
                debt.setId(debts.getId());
                debt.setObjectId(debts.getObjectId());
                new AsyncTask<Debts, Void, Void>(){
                    @Override
                    protected Void doInBackground(Debts... params) {
                        dbDebts.updateDebts(debt);
                        return null;
                    }
                }.execute(debt);
                mdfDialog.showProgressDialog(Constants.CODE_DIALOG_UPDATING);
                PendingIntent pendingIntent = getActivity().createPendingResult(Constants.CODE_SERVICE_UPDATE_DEBTS, intent, 0);
                intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                bundle = new Bundle();
                bundle.putSerializable(Constants.PARAM_DEBT, debt);
                intent.putExtra(Constants.PARAM_DEBT, bundle);
                intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_UPDATE_DEBTS);
                getActivity().startService(intent);
            }else{
                if(MainActivity.isNetworkConnected(getActivity())) {
                    final boolean type_ = type;
                    final long left_ = left;
                    new AsyncTask<Debts, Void, Long>() {
                        @Override
                        protected Long doInBackground(Debts... params) {
                            Debts debt = params[0];
                            return dbDebts.createDebts(debt);
                        }
                        @Override
                        protected void onPostExecute(Long aLong) {
                            super.onPostExecute(aLong);
                            if (MainActivity.isNetworkConnected(getActivity())) {
                                id = aLong;
                                bundle = new Bundle();
                                bundle.putSerializable(Constants.PARAM_DEBT, debt);
                                intent.putExtra(Constants.PARAM_DEBT, bundle);
                                intent.putExtra(Constants.PARAM_DEBTORS_ID, debt.getDebtors_id());
                                intent.putExtra(Constants.PARAM_DID, debt.getDid());
                                intent.putExtra(Constants.PARAM_ID, id);
                                intent.putExtra(Constants.PARAM_TIME, left_);
                                if (type_ && path != null) {
                                    PendingIntent pendingIntent = getActivity().createPendingResult(Constants.CODE_SERVICE_PHOTO_DEBT, intent, 0);
                                    intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_PHOTO_DEBTS);
                                    intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                                    intent.putExtra(Constants.PARAM_PATH, path);
                                    mdfDialog.showProgressDialog(Constants.CODE_DIALOG_UPLOADING);
                                    getActivity().startService(intent);
                                } else {
                                    PendingIntent pendingIntent = getActivity().createPendingResult(Constants.CODE_SERVICE_CREATE_DEBT, intent, 0);
                                    intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_CREATE_DEBTS);
                                    intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                                    mdfDialog.showProgressDialog(Constants.CODE_DIALOG_UPLOADING);
                                    getActivity().startService(intent);
                                }
                            }
                        }
                    }.execute(debt);
                }else{
                    String buf = "file://";
                    if(path!=null) {
                        buf = buf.concat(path);
                        debt.setPhoto(buf);
                    }
                    new AsyncTask<Debts, Void, Void>(){
                        @Override
                        protected Void doInBackground(Debts... params) {
                            Debts debt = params[0];
                            dbDebts.createDebts(debt);
                            return null;
                        }
                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            getActivity().onBackPressed();
                        }
                    }.execute(debt);
                }
            }
        }
    }

    private void onDateBeginClick(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                boolean wrong = false;
                if(year > years){
                    wrong = true;
                }else if(year == years){
                    if(monthOfYear+1 > months){
                        wrong = true;
                    }else if(monthOfYear+1 == months){
                        if(dayOfMonth > days){
                            wrong = true;
                        }
                    }
                }
                if(wrong){
                    Toast.makeText(getActivity(), R.string.wrong_date, Toast.LENGTH_SHORT).show();
                }else{
                    begDays = dayOfMonth;
                    begMonths = monthOfYear+1;
                    begYears = year;
                    tvBeginDate.setText("" + begDays + "." + begMonths + "." + begYears);
                }
            }
        }, begYears, begMonths-1, begDays);
        datePickerDialog.setCancelable(true);
        datePickerDialog.show();
    }

    private void onDateEndClick(){
        DatePickerDialog termPickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                boolean wrong = false;
                if(year < years){
                    wrong = true;
                }else if(year == years){
                    if(monthOfYear+1 < months){
                        wrong = true;
                    }else if(monthOfYear+1 == months){
                        if(dayOfMonth <= days){
                            wrong = true;
                        }
                    }
                }
                if(wrong){
                    Toast.makeText(getActivity(), R.string.wrong_terms, Toast.LENGTH_SHORT).show();
                }else{
                    endDays = dayOfMonth;
                    endMonths = monthOfYear+1;
                    endYears = year;
                    tvEndDate.setText("" + endDays + "." + endMonths + "." + endYears);
                }
            }
        }, endYears, endMonths-1, endDays);
        termPickerDialog.setCancelable(true);
        termPickerDialog.show();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(getActivity(), dbDebts, id);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        int id = loader.getId();
        if(id == 1)
            scSpinner.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    public interface MDFDialog{
        public void showProgressDialog(int i);
    }
}
