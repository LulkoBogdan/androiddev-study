package com.example.debtors.app.gson.base;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Богдан on 27.05.14.
 */
public class BaseDebtors {

    @SerializedName("createdAt")
    public String createdAt = null;

    @SerializedName("objectId")
    public String objectId = null;

}
