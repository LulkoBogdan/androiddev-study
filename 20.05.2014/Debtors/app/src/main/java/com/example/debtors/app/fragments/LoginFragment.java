package com.example.debtors.app.fragments;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.debtors.app.MainActivity;
import com.example.debtors.app.R;
import com.example.debtors.app.common.Constants;
import com.example.debtors.app.common.DBHelper;
import com.example.debtors.app.helpers.KeyboardHelper;
import com.example.debtors.app.services.MainService;

import java.lang.reflect.Field;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by androiddev9 on 20.05.14.
 */
public class LoginFragment extends Fragment implements View.OnClickListener{

    Button btnLogIn;
    Button btnSignUp;
    EditText etLogin;
    EditText etPassword;
    public SignUpFragment signUpFragment;
    LFDialog lfDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ActionBarActivity actionBarActivity = (ActionBarActivity) getActivity();
        ActionBar actionBar = actionBarActivity.getSupportActionBar();
        actionBar.setTitle(R.string.ab_login);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        btnLogIn = (Button) view.findViewById(R.id.buttonLoggingIn);
        btnSignUp = (Button) view.findViewById(R.id.buttonSignUp);
        etLogin = (EditText) view.findViewById(R.id.editTextLogin);
        etPassword = (EditText) view.findViewById(R.id.editTextPass);
        btnSignUp.setOnClickListener(this);
        btnLogIn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            lfDialog = (LFDialog) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement LFDialog");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonLoggingIn:
                if(!MainActivity.isNetworkConnected(getActivity())){
                    lfDialog.showAlertDialog();
                } else {
                    String login = etLogin.getText().toString();
                    String pass = etPassword.getText().toString();
                    if(TextUtils.isEmpty(login)){
                        Toast.makeText(getActivity(), R.string.login_is_empty, Toast.LENGTH_SHORT).show();
                    } else if(TextUtils.isEmpty(pass)){
                        Toast.makeText(getActivity(), R.string.password_is_empty, Toast.LENGTH_SHORT).show();
                    } else {
                        if (!searchRus(login)) {
                            Toast.makeText(getActivity(), R.string.error_in_login, Toast.LENGTH_SHORT).show();
                        } else if(login.length() > 20 || login.length() < 5){
                            Toast.makeText(getActivity(), R.string.error_in_login_length, Toast.LENGTH_SHORT).show();
                        } else if(pass.length() < 5 || pass.length() > 20){
                            Toast.makeText(getActivity(), R.string.error_in_pass, Toast.LENGTH_SHORT).show();
                        }else {
                            DBHelper.getInstance(getActivity());
                            lfDialog.showProgressDialog(Constants.CODE_DIALOG_CONNECTING);
                            Intent intent = new Intent(getActivity(), MainService.class);
                            PendingIntent pendingIntent = getActivity().createPendingResult(Constants.CODE_SERVICE_LOG_IN, intent, 0);
                            intent.putExtra(Constants.PARAM_LOGIN, login);
                            intent.putExtra(Constants.PARAM_PASS, pass);
                            intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                            intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_LOGIN);
                            getActivity().startService(intent);
                            KeyboardHelper.hideKeyBoard(getActivity(), etPassword);
                        }
                    }
                }
                break;
            case R.id.buttonSignUp:
                if(!MainActivity.isNetworkConnected(getActivity())){
                    lfDialog.showAlertDialog();
                } else {
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    signUpFragment = new SignUpFragment();
                    fragmentTransaction.replace(R.id.main_window, signUpFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
                break;
        }
    }

    static boolean searchRus (String s)
    {
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9]$");
        Matcher matcher = pattern.matcher(s);
        return !matcher.matches();
    }

    public interface LFDialog{
        public void showProgressDialog(int i);
        public void showAlertDialog();
    }
}
