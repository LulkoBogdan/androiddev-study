package com.example.debtors.app.gson.base;

import com.google.gson.annotations.Expose;

/**
 * Created by androiddev9 on 22.05.14.
 */
public class BaseForSigUp {

    @Expose
    private String username = null;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Expose
    private String password = null;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
