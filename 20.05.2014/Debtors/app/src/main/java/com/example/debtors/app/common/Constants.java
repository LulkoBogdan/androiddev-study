package com.example.debtors.app.common;

/**
 * Created by androiddev9 on 02.06.14.
 */
public class Constants {
    public static final String PARSE_APPLICATION_ID_HEADER = "X-Parse-Application-Id";
    public static final String PARSE_REST_API_KEY_HEADER = "X-Parse-REST-API-Key";
    public static final String PARSE_APPLICATION_ID = "DwqmymsCwNWU0ket7PckBaoSiRVXpzLfnjDIH7T6";
    public static final String PARSE_REST_API_KEY = "k0AmDO9yTTaJhKEFX5cWV21FxRw8CYLoADmNm5c2";
    public static final String PARSE_CONTENT_TYPE_HEADER = "Content-Type";
    public static final String PARSE_SESSION_HEADER = "X-Parse-Session-Token";
    public static final String PARSE_CONTENT_TYPE_JSON_OBJ = "application/json";
    public static final String PARSE_CONTENT_TYPE_JPEG = "image/jpeg";

    public static final int CODE_SERVICE_SIGN_UP = 100;
    public static final int CODE_SERVICE_LOG_IN = 101;
    public static final int CODE_SERVICE_CREATE_DEBTOR = 102;
    public static final int CODE_SERVICE_CREATE_DEBT = 103;
    public static final int CODE_SERVICE_SYNC_DEBTS = 104;
    public static final int CODE_SERVICE_SYNC_DEBTORS = 105;
    public static final int CODE_SERVICE_PHOTO_DEBTOR = 106;
    public static final int CODE_SERVICE_PHOTO_DEBT = 107;
    public static final int CODE_SERVICE_UPDATE_DEBTS = 108;
    public static final int CODE_SERVICE_UPDATE_DEBTORS = 109;
    public static final int CODE_SERVICE_DELETE_DEBTORS = 110;
    public static final int CODE_SERVICE_DELETE_DEBTS = 111;
    public static final String PARAM_LOGIN = "login";
    public static final String PARAM_PASS = "password";
    public static final String PARAM_PINTENT = "pendingIntent";
    public static final String PARAM_RESULT = "result";
    public static final String PARAM_OUT_STRING = "out_string";
    public static final String PARAM_MASTER = "master";
    public static final String PARAM_PHOTO = "photo";
    public static final String PARAM_DEBTORS_ID = "debtors_id";
    public static final String PARAM_SERVICE_TYPE = "service_type";
    public static final String PARAM_ID = "id";
    public static final String PARAM_OBJECT_ID = "object_id";
    public static final String PARAM_PATH = "path";
    public static final String PARAM_SESSION = "session";
    public static final String PARAM_DID = "did";
    public static final String PARAM_DEBTOR = "debtor";
    public static final String PARAM_DEBT = "debt";
    public static final String PARAM_TIME = "time";
    public static final int TYPE_LOGIN = 1;
    public static final int TYPE_SIGNUP = 2;
    public static final int TYPE_CREATE_DEBTS = 3;
    public static final int TYPE_CREATE_DEBTORS = 4;
    public static final int TYPE_SYNC_DEBTS = 5;
    public static final int TYPE_SYNC_DEBTORS = 6;
    public static final int TYPE_DELETE_DEBTS = 7;
    public static final int TYPE_DELETE_DEBTORS = 8;
    public static final int TYPE_UPDATE_DEBTS = 9;
    public static final int TYPE_UPDATE_DEBTORS = 10;
    public static final int TYPE_PHOTO_DEBTORS = 11;
    public static final int TYPE_PHOTO_DEBTS = 12;
    public static final int MESSAGE_OK = 200;
    public static final int MESSAGE_CREATED = 201;
    public static final int MESSAGE_IN_USE = 400;
    public static final String PREF_LOGIN = "login";
    public static final String PREF_PASS = "password";
    public static final int CODE_DIALOG_CONNECTING = 1;
    public static final int CODE_DIALOG_UPLOADING = 2;
    public static final int CODE_DIALOG_UPDATING = 3;

    public static String MASTER;
    public static String SESSION;
}
