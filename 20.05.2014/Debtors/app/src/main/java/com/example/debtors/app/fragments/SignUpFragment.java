package com.example.debtors.app.fragments;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.debtors.app.R;
import com.example.debtors.app.common.Constants;
import com.example.debtors.app.services.MainService;

import java.lang.reflect.Field;

/**
 * Created by androiddev9 on 21.05.14.
 */
public class SignUpFragment extends Fragment implements View.OnClickListener{

    Button btnOk;
    Button btnCancel;
    EditText etPassword1;
    EditText etPassword2;
    EditText etLogin;
    SUFDialog sufDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        ActionBarActivity actionBarActivity = (ActionBarActivity) getActivity();
        ActionBar actionBar = actionBarActivity.getSupportActionBar();
        actionBar.setTitle(R.string.ab_sign_up);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        btnOk = (Button) view.findViewById(R.id.buttonSignOk);
        btnCancel = (Button) view.findViewById(R.id.buttonSignCancel);
        btnCancel.setOnClickListener(this);
        btnOk.setOnClickListener(this);
        etPassword2 = (EditText) view.findViewById(R.id.editTextSignPass2);
        etPassword1 = (EditText) view.findViewById(R.id.editTextSignPass1);
        etLogin = (EditText) view.findViewById(R.id.editTextSignLogin);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            sufDialog = (SUFDialog) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement SUFDialog");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonSignOk:
                String log = etLogin.getText().toString();
                String pass1 = etPassword1.getText().toString();
                String pass2 = etPassword2.getText().toString();
                if(TextUtils.isEmpty(log) || TextUtils.isEmpty(pass1) || TextUtils.isEmpty(pass2)){
                    Toast.makeText(getActivity(), R.string.empty_fields, Toast.LENGTH_SHORT).show();
                }else if(!LoginFragment.searchRus(log)){
                    Toast.makeText(getActivity(), R.string.unexpected_symbols, Toast.LENGTH_SHORT).show();
                }else if(log.length()<5 || log.length()>20){
                    Toast.makeText(getActivity(), R.string.error_in_login_length, Toast.LENGTH_SHORT).show();
                }else if(pass1.length()<5 || pass1.length()>20 || pass2.length()<5 || pass2.length()>20){
                    Toast.makeText(getActivity(), R.string.error_in_pass, Toast.LENGTH_SHORT).show();
                }else{
                    if(!pass1.equals(pass2)){
                        Toast.makeText(getActivity(), R.string.pass_do_not_match, Toast.LENGTH_SHORT).show();
                    } else {
                        sufDialog.showProgressDialog(Constants.CODE_DIALOG_CONNECTING);
                        Intent intent = new Intent(getActivity(), MainService.class);
                        PendingIntent pendingIntent = getActivity().createPendingResult(Constants.CODE_SERVICE_SIGN_UP, intent, 0);
                        intent.putExtra(Constants.PARAM_LOGIN, log);
                        intent.putExtra(Constants.PARAM_PASS, pass1);
                        intent.putExtra(Constants.PARAM_PINTENT, pendingIntent);
                        intent.putExtra(Constants.PARAM_SERVICE_TYPE, Constants.TYPE_SIGNUP);
                        getActivity().startService(intent);
                    }
                }
                break;
            case R.id.buttonSignCancel:
                getActivity().onBackPressed();
                break;
        }
    }

    public interface SUFDialog{
        public void showProgressDialog(int i);
    }
}
