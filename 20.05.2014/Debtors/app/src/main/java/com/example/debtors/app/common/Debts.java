package com.example.debtors.app.common;

import com.example.debtors.app.gson.base.ResultDebts;

import java.io.Serializable;

/**
 * Created by androiddev9 on 20.05.14.
 */
public class Debts implements Serializable{

    int id;
    int amount;
    long did;
    boolean type;
    boolean uploaded;
    String date;
    String terms;
    String photo;
    String debtors_id;
    String master;
    String objectId;

    public Debts() {
    }

    public Debts (ResultDebts res){
        date = res.date;
        amount = res.amount;
        master = res.master;
        photo = res.photo;
        terms = res.terms;
        objectId = res.objectId;
        uploaded = true;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }

    public long getDid() {
        return did;
    }

    public void setDid(long did) {
        this.did = did;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDebtors_id() {
        return debtors_id;
    }

    public void setDebtors_id(String debtors_id) {
        this.debtors_id = debtors_id;
    }

    public String getMaster() {
        return master;
    }

    public void setMaster(String master) {
        this.master = master;
    }
}
