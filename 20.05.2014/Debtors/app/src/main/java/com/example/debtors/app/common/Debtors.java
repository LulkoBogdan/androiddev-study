package com.example.debtors.app.common;

import com.example.debtors.app.gson.base.ResultDebtors;

import java.io.Serializable;

/**
 * Created by androiddev9 on 20.05.14.
 */
public class Debtors implements Serializable{

    int id;
    String secondName;
    String name;
    String phoneNumber;
    String email;
    String photo;
    String master;
    String objectId;
    boolean uploded;

    public Debtors() {
    }

    public Debtors (ResultDebtors res){
        photo = res.photo;
        master = res.master;
        email = res.email;
        name = res.name;
        phoneNumber = res.phone;
        secondName = res.surname;
        objectId = res.objectId;
        uploded = true;
    }

    public boolean isUploded() {
        return uploded;
    }

    public void setUploded(boolean uploded) {
        this.uploded = uploded;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getMaster() {
        return master;
    }

    public void setMaster(String master) {
        this.master = master;
    }
}
