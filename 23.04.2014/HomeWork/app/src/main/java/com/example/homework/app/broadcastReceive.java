package com.example.homework.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by androiddev9 on 23.04.14.
 */
public class broadcastReceive extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        int status = intent.getIntExtra(MainActivity.PARAM_STATUS, 0);
        if(status == MainActivity.STATUS_START){
            MainActivity.tvTask.setText("Task started");
        }
        if(status == MainActivity.STATUS_FINISH){
            int result = intent.getIntExtra(MainActivity.PARAM_RESULT, 0);
            MainActivity.tvTask.setText("Task finished, result = " + result);
        }
    }
}
