package com.example.task2.app;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View;
import android.provider.ContactsContract.CommonDataKinds.*;

public class MainActivity extends ActionBarActivity {

    final int REQUEST_CODE_PHOTO = 1;
    final int REQUEST_CODE_CONTACT = 2;
    final String KEY_CODE_DATA = "data";

    ImageView ivPhoto;
    TextView tvNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ivPhoto = (ImageView) findViewById(R.id.fotoView);
        tvNumber = (TextView) findViewById(R.id.numberView);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onGetPhotoClick(View v){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CODE_PHOTO);

    }

    public void onGetContactClick(View v){

        Intent intent = new Intent (Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        intent.setType(Phone.CONTENT_TYPE);
        startActivityForResult(intent, REQUEST_CODE_CONTACT);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        if(requestCode == REQUEST_CODE_PHOTO){
            if(resultCode == RESULT_OK){
                if(intent != null){
                    Bundle locBundle = intent.getExtras();
                    if(locBundle != null){
                        Object obj = intent.getExtras().get(KEY_CODE_DATA);
                        if(obj instanceof Bitmap){
                            Bitmap bitmap = (Bitmap) obj;
                            ivPhoto.setImageBitmap(bitmap);
                        }
                    }
                } else {
                    Toast.makeText(this, "no photo", Toast.LENGTH_SHORT);
                }
            } else {
                Toast.makeText(this, "canceled", Toast.LENGTH_SHORT);
            }
        } else if(requestCode == REQUEST_CODE_CONTACT){
            if(resultCode == RESULT_OK){
                Uri contactUri = intent.getData();
                String[] projection = {Phone.NUMBER};
                Cursor cursor = getContentResolver()
                        .query(contactUri, projection, null, null, null);
                if(cursor != null){
                    int column = cursor.getColumnIndex(Phone.NUMBER);
                    if(cursor.moveToFirst()){
                        String number = cursor.getString(column);
                        tvNumber.setText(number);
                    }
                    cursor.close();
                }
            } else {
                Toast.makeText(this, "canceled", Toast.LENGTH_SHORT);
            }
        }
    }

}
