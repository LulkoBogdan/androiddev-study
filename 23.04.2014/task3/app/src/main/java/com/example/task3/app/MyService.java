package com.example.task3.app;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by androiddev9 on 23.04.14.
 */
public class MyService extends Service{

    ExecutorService es;

    public void onCreate(){
        super.onCreate();
        es = Executors.newFixedThreadPool(1);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId){
        int time = intent.getIntExtra(MainActivity.PARAM_TIME, 1);
        MyRun myRun = new MyRun(startId, time);
        es.execute(myRun);
        return super.onStartCommand(intent, flags, startId);
    }

    class MyRun implements Runnable{

        int time;
        int startId;

        public MyRun(int startId, int time){
            this.time = time;
            this.startId = startId;
        }

        @Override
        public void run() {
            Intent intent = new Intent (MainActivity.BROADCAST_ACTION);
            try{
                intent.putExtra(MainActivity.PARAM_STATUS, MainActivity.STATUS_START);
                sendBroadcast(intent);
                TimeUnit.SECONDS.sleep(time);
                intent.putExtra(MainActivity.PARAM_STATUS, MainActivity.STATUS_FINISH);
                intent.putExtra(MainActivity.PARAM_RESULT, time*100);
                sendBroadcast(intent);
            } catch(InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
