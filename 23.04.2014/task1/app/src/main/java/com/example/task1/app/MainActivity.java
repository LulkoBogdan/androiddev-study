package com.example.task1.app;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    Button btnPhone;
    Button btnSms;
    Button btnMail;
    Button btnBrowser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSms = (Button) findViewById(R.id.button_sms);
        btnPhone = (Button) findViewById(R.id.button_phone);
        btnMail = (Button) findViewById(R.id.button_mail);
        btnBrowser = (Button) findViewById(R.id.button_browser);

        btnBrowser.setOnClickListener(this);
        btnMail.setOnClickListener(this);
        btnPhone.setOnClickListener(this);
        btnSms.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;

        switch (view.getId()){
        case R.id.button_phone:
            intent = new Intent(Intent.ACTION_DIAL);
            break;
        case R.id.button_sms:
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("sms:"));
            break;
        case R.id.button_mail:
            intent = new Intent(Intent.ACTION_SEND);
            intent.setData(Uri.parse("mailto:"));
            intent.setType("text/plain");
            break;
        case R.id.button_browser:
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http:"));
            break;
        }
        startActivity(intent);
    }
}
