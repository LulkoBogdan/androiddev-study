package com.example.mydatabase.app.fragments.modify;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mydatabase.app.DBHelper;
import com.example.mydatabase.app.R;
import com.example.mydatabase.app.classes.base.Sellers;

/**
 * Created by androiddev9 on 07.05.14.
 */
public class ModifySellers extends Fragment implements View.OnClickListener {

    String name;
    String post;
    float salary;
    int experience;
    EditText etName;
    EditText etPost;
    EditText etSalary;
    EditText etExp;
    Button add;
    DBHelper dbTrade;
    boolean type;
    int id;

    public ModifySellers(boolean type) {
        this.type = type;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_sellers, container, false);
        dbTrade = DBHelper.getInstance(getActivity());
        add = (Button) view.findViewById(R.id.buttonAddNewSellers);
        add.setOnClickListener(this);
        etName = (EditText) view.findViewById(R.id.editTextNameSellers);
        etPost = (EditText) view.findViewById(R.id.editTextPostSellers);
        etSalary = (EditText) view.findViewById(R.id.editTextSalarySellers);
        etExp = (EditText) view.findViewById(R.id.editTextExperienceSellers);
        if(type){
            add.setText("Update");
            Sellers sellers = dbTrade.getSellers(dbTrade.ID_FOR_UPDATE);
            etName.setText(sellers.getName());
            etPost.setText(sellers.getPost());
            etExp.setText(String.valueOf(sellers.getExperience()));
            etSalary.setText(String.valueOf(sellers.getSalary()));
            id = sellers.getId();
        }
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonAddNewSellers:
                String bufSalary;
                String bufExp;
                name = etName.getText().toString();
                post = etPost.getText().toString();
                bufSalary = etSalary.getText().toString();
                bufExp = etExp.getText().toString();
                if(TextUtils.isEmpty(name)){
                    Toast.makeText(getActivity(), "Name is null", Toast.LENGTH_SHORT).show();
                }
                if(TextUtils.isEmpty(post)){
                    Toast.makeText(getActivity(), "Post is null", Toast.LENGTH_SHORT).show();
                }
                try {
                    experience = Integer.parseInt(bufExp);
                    salary = Float.parseFloat(bufSalary);
                    if((experience < 0)||(experience > 60)){
                        experience = -1;
                        Toast.makeText(getActivity(), "Wrong experience: 0 < experience < 60", Toast.LENGTH_SHORT).show();
                    }
                    if((salary > 10000)||(salary < 2000)){
                        salary = -1;
                        Toast.makeText(getActivity(), "Wrong salary: 2000 < salary < 10000", Toast.LENGTH_SHORT).show();
                    }
                } catch (NumberFormatException e){
                    Toast.makeText(getActivity(), "Wrong experience or salary", Toast.LENGTH_SHORT).show();
                    salary = -1;
                    experience = -1;
                }

                if((salary > 0) &&(experience >= 0)&&(!TextUtils.isEmpty(name))&&(!TextUtils.isEmpty(post))){
                    Sellers sellers = new Sellers();
                    sellers.setSalary(salary);
                    sellers.setPost(post);
                    sellers.setExperience(experience);
                    sellers.setName(name);
                    if(type){
                        sellers.setId(id);
                        dbTrade.updateSellers(sellers);
                        Toast.makeText(getActivity(), "Updated Seller", Toast.LENGTH_SHORT).show();
                    } else {
                        dbTrade.createSellers(sellers);
                        Toast.makeText(getActivity(), "Added Seller", Toast.LENGTH_SHORT).show();
                    }
                    getFragmentManager().popBackStack();
                }
                break;
        }
    }

}
