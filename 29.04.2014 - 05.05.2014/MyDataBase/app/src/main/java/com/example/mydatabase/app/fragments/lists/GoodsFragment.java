package com.example.mydatabase.app.fragments.lists;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.mydatabase.app.DBHelper;
import com.example.mydatabase.app.R;
import com.example.mydatabase.app.MyCursorLoader;
import com.example.mydatabase.app.fragments.modify.ModifyGoods;

/**
 * Created by androiddev9 on 29.04.14.
 */
public class GoodsFragment extends Fragment implements View.OnClickListener,
        LoaderManager.LoaderCallbacks<Cursor>{

    private final int CM_DELETE_ID_GOODS = 1;
    private final int CM_UPDATE_ID_GOODS = 5;

    DBHelper dbTrade;
    public SimpleCursorAdapter scAdapterGoods;
    String fromGoods[];
    int toGoods[];

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_goods, container, false);
        dbTrade = DBHelper.getInstance(getActivity());
        Button btnNewGoods = (Button) view.findViewById(R.id.buttonAddGoods);
        ListView listView = (ListView) view.findViewById(R.id.listViewGoods);
        btnNewGoods.setOnClickListener(this);

        fromGoods = new String[] {DBHelper.KEY_NAME, DBHelper.KEY_ID, DBHelper.KEY_PRICE,
                DBHelper.KEY_AVAILABILITY};
        toGoods = new int[]{R.id.textGoodsName, R.id.textGoodsId, R.id.textGoodsPrice,
                R.id.textGoodsAvailability};
        scAdapterGoods = new SimpleCursorAdapter(getActivity(), R.layout.item_goods, null, fromGoods,
                toGoods, 0);
        getActivity().getSupportLoaderManager().initLoader(0, null, this);
        listView.setAdapter(scAdapterGoods);
        registerForContextMenu(listView);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getSupportLoaderManager().getLoader(0).forceLoad();
    }

    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        switch (v.getId()){
            case R.id.listViewGoods:
                menu.add(0, CM_DELETE_ID_GOODS, 0, R.string.delete_record);
                menu.add(0, CM_UPDATE_ID_GOODS, 0, R.string.update_record);
                break;
        }
    }
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == CM_DELETE_ID_GOODS) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();
            dbTrade.deleteGoods(acmi.id);
            getActivity().getSupportLoaderManager().getLoader(0).forceLoad();
            return true;
        }
        if (item.getItemId() == CM_UPDATE_ID_GOODS) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();
            dbTrade.ID_FOR_UPDATE = acmi.id;
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            ModifyGoods modifyGoods = new ModifyGoods(true);
            fragmentTransaction.replace(R.id.relative_layout, modifyGoods);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.buttonAddGoods:
                ModifyGoods modifyGoods = new ModifyGoods(false);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.relative_layout, modifyGoods);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(getActivity(), dbTrade, id);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        int id = loader.getId();
        if(id == 0)
            scAdapterGoods.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
