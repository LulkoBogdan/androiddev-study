package com.example.mydatabase.app.fragments.modify;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mydatabase.app.DBHelper;
import com.example.mydatabase.app.R;
import com.example.mydatabase.app.classes.base.Goods;

/**
 * Created by androiddev9 on 07.05.14.
 */
public class ModifyGoods extends Fragment implements View.OnClickListener {

    String name;
    boolean availabilityBool;
    EditText etName;
    EditText etPrice;
    CheckBox cbAvailability;
    float priceFloat;
    DBHelper dbTrade;
    boolean type = false;
    int id;

    public ModifyGoods(boolean type) {
        this.type = type;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_goods, container, false);
        dbTrade = DBHelper.getInstance(getActivity());
        Button btnAdd = (Button) view.findViewById(R.id.buttonAddNewGoods);
        etName = (EditText) view.findViewById(R.id.editTextNameGoods);
        cbAvailability = (CheckBox) view.findViewById(R.id.checkBoxAvailabilityGoods);
        etPrice = (EditText) view.findViewById(R.id.editTextPriceGoods);
        btnAdd.setOnClickListener(this);
        if(type){
            btnAdd.setText("Update");
            Goods goods = dbTrade.getGoods(dbTrade.ID_FOR_UPDATE);
            etName.setText(goods.getName());
            if(goods.isAvailability()){
                cbAvailability.setChecked(true);
            } else {
                cbAvailability.setChecked(false);
            }
            etPrice.setText(String.valueOf((long)goods.getPrice()));
            id = goods.getId();
        }
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonAddNewGoods:
                name = etName.getText().toString();
                String price = etPrice.getText().toString();
                if(cbAvailability.isChecked()){
                    availabilityBool = true;
                } else {
                    availabilityBool = false;
                }
                if(TextUtils.isEmpty(name)){
                    Toast.makeText(getActivity(), "Wrong name", Toast.LENGTH_SHORT).show();
                }
                try {
                    priceFloat = Float.parseFloat(price);
                } catch (NumberFormatException e){
                    Toast.makeText(getActivity(), "Wrong price", Toast.LENGTH_SHORT).show();
                    priceFloat = -1;
                }
                if((priceFloat > 0)&&(!TextUtils.isEmpty(name))){
                    Goods goods = new Goods();
                    goods.setName(name);
                    goods.setPrice(priceFloat);
                    goods.setAvailability(availabilityBool);
                    if(type){
                        goods.setId(id);
                        dbTrade.updateGoods(goods);
                        Toast.makeText(getActivity(), "Updated Goods", Toast.LENGTH_SHORT).show();
                    } else {
                        dbTrade.createGoods(goods);
                        Toast.makeText(getActivity(), "Added Goods", Toast.LENGTH_SHORT).show();
                    }
                    getFragmentManager().popBackStack();
                }
                break;
        }
    }
}
