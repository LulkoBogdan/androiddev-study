package com.example.mydatabase.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.mydatabase.app.fragments.lists.BuyersFragment;
import com.example.mydatabase.app.fragments.lists.GoodsFragment;
import com.example.mydatabase.app.fragments.lists.OrdersFragment;
import com.example.mydatabase.app.fragments.lists.SellersFragment;

/**
 * Created by androiddev9 on 29.04.14.
 */
public class StartFragment extends Fragment implements View.OnClickListener{

    GoodsFragment goodsFragment;
    SellersFragment sellersFragment;
    BuyersFragment buyersFragment;
    OrdersFragment ordersFragment;
    DBHelper dbTrade;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fargment_start, container, false);
        dbTrade = DBHelper.getInstance(getActivity());
        Button btnToGoods = (Button) view.findViewById(R.id.buttonGoToGoods);
        Button btnToSellers = (Button) view.findViewById(R.id.buttonGoToSellers);
        Button btnToBuyers = (Button) view.findViewById(R.id.buttonGoToBuyers);
        Button btnToOrders = (Button) view.findViewById(R.id.buttonGoToOreders);
        btnToBuyers.setOnClickListener(this);
        btnToGoods.setOnClickListener(this);
        btnToOrders.setOnClickListener(this);
        btnToSellers.setOnClickListener(this);
        goodsFragment = new GoodsFragment();
        sellersFragment = new SellersFragment();
        buyersFragment = new BuyersFragment();
        ordersFragment = new OrdersFragment();
        return view;
    }

    @Override
    public void onClick(View view) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        switch (view.getId()){
            case R.id.buttonGoToBuyers:
                fragmentTransaction.replace(R.id.relative_layout, buyersFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
            case R.id.buttonGoToSellers:
                fragmentTransaction.replace(R.id.relative_layout, sellersFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
            case R.id.buttonGoToGoods:
                fragmentTransaction.replace(R.id.relative_layout, goodsFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
            case R.id.buttonGoToOreders:
                fragmentTransaction.replace(R.id.relative_layout, ordersFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
        }
    }
}
