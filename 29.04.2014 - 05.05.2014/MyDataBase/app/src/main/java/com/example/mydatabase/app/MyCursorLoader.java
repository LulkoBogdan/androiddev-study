package com.example.mydatabase.app;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

/**
 * Created by androiddev9 on 06.05.14.
 */
public class MyCursorLoader extends CursorLoader {
    DBHelper db;
    int id;

    public MyCursorLoader(Context context, DBHelper db, int id) {
        super(context);
        this.db = db;
        this.id = id;
    }

    @Override
    public Cursor loadInBackground() {
        if(id == 0) {
            Cursor cursor = db.getAllDataGoods();
            return cursor;
        }
        if(id == 1) {
            Cursor cursor = db.getAllDataSellers();
            return cursor;
        }
        if(id == 2) {
            Cursor cursor = db.getAllDataBuyers();
            return cursor;
        }
        if(id == 3) {
            Cursor cursor = db.getAllDataOrders();
            return cursor;
        }
        if(id == 4){
            Cursor cursor = db.getAllDataGoods();
            return cursor;
        }
        if(id == 5){
            Cursor cursor = db.getAllDataSellers();
            return cursor;
        }
        if(id == 6){
            Cursor cursor = db.getAllDataBuyers();
            return cursor;
        }
        return null;
    }
}