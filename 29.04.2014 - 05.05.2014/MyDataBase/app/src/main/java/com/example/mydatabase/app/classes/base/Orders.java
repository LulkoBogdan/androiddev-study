package com.example.mydatabase.app.classes.base;

/**
 * Created by androiddev9 on 29.04.14.
 */
public class Orders {
    private int id;
    private int amount;
    private boolean delivered;
    private String address;
    private String date;
    private int id_goods;
    private int id_sellers;
    private int id_buyers;

    public Orders (int id, int amount, boolean delivered, String address, String date,
                   int id_goods, int id_sellers, int id_buyers){
        this.address = address;
        this.amount = amount;
        this.date = date;
        this.delivered = delivered;
        this.id = id;
        this.id_buyers = id_buyers;
        this.id_goods = id_goods;
        this.id_sellers = id_sellers;
    }

    public Orders (){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId_goods() {
        return id_goods;
    }

    public void setId_goods(int id_goods) {
        this.id_goods = id_goods;
    }

    public int getId_sellers() {
        return id_sellers;
    }

    public void setId_sellers(int id_sellers) {
        this.id_sellers = id_sellers;
    }

    public int getId_buyers() {
        return id_buyers;
    }

    public void setId_buyers(int id_buyers) {
        this.id_buyers = id_buyers;
    }
}
