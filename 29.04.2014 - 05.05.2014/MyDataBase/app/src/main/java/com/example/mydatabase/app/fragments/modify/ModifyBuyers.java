package com.example.mydatabase.app.fragments.modify;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mydatabase.app.DBHelper;
import com.example.mydatabase.app.R;
import com.example.mydatabase.app.classes.base.Buyers;

/**
 * Created by androiddev9 on 07.05.14.
 */
public class ModifyBuyers extends Fragment implements View.OnClickListener{

    String name;
    boolean wholesale;
    int age;
    int discount;
    EditText etName;
    EditText etAge;
    EditText etDiscount;
    CheckBox cbWholesale;
    DBHelper dbTrade;
    boolean type;
    int id;

    public ModifyBuyers(boolean type) {
        this.type = type;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_buyers, container, false);
        dbTrade = DBHelper.getInstance(getActivity());
        Button add = (Button) view.findViewById(R.id.buttonAddNewBuyers);
        add.setOnClickListener(this);
        etAge = (EditText) view.findViewById(R.id.editTextAgeBuyers);
        etDiscount = (EditText) view.findViewById(R.id.editTextDiscountBuyers);
        etName = (EditText) view.findViewById(R.id.editTextNameBuyers);
        cbWholesale = (CheckBox) view.findViewById(R.id.checkBoxBuyers);
        if(type){
            add.setText("Update");
            Buyers buyers = dbTrade.getBuyers(dbTrade.ID_FOR_UPDATE);
            if(buyers.isWholesale()){
                cbWholesale.setChecked(true);
            } else {
                cbWholesale.setChecked(false);
            }
            etName.setText(buyers.getName());
            etAge.setText(String.valueOf(buyers.getAge()));
            etDiscount.setText(String.valueOf(buyers.getDiscount()));
            id = buyers.getId();
        }
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonAddNewBuyers:
                String bufAge = etAge.getText().toString();
                String bufDiscount = etDiscount.getText().toString();
                name = etName.getText().toString();
                if(cbWholesale.isChecked()){
                    wholesale = true;
                } else {
                    wholesale = false;
                }
                try {
                    age = Integer.parseInt(bufAge);
                    discount = Integer.parseInt(bufDiscount);
                    if((age > 100)||(age < 18)){
                        age = -1;
                        Toast.makeText(getActivity(), "Wrong age", Toast.LENGTH_SHORT).show();
                    }
                    if((discount > 100)||(discount < 0)){
                        discount = -1;
                        Toast.makeText(getActivity(), "Unreal discount", Toast.LENGTH_SHORT).show();
                    }
                } catch (NumberFormatException e){
                    Toast.makeText(getActivity(), "Wrong age or discount", Toast.LENGTH_SHORT).show();
                    age = -1;
                    discount = -1;
                }
                if(TextUtils.isEmpty(name)){
                    Toast.makeText(getActivity(), "Name is NULL", Toast.LENGTH_SHORT).show();
                }
                if((age > 0)&&(discount >= 0)&&(!TextUtils.isEmpty(name))){
                    Buyers buyers = new Buyers();
                    buyers.setName(name);
                    buyers.setAge(age);
                    buyers.setDiscount(discount);
                    buyers.setWholesale(wholesale);
                    if(type){
                        buyers.setId(id);
                        dbTrade.updateBuyers(buyers);
                        Toast.makeText(getActivity(), "Updated Buyer", Toast.LENGTH_SHORT).show();
                    } else {
                        dbTrade.createBuyers(buyers);
                        Toast.makeText(getActivity(), "Added Buyer", Toast.LENGTH_SHORT).show();
                    }
                    getFragmentManager().popBackStack();
                }
                break;
        }
    }

}
