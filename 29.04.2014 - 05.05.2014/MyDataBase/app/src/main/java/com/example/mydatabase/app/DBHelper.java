package com.example.mydatabase.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.support.v4.widget.SimpleCursorAdapter;

import com.example.mydatabase.app.classes.base.Buyers;
import com.example.mydatabase.app.classes.base.Goods;
import com.example.mydatabase.app.classes.base.Orders;
import com.example.mydatabase.app.classes.base.Sellers;

import java.util.concurrent.ExecutionException;


/**
 * Created by androiddev9 on 28.04.14.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "databaseTrading2";
    // tables names
    public static final String TABLE_GOODS = "table_goods";
    public static final String TABLE_SELLERS = "table_sellers";
    public static final String TABLE_BUYERS = "table_buyers";
    public static final String TABLE_ORDERS = "table_orders";
    //common columns
    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";
    //goods columns
    public static final String KEY_PRICE = "price";
    public static final String KEY_AVAILABILITY = "availability";
    //buyers columns
    public static final String KEY_WHOLESALE = "wholesale";
    public static final String KEY_AGE = "age";
    public static final String KEY_DISCOUNT = "discount";
    //sellers columns
    public static final String KEY_POST = "post";
    public static final String KEY_SALARY = "salary";
    public static final String KEY_EXPERIENCE = "experience";
    //orders columns
    public static final String KEY_AMOUNT = "amount";
    public static final String KEY_DELIVERED = "delivered";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_DATE = "date";
    public static final String KEY_ID_GOODS = "id_goods";
    public static final String KEY_ID_SELLERS = "id_sellers";
    public static final String KEY_ID_BUYERS = "id_buyers";

    public static final String CREATE_TABLE_GOODS = "CREATE TABLE " + TABLE_GOODS + "(" + KEY_ID +
            " INTEGER PRIMARY KEY, " + KEY_NAME + " TEXT, " + KEY_PRICE + " REAL, " +
            KEY_AVAILABILITY + " BLOB" + ");";
    public static final String CREATE_TABLE_SELLERS = "CREATE TABLE " + TABLE_SELLERS + "(" +
            KEY_ID + " INTEGER PRIMARY KEY, " + KEY_NAME + " TEXT, " + KEY_POST + " TEXT, " +
            KEY_SALARY + " REAL, " + KEY_EXPERIENCE + " INTEGER" + ");";
    public static final String CREATE_TABLE_BUYERS = "CREATE TABLE " + TABLE_BUYERS + "(" + KEY_ID +
            " INTEGER PRIMARY KEY, " + KEY_NAME + " TEXT, " + KEY_WHOLESALE + " BOOLEAN, " +
            KEY_AGE + " INTEGER, " + KEY_DISCOUNT + " INTEGER" + ");";
    public static final String CREATE_TABLE_ORDERS = "CREATE TABLE " + TABLE_ORDERS + "(" + KEY_ID +
            " INTEGER PRIMARY KEY, " + KEY_AMOUNT + " INTEGER, " + KEY_DELIVERED + " BOOLEAN, "
            + KEY_ADDRESS + " TEXT, " + KEY_DATE + " TEXT, " + KEY_ID_GOODS + " INTEGER, " +
            KEY_ID_SELLERS + " INTEGER, " + KEY_ID_BUYERS + " INTEGER" + ", FOREIGN KEY(id_goods)" +
            " REFERENCES table_goods(_id) ON DELETE CASCADE, FOREIGN KEY(id_sellers) REFERENCES " +
            "table_sellers(_id) ON DELETE CASCADE, FOREIGN KEY(id_buyers) REFERENCES " +
            "table_buyers(_id) ON DELETE CASCADE);";

    public long ID_FOR_UPDATE;
    private static DBHelper instance;
    DBHelper dbTrade;

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DBHelper getInstance(Context context){
        if(instance == null){
            instance = new DBHelper(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_GOODS);
        sqLiteDatabase.execSQL(CREATE_TABLE_SELLERS);
        sqLiteDatabase.execSQL(CREATE_TABLE_BUYERS);
        sqLiteDatabase.execSQL(CREATE_TABLE_ORDERS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_GOODS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_SELLERS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_BUYERS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERS);
        onCreate(sqLiteDatabase);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys = ON;");
        }
    }

    public long createGoods(Goods goods){
        AsyncCreateGoods asyncCreateGoods = new AsyncCreateGoods();
        asyncCreateGoods.execute(goods);
        long goods_id = -1;
        try {
            goods_id = asyncCreateGoods.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return goods_id;
    }

    public Goods getGoods(long goods_id){
        Goods goods = new Goods();
        AsyncGetGoods asyncGetGoods = new AsyncGetGoods();
        asyncGetGoods.execute(goods_id);
        try {
            goods = asyncGetGoods.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return goods;
    }

    public int updateGoods(Goods goods){
        int id = -1;
        AsyncUpdateGoods asyncUpdateGoods = new AsyncUpdateGoods();
        asyncUpdateGoods.execute(goods);
        try {
            id = asyncUpdateGoods.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return id;
    }

    public void deleteGoods(long goods_id){
        AsyncDeleteGoods asyncDeleteGoods = new AsyncDeleteGoods();
        asyncDeleteGoods.execute(goods_id);
    }

    public Cursor getAllDataGoods() {
        AsyncGetAllDataGoods asyncGetAllDataGoods = new AsyncGetAllDataGoods();
        Cursor cursor = null;
        asyncGetAllDataGoods.execute();
        try {
            cursor = asyncGetAllDataGoods.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public long createSellers(Sellers sellers){
        long sellers_id = -1;
        AsyncCreateSellers asyncCreateSellers = new AsyncCreateSellers();
        asyncCreateSellers.execute(sellers);
        try {
            asyncCreateSellers.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return sellers_id;
    }

    public Sellers getSellers(long sellers_id){
        Sellers sellers = new Sellers();
        AsyncGetSellers asyncGetSellers = new AsyncGetSellers();
        asyncGetSellers.execute(sellers_id);
        try {
            sellers = asyncGetSellers.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return sellers;
    }

    public int updateSellers(Sellers sellers){
        AsyncUpdateSellers asyncUpdateSellers = new AsyncUpdateSellers();
        asyncUpdateSellers.execute(sellers);
        int id_sellers = -1;
        try {
            id_sellers = asyncUpdateSellers.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return id_sellers;
    }

    public void deleteSellers(long sellers_id){
        AsyncDeleteSellers asyncDeleteSellers = new AsyncDeleteSellers();
        asyncDeleteSellers.execute(sellers_id);
    }

    public Cursor getAllDataSellers() {
        Cursor cursor = null;
        AsyncGetAllDataSellers asyncGetAllDataSellers = new AsyncGetAllDataSellers();
        asyncGetAllDataSellers.execute();
        try {
            cursor = asyncGetAllDataSellers.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public long createBuyers(Buyers buyers){
        long buyers_id =-1;
        AsyncCreateBuyers asyncCreateBuyers = new AsyncCreateBuyers();
        asyncCreateBuyers.execute(buyers);
        try {
            buyers_id = asyncCreateBuyers.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return buyers_id;
    }
    public Buyers getBuyers(long buyers_id){
        Buyers buyers = new Buyers();
        AsyncGetBuyers asyncGetBuyers = new AsyncGetBuyers();
        asyncGetBuyers.execute(buyers_id);
        try {
            buyers = asyncGetBuyers.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return buyers;
    }

    public int updateBuyers(Buyers buyers){
        int buyers_id = -1;
        AsyncUpdateBuyers asyncUpdateBuyers = new AsyncUpdateBuyers();
        asyncUpdateBuyers.execute(buyers);
        try {
            buyers_id = asyncUpdateBuyers.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return buyers_id;
    }

    public void deleteBuyers(long sellers_id){
        AsyncDeleteBuyers asyncDeleteBuyers = new AsyncDeleteBuyers();
        asyncDeleteBuyers.execute(sellers_id);
    }

    public Cursor getAllDataBuyers() {
        Cursor cursor = null;
        AsyncGetAllDataBuyers asyncGetAllDataBuyers = new AsyncGetAllDataBuyers();
        asyncGetAllDataBuyers.execute();
        try {
            cursor = asyncGetAllDataBuyers.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public long createOrders(Orders orders){
        long orders_id = -1;
        AsyncCreateOrders asyncCreateOrders = new AsyncCreateOrders();
        asyncCreateOrders.execute(orders);
        try {
            orders_id = asyncCreateOrders.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return orders_id;
    }

    public Orders getOrders(long orders_id){
        Orders orders = new Orders();
        AsyncGetOrders asyncGetOrders = new AsyncGetOrders();
        asyncGetOrders.execute(orders_id);
        try {
            orders = asyncGetOrders.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return orders;
    }

    public int updateOrders(Orders orders){
        int orders_id = -1;
        AsyncUpdateOrders asyncUpdateOrders = new AsyncUpdateOrders();
        asyncUpdateOrders.execute(orders);
        try {
            orders_id = asyncUpdateOrders.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return orders_id;
    }

    public void deleteOrders(long sellers_id){
        AsyncDeleteOrders asyncDeleteOrders = new AsyncDeleteOrders();
        asyncDeleteOrders.execute(sellers_id);
    }

    public Cursor getAllDataOrders() {
        Cursor cursor = null;
        AsyncGetAllDataOrders asyncGetAllDataOrders = new AsyncGetAllDataOrders();
        asyncGetAllDataOrders.execute();
        try {
            cursor = asyncGetAllDataOrders.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    class AsyncUpdateGoods extends AsyncTask<Goods, Void, Integer>{

        Goods goods;

        @Override
        protected Integer doInBackground(Goods... params) {
            goods = params[0];
            dbTrade = instance;
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_NAME, goods.getName());
            contentValues.put(KEY_PRICE, goods.getPrice());
            contentValues.put(KEY_AVAILABILITY, goods.isAvailability());
            return sqLiteDatabase.update(TABLE_GOODS, contentValues, KEY_ID + " = ?",
                    new String[] { String.valueOf(goods.getId()) });
        }
    }

    class AsyncGetGoods extends AsyncTask<Long, Void, Goods>{

        @Override
        protected Goods doInBackground(Long... params) {
            dbTrade = instance;
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_GOODS + " WHERE " + KEY_ID + " = " + params[0];
            Goods goods = new Goods();
            Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
            if(cursor != null){
                cursor.moveToFirst();
                goods.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                goods.setAvailability(cursor.getInt(cursor.getColumnIndex(KEY_AVAILABILITY))>0);
                goods.setPrice(cursor.getFloat(cursor.getColumnIndex(KEY_PRICE)));
                goods.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
            }
            return goods;
        }
    }

    class AsyncCreateGoods extends AsyncTask<Goods, Void, Long>{

        @Override
        protected Long doInBackground(Goods... params) {
            dbTrade = instance;
            Goods goods;
            goods = params[0];
            ContentValues contentValues = new ContentValues();
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            contentValues.put(KEY_NAME, goods.getName());
            contentValues.put(KEY_PRICE, goods.getPrice());
            contentValues.put(KEY_AVAILABILITY, goods.isAvailability());
            long goods_id = sqLiteDatabase.insert(TABLE_GOODS, null, contentValues);
            return goods_id;
        }
    }

    class AsyncDeleteGoods extends AsyncTask<Long, Void, Void>{

        @Override
        protected Void doInBackground(Long... params) {
            dbTrade = instance;
            long goods_id = params[0];
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            sqLiteDatabase.delete(TABLE_GOODS, KEY_ID + " = ?", new String[]{String.valueOf(goods_id)});
            return null;
        }
    }

    class AsyncGetAllDataGoods extends AsyncTask<Void, Void, Cursor>{

        @Override
        protected Cursor doInBackground(Void... params) {
            dbTrade = instance;
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            return sqLiteDatabase.query(TABLE_GOODS, null, null, null, null, null, null);
        }
    }

    class AsyncUpdateSellers extends AsyncTask<Sellers, Void, Integer>{

        Sellers sellers;

        @Override
        protected Integer doInBackground(Sellers... params) {
            sellers = params[0];
            dbTrade = instance;
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_NAME, sellers.getName());
            contentValues.put(KEY_POST, sellers.getPost());
            contentValues.put(KEY_SALARY, sellers.getSalary());
            contentValues.put(KEY_EXPERIENCE, sellers.getExperience());
            return sqLiteDatabase.update(TABLE_SELLERS, contentValues, KEY_ID + " = ?",
                    new String[] { String.valueOf(sellers.getId()) });
        }
    }

    class AsyncGetSellers extends AsyncTask<Long, Void, Sellers>{

        @Override
        protected Sellers doInBackground(Long... params) {
            dbTrade = instance;
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_SELLERS + " WHERE " + KEY_ID + " = " + params[0];
            Sellers sellers = new Sellers();
            Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
            if(cursor != null){
                cursor.moveToFirst();
                sellers.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                sellers.setExperience(cursor.getInt(cursor.getColumnIndex(KEY_EXPERIENCE)));
                sellers.setSalary(cursor.getFloat(cursor.getColumnIndex(KEY_SALARY)));
                sellers.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                sellers.setPost(cursor.getString(cursor.getColumnIndex(KEY_POST)));
            }
            return sellers;
        }
    }

    class AsyncCreateSellers extends AsyncTask<Sellers, Void, Long>{

        @Override
        protected Long doInBackground(Sellers... params) {
            dbTrade = instance;
            Sellers sellers;
            sellers = params[0];
            ContentValues contentValues = new ContentValues();
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            contentValues.put(KEY_NAME, sellers.getName());
            contentValues.put(KEY_POST, sellers.getPost());
            contentValues.put(KEY_SALARY, sellers.getSalary());
            contentValues.put(KEY_EXPERIENCE, sellers.getExperience());
            long sellers_id = sqLiteDatabase.insert(TABLE_SELLERS, null, contentValues);
            return sellers_id;
        }
    }

    class AsyncDeleteSellers extends AsyncTask<Long, Void, Void>{

        @Override
        protected Void doInBackground(Long... params) {
            dbTrade = instance;
            long sellers_id = params[0];
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            sqLiteDatabase.delete(TABLE_SELLERS, KEY_ID + " = ?", new String[]{String.valueOf(sellers_id)});
            return null;
        }
    }

    class AsyncGetAllDataSellers extends AsyncTask<Void, Void, Cursor>{

        @Override
        protected Cursor doInBackground(Void... params) {
            dbTrade = instance;
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            return sqLiteDatabase.query(TABLE_SELLERS, null, null, null, null, null, null);
        }
    }

    class AsyncUpdateBuyers extends AsyncTask<Buyers, Void, Integer>{

        Buyers buyers;

        @Override
        protected Integer doInBackground(Buyers... params) {
            buyers = params[0];
            dbTrade = instance;
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_NAME, buyers.getName());
            contentValues.put(KEY_WHOLESALE, buyers.isWholesale());
            contentValues.put(KEY_AGE, buyers.getAge());
            contentValues.put(KEY_DISCOUNT, buyers.getDiscount());
            return sqLiteDatabase.update(TABLE_BUYERS, contentValues, KEY_ID + " = ?",
                    new String[] { String.valueOf(buyers.getId()) });
        }
    }

    class AsyncGetBuyers extends AsyncTask<Long, Void, Buyers>{

        @Override
        protected Buyers doInBackground(Long... params) {
            dbTrade = instance;
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_BUYERS + " WHERE " + KEY_ID + " = " + params[0];
            Buyers buyers = new Buyers();
            Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
            if(cursor != null){
                if(cursor.moveToFirst()) {
                    buyers.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                    buyers.setWholesale(cursor.getInt(cursor.getColumnIndex(KEY_WHOLESALE)) > 0);
                    buyers.setAge(cursor.getInt(cursor.getColumnIndex(KEY_AGE)));
                    buyers.setDiscount(cursor.getInt(cursor.getColumnIndex(KEY_DISCOUNT)));
                    buyers.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                }
            }
            return buyers;
        }
    }

    class AsyncCreateBuyers extends AsyncTask<Buyers, Void, Long>{

        @Override
        protected Long doInBackground(Buyers... params) {
            dbTrade = instance;
            Buyers buyers;
            buyers = params[0];
            ContentValues contentValues = new ContentValues();
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            contentValues.put(KEY_NAME, buyers.getName());
            contentValues.put(KEY_WHOLESALE, buyers.isWholesale());
            contentValues.put(KEY_AGE, buyers.getAge());
            contentValues.put(KEY_DISCOUNT, buyers.getDiscount());
            long buyers_id = sqLiteDatabase.insert(TABLE_BUYERS, null, contentValues);
            return buyers_id;
        }
    }

    class AsyncDeleteBuyers extends AsyncTask<Long, Void, Void>{

        @Override
        protected Void doInBackground(Long... params) {
            dbTrade = instance;
            long buyers_id = params[0];
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            sqLiteDatabase.delete(TABLE_BUYERS, KEY_ID + " = ?", new String[]{String.valueOf(buyers_id)});
            return null;
        }
    }

    class AsyncGetAllDataBuyers extends AsyncTask<Void, Void, Cursor>{

        @Override
        protected Cursor doInBackground(Void... params) {
            dbTrade = instance;
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            return sqLiteDatabase.query(TABLE_BUYERS, null, null, null, null, null, null);
        }
    }

    class AsyncUpdateOrders extends AsyncTask<Orders, Void, Integer>{

        Orders orders;

        @Override
        protected Integer doInBackground(Orders... params) {
            orders = params[0];
            dbTrade = instance;
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_AMOUNT, orders.getAmount());
            contentValues.put(KEY_DELIVERED, orders.isDelivered());
            contentValues.put(KEY_ADDRESS, orders.getAddress());
            contentValues.put(KEY_DATE, orders.getDate());
            contentValues.put(KEY_ID_GOODS, orders.getId_goods());
            contentValues.put(KEY_ID_BUYERS, orders.getId_buyers());
            contentValues.put(KEY_ID_SELLERS, orders.getId_sellers());
            return sqLiteDatabase.update(TABLE_ORDERS, contentValues, KEY_ID + " = ?",
                    new String[] { String.valueOf(orders.getId()) });
        }
    }

    class AsyncGetOrders extends AsyncTask<Long, Void, Orders>{

        @Override
        protected Orders doInBackground(Long... params) {
            dbTrade = instance;
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_ORDERS + " WHERE " + KEY_ID + " = " + params[0];
            Orders orders = new Orders();
            Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
            if(cursor != null){
                cursor.moveToFirst();
                orders.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                orders.setAmount(cursor.getInt(cursor.getColumnIndex(KEY_AMOUNT)));
                orders.setDelivered(cursor.getInt(cursor.getColumnIndex(KEY_DELIVERED)) > 0);
                orders.setDate(cursor.getString(cursor.getColumnIndex(KEY_DATE)));
                orders.setAddress(cursor.getString(cursor.getColumnIndex(KEY_ADDRESS)));
                orders.setId_buyers(cursor.getInt(cursor.getColumnIndex(KEY_ID_BUYERS)));
                orders.setId_sellers(cursor.getInt(cursor.getColumnIndex(KEY_ID_SELLERS)));
                orders.setId_goods(cursor.getInt(cursor.getColumnIndex(KEY_ID_GOODS)));
            }
            return orders;
        }
    }

    class AsyncCreateOrders extends AsyncTask<Orders, Void, Long>{

        @Override
        protected Long doInBackground(Orders... params) {
            dbTrade = instance;
            Orders orders;
            orders = params[0];
            ContentValues contentValues = new ContentValues();
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            contentValues.put(KEY_AMOUNT, orders.getAmount());
            contentValues.put(KEY_DELIVERED, orders.isDelivered());
            contentValues.put(KEY_ADDRESS, orders.getAddress());
            contentValues.put(KEY_DATE, orders.getDate());
            contentValues.put(KEY_ID_GOODS, orders.getId_goods());
            contentValues.put(KEY_ID_BUYERS, orders.getId_buyers());
            contentValues.put(KEY_ID_SELLERS, orders.getId_sellers());
            long orders_id = sqLiteDatabase.insert(TABLE_ORDERS, null, contentValues);
            return orders_id;
        }
    }

    class AsyncDeleteOrders extends AsyncTask<Long, Void, Void>{

        @Override
        protected Void doInBackground(Long... params) {
            dbTrade = instance;
            long orders_id = params[0];
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            sqLiteDatabase.delete(TABLE_ORDERS, KEY_ID + " = ?", new String[]{String.valueOf(orders_id)});
            return null;
        }
    }

    class AsyncGetAllDataOrders extends AsyncTask<Void, Void, Cursor>{

        @Override
        protected Cursor doInBackground(Void... params) {
            dbTrade = instance;
            SQLiteDatabase sqLiteDatabase = dbTrade.getWritableDatabase();
            return sqLiteDatabase.query(TABLE_ORDERS, null, null, null, null, null, null);
        }
    }
}
