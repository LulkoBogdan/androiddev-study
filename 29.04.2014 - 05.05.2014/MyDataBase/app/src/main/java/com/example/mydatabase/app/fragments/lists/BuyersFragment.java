package com.example.mydatabase.app.fragments.lists;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.mydatabase.app.MyCursorLoader;
import com.example.mydatabase.app.DBHelper;
import com.example.mydatabase.app.R;
import com.example.mydatabase.app.fragments.modify.ModifyBuyers;

/**
 * Created by androiddev9 on 30.04.14.
 */
public class BuyersFragment extends Fragment implements View.OnClickListener,
        LoaderManager.LoaderCallbacks<Cursor>{

    private final int CM_DELETE_ID_BUYERS = 3;
    private final int CM_UPDATE_ID_BUYERS = 7;

    public SimpleCursorAdapter scAdapterBuyers;
    String fromBuyers[];
    int toBuyers[];
    DBHelper dbTrade;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buyers, container, false);
        dbTrade = DBHelper.getInstance(getActivity());
        fromBuyers = new String[] {DBHelper.KEY_NAME, DBHelper.KEY_ID, DBHelper.KEY_WHOLESALE,
                DBHelper.KEY_AGE, DBHelper.KEY_DISCOUNT};
        toBuyers = new int[] {R.id.textBuyersName, R.id.textBuyersId, R.id.textBuyersWholesale,
                R.id.textBuyersAge, R.id.textBuyersDiscount};
        scAdapterBuyers = new SimpleCursorAdapter(getActivity(), R.layout.item_buyers, null, fromBuyers,
                toBuyers, 0);
        getActivity().getSupportLoaderManager().initLoader(2, null, this);
        Button btnNewBuyers = (Button) view.findViewById(R.id.buttonAddBuyers);
        ListView listView = (ListView) view.findViewById(R.id.listViewBuyers);
        btnNewBuyers.setOnClickListener(this);
        listView.setAdapter(scAdapterBuyers);
        registerForContextMenu(listView);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getSupportLoaderManager().getLoader(2).forceLoad();
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        switch (v.getId()){
            case R.id.listViewBuyers:
                menu.add(0, CM_DELETE_ID_BUYERS, 0, R.string.delete_record);
                menu.add(0, CM_UPDATE_ID_BUYERS, 0, R.string.update_record);
                break;
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == CM_DELETE_ID_BUYERS) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();
            dbTrade.deleteBuyers(acmi.id);
            getActivity().getSupportLoaderManager().getLoader(2).forceLoad();
            return true;
        }
        if (item.getItemId() == CM_UPDATE_ID_BUYERS) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();
            dbTrade.ID_FOR_UPDATE = acmi.id;
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            ModifyBuyers modifyBuyers = new ModifyBuyers(true);
            fragmentTransaction.replace(R.id.relative_layout, modifyBuyers);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonAddBuyers:
                ModifyBuyers modifyBuyers = new ModifyBuyers(false);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.relative_layout, modifyBuyers);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(getActivity(), dbTrade, id);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        int id = loader.getId();
        if(id == 2)
            scAdapterBuyers.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
