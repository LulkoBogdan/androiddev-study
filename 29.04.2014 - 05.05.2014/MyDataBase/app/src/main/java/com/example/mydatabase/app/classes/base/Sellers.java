package com.example.mydatabase.app.classes.base;

/**
 * Created by androiddev9 on 29.04.14.
 */
public class Sellers {
    private int id;
    private String name;
    private String post;
    private float salary;
    private int experience;

    public Sellers(int id, String name, String post, float salary, int experience){
        this.experience = experience;
        this.id = id;
        this.name = name;
        this.post = post;
        this.salary = salary;
    }

    public Sellers(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }
}
