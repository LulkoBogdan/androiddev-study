package com.example.mydatabase.app.classes.base;

/**
 * Created by androiddev9 on 29.04.14.
 */
public class Goods {
    private int id;
    private String name;
    private float price;
    private boolean availability;

    public Goods(int id, String name, float price, boolean availability){
        this.availability = availability;
        this.id = id;
        this.name = name;
        this.price = price;
    }
    public Goods(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }
}
