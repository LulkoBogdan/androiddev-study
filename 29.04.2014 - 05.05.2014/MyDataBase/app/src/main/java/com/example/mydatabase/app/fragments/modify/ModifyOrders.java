package com.example.mydatabase.app.fragments.modify;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.mydatabase.app.DBHelper;
import com.example.mydatabase.app.MyCursorLoader;
import com.example.mydatabase.app.R;
import com.example.mydatabase.app.classes.base.Orders;

/**
 * Created by androiddev9 on 07.05.14.
 */
public class ModifyOrders extends Fragment implements View.OnClickListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    EditText etAmount;
    EditText etAddress;
    EditText etDate;
    CheckBox cbDelivered;
    DBHelper dbTrade;
    Spinner spinnerGoods;
    Spinner spinnerSellers;
    Spinner spinnerBuyers;
    public SimpleCursorAdapter scSpinnerOrders;
    public SimpleCursorAdapter scSpinnerSellers;
    public SimpleCursorAdapter scSpinnerBuyers;
    String fromSpinner[];
    int toSpinner[];
    boolean type;
    int id;

    public ModifyOrders(boolean type) {
        this.type = type;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_orders, container, false);
        dbTrade = DBHelper.getInstance(getActivity());
        fromSpinner = new String[] {DBHelper.KEY_ID, DBHelper.KEY_NAME};
        toSpinner = new int[] {R.id.textViewSpinnerId, R.id.textViewSpinnerName};
        scSpinnerOrders = new SimpleCursorAdapter(getActivity(), R.layout.item_spinner, null,
                fromSpinner, toSpinner, 0);
        scSpinnerSellers = new SimpleCursorAdapter(getActivity(), R.layout.item_spinner, null,
                fromSpinner, toSpinner, 0);
        scSpinnerBuyers = new SimpleCursorAdapter(getActivity(), R.layout.item_spinner, null,
                fromSpinner, toSpinner, 0);
        spinnerGoods = (Spinner) view.findViewById(R.id.spinnerGoods);
        spinnerGoods.setAdapter(scSpinnerOrders);
        spinnerSellers = (Spinner) view.findViewById(R.id.spinnerSeller);
        spinnerSellers.setAdapter(scSpinnerSellers);
        spinnerBuyers = (Spinner) view.findViewById(R.id.spinnerBuyers);
        spinnerBuyers.setAdapter(scSpinnerBuyers);
        Button btnAdd = (Button) view.findViewById(R.id.buttonAddNewOrders);
        etAddress = (EditText) view.findViewById(R.id.editTextOrdersAddress);
        etAmount = (EditText) view.findViewById(R.id.editTextOrdersAmount);
        etDate = (EditText) view.findViewById(R.id.editTextOrdersDate);
        cbDelivered = (CheckBox) view.findViewById(R.id.checkBoxOrders);
        if(type){
            btnAdd.setText("Update");
            Orders orders = dbTrade.getOrders(dbTrade.ID_FOR_UPDATE);
            etAddress.setText(orders.getAddress());
            etDate.setText(orders.getDate());
            etAmount.setText(String.valueOf(orders.getAmount()));
            if(orders.isDelivered()){
                cbDelivered.setChecked(true);
            } else {
                cbDelivered.setChecked(false);
            }
            id = orders.getId();
            Cursor cursor = dbTrade.getAllDataBuyers();
            int count = 1;
            if(cursor != null){
                if(cursor.moveToFirst()){
                    do{
                        int i = cursor.getInt(cursor.getColumnIndex("_id"));
                        if(i == orders.getId_buyers()){
                            break;
                        }
                        count++;
                    }while (cursor.moveToNext());
                }
            }
            Log.d("MyLogs", "countOrders = " + count);
            spinnerBuyers.setSelection(count - 1);
            cursor = dbTrade.getAllDataSellers();
            count = 1;
            if(cursor != null){
                if(cursor.moveToFirst()){
                    do{
                        int i = cursor.getInt(cursor.getColumnIndex("_id"));
                        if(i == orders.getId_sellers()){
                            break;
                        }
                        count++;
                    }while (cursor.moveToNext());
                }
            }
            Log.d("MyLogs", "countSellers = " + count);
            spinnerSellers.setSelection(count - 1);
            cursor = dbTrade.getAllDataGoods();
            count = 1;
            if(cursor != null){
                if(cursor.moveToFirst()){
                    do{
                        int i = cursor.getInt(cursor.getColumnIndex("_id"));
                        if(i == orders.getId_goods()){
                            break;
                        }
                        count++;
                    }while (cursor.moveToNext());
                }
            }
            Log.d("MyLogs", "countGoods = " + count);
            spinnerGoods.setSelection(count - 1);
        }
        getActivity().getSupportLoaderManager().initLoader(4, null, this);
        getActivity().getSupportLoaderManager().initLoader(5, null, this);
        getActivity().getSupportLoaderManager().initLoader(6, null, this);
        btnAdd.setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getSupportLoaderManager().getLoader(4).forceLoad();
        getActivity().getSupportLoaderManager().getLoader(5).forceLoad();
        getActivity().getSupportLoaderManager().getLoader(6).forceLoad();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(getActivity(), dbTrade, id);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        int id = loader.getId();
        if(id == 4)
            scSpinnerOrders.swapCursor(data);
        if(id == 5)
            scSpinnerSellers.swapCursor(data);
        if(id == 6)
            scSpinnerBuyers.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonAddNewOrders:
                String bufAmount;
                String date;
                String address;
                int id_goods = 0;
                int id_seller = 0;
                int id_buyers = 0;
                int amount;

                boolean delivered;
                Cursor cursor;
                if(cbDelivered.isChecked()){
                    delivered = true;
                } else {
                    delivered = false;
                }
                date = etDate.getText().toString();
                address = etAddress.getText().toString();
                bufAmount = etAmount.getText().toString();
                try {
                    amount = Integer.parseInt(bufAmount);
                } catch (NumberFormatException e){
                    Toast.makeText(getActivity(), "Wrong amount", Toast.LENGTH_SHORT).show();
                    amount = -1;
                }
                if(TextUtils.isEmpty(date)){
                    Toast.makeText(getActivity(), "Date is NULL", Toast.LENGTH_SHORT).show();
                }
                if(TextUtils.isEmpty(address)){
                    Toast.makeText(getActivity(), "Address is NULL", Toast.LENGTH_SHORT).show();
                }
                if(spinnerGoods.getSelectedItem() != null) {
                    cursor = (Cursor) spinnerGoods.getSelectedItem();
                    id_goods = cursor.getInt(cursor.getColumnIndex(DBHelper.KEY_ID));
                } else {
                    amount = -1;
                    Toast.makeText(getActivity(), "No selected Goods", Toast.LENGTH_SHORT).show();
                }
                if(spinnerSellers.getSelectedItem() != null) {
                    cursor = (Cursor) spinnerSellers.getSelectedItem();
                    id_seller = cursor.getInt(cursor.getColumnIndex(DBHelper.KEY_ID));
                } else {
                    amount = -1;
                    Toast.makeText(getActivity(), "No selected Sellers", Toast.LENGTH_SHORT).show();
                }
                if(spinnerBuyers.getSelectedItem() != null) {
                    cursor = (Cursor) spinnerBuyers.getSelectedItem();
                    id_buyers = cursor.getInt(cursor.getColumnIndex(DBHelper.KEY_ID));
                } else {
                    amount = -1;
                    Toast.makeText(getActivity(), "No selected Buyers", Toast.LENGTH_SHORT).show();
                }


                if((amount > 0)&&(!TextUtils.isEmpty(date))&&(!TextUtils.isEmpty(address))){
                    Orders orders = new Orders();
                    orders.setAddress(address);
                    orders.setAmount(amount);
                    orders.setDate(date);
                    orders.setDelivered(delivered);
                    orders.setId_buyers(id_buyers);
                    orders.setId_goods(id_goods);
                    orders.setId_sellers(id_seller);
                    if(type){
                        orders.setId(id);
                        dbTrade.updateOrders(orders);
                        Toast.makeText(getActivity(), "Updated ", Toast.LENGTH_SHORT).show();
                    } else {
                        dbTrade.createOrders(orders);
                        Toast.makeText(getActivity(), "Added ", Toast.LENGTH_SHORT).show();
                    }
                    getFragmentManager().popBackStack();
                }
                break;
        }
    }

}
