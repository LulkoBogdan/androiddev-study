package com.example.mydatabase.app.fragments.lists;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.mydatabase.app.MyCursorLoader;
import com.example.mydatabase.app.DBHelper;
import com.example.mydatabase.app.R;
import com.example.mydatabase.app.fragments.modify.ModifySellers;

/**
 * Created by androiddev9 on 30.04.14.
 */
public class SellersFragment extends Fragment implements View.OnClickListener,
        LoaderManager.LoaderCallbacks<Cursor>{

    private final int CM_DELETE_ID_SELLERS = 2;
    private final int CM_UPDATE_ID_SELLERS = 6;

    public SimpleCursorAdapter scAdapterSellers;
    DBHelper dbTrade;
    String fromSellers[];
    int toSellers[];

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sellers, container, false);
        dbTrade = DBHelper.getInstance(getActivity());
        fromSellers = new String[] {DBHelper.KEY_NAME, DBHelper.KEY_ID, DBHelper.KEY_POST,
                DBHelper.KEY_SALARY, DBHelper.KEY_EXPERIENCE};
        toSellers = new int[] {R.id.textSellerName, R.id.textSellersId, R.id.textSellerPost,
                R.id.textSellerSalary, R.id.textSellerExperience};
        scAdapterSellers = new SimpleCursorAdapter(getActivity(), R.layout.item_sellers, null, fromSellers,
                toSellers, 0);
        getActivity().getSupportLoaderManager().initLoader(1, null, this);
        Button btnNewSellers = (Button) view.findViewById(R.id.buttonAddSellers);
        ListView listView = (ListView) view.findViewById(R.id.listViewSellers);
        btnNewSellers.setOnClickListener(this);
        listView.setAdapter(scAdapterSellers);
        registerForContextMenu(listView);
        return view;
    }

    public void onCreateContextMenu(ContextMenu menu, View v,ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        switch (v.getId()) {
            case R.id.listViewSellers:
                menu.add(0, CM_DELETE_ID_SELLERS, 0, R.string.delete_record);
                menu.add(0, CM_UPDATE_ID_SELLERS, 0, R.string.update_record);
                break;
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == CM_DELETE_ID_SELLERS) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();
            dbTrade.deleteSellers(acmi.id);
            getActivity().getSupportLoaderManager().getLoader(1).forceLoad();
            return true;
        }
        if (item.getItemId() == CM_UPDATE_ID_SELLERS) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();
            dbTrade.ID_FOR_UPDATE = acmi.id;
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            ModifySellers modifySellers = new ModifySellers(true);
            fragmentTransaction.replace(R.id.relative_layout, modifySellers);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            return true;
        }
        return super.onContextItemSelected(item);
    }
    @Override
    public void onResume() {
        super.onResume();
        getActivity().getSupportLoaderManager().getLoader(1).forceLoad();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonAddSellers:
                ModifySellers modifySellers = new ModifySellers(false);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.relative_layout, modifySellers);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(getActivity(), dbTrade, id);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        int id = loader.getId();
        if(id == 1)
            scAdapterSellers.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

}
