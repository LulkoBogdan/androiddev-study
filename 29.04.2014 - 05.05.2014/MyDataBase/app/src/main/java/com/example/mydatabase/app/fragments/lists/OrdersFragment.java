package com.example.mydatabase.app.fragments.lists;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.mydatabase.app.MyCursorLoader;
import com.example.mydatabase.app.DBHelper;
import com.example.mydatabase.app.R;
import com.example.mydatabase.app.fragments.modify.ModifyOrders;

/**
 * Created by androiddev9 on 30.04.14.
 */
public class OrdersFragment extends Fragment implements View.OnClickListener,
        LoaderManager.LoaderCallbacks<Cursor>{

    private final int CM_DELETE_ID_ORDERS = 4;
    private final int CM_UPDATE_ID_ORDERS = 8;

    DBHelper dbTrade;
    public SimpleCursorAdapter scAdapterOrders;
    String fromOrders[];
    int toOrders[];

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orders, container, false);
        dbTrade = DBHelper.getInstance(getActivity());
        fromOrders = new String[] {DBHelper.KEY_ID, DBHelper.KEY_AMOUNT, DBHelper.KEY_DELIVERED,
                DBHelper.KEY_ADDRESS, DBHelper.KEY_DATE, DBHelper.KEY_ID_BUYERS,
                DBHelper.KEY_ID_SELLERS, DBHelper.KEY_ID_GOODS};
        toOrders = new int[]{R.id.textViewOrdersId, R.id.textViewOrdersAmount,
                R.id.textViewOrdersDelivered, R.id.textViewOrdersAddress, R.id.textViewOrdersDate,
                R.id.textViewOrdersBuyerId, R.id.textViewOrdersSellerId, R.id.textViewOrdersGoodsId};
        scAdapterOrders = new SimpleCursorAdapter(getActivity(), R.layout.item_orders, null, fromOrders,
                toOrders, 0);
        getActivity().getSupportLoaderManager().initLoader(3, null, this);
        Button btnNewGoods = (Button) view.findViewById(R.id.buttonAddOrders);
        ListView listView = (ListView) view.findViewById(R.id.listViewOrders);
        btnNewGoods.setOnClickListener(this);
        listView.setAdapter(scAdapterOrders);
        registerForContextMenu(listView);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getSupportLoaderManager().getLoader(3).forceLoad();
    }

    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        switch (v.getId()){
            case R.id.listViewOrders:
                menu.add(0, CM_DELETE_ID_ORDERS, 0, R.string.delete_record);
                menu.add(0, CM_UPDATE_ID_ORDERS, 0, R.string.update_record);
                break;
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == CM_DELETE_ID_ORDERS) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();
            dbTrade.deleteOrders(acmi.id);
            getActivity().getSupportLoaderManager().getLoader(3).forceLoad();
            return true;
        }
        if (item.getItemId() == CM_UPDATE_ID_ORDERS) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();
            dbTrade.ID_FOR_UPDATE = acmi.id;
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            ModifyOrders modifyOrders = new ModifyOrders(true);
            fragmentTransaction.replace(R.id.relative_layout, modifyOrders);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.buttonAddOrders:
                ModifyOrders modifyOrders = new ModifyOrders(false);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.relative_layout, modifyOrders);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(getActivity(), dbTrade, id);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        int id = loader.getId();
        if(id == 3)
            scAdapterOrders.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

}
