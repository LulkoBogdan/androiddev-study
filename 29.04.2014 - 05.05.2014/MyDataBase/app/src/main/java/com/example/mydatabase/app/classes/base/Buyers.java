package com.example.mydatabase.app.classes.base;

/**
 * Created by androiddev9 on 29.04.14.
 */
public class Buyers {
    private int id;
    private String name;
    private boolean wholesale;
    private int age;
    private int discount;

    public Buyers(int id, String name, boolean wholesale, int age, int discount){
        this.age = age;
        this.discount = discount;
        this.id = id;
        this.name = name;
        this.wholesale = wholesale;
    }

    public Buyers(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isWholesale() {
        return wholesale;
    }

    public void setWholesale(boolean wholesale) {
        this.wholesale = wholesale;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }
}
