package com.example.homework.app;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

    final int CODE_RESULT_TEXT = 1;
    final static String KEY_RESULT_TEXT = "html-code";
    EditText etResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etResult = (EditText) findViewById(R.id.editText);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public  void onClickButton1(View v){
        Intent intent = new Intent(this, SecondActivity.class);
        startActivityForResult(intent, CODE_RESULT_TEXT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK){
            if(requestCode == CODE_RESULT_TEXT){
                if(data == null){
                    return;
                }
                String result = data.getStringExtra(KEY_RESULT_TEXT);
                etResult.setText(result);
            }
        } else {
            Toast.makeText(this, "No html-code", Toast.LENGTH_SHORT).show();
        }
    }

}
