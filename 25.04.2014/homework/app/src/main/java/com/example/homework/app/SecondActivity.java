package com.example.homework.app;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class SecondActivity extends ActionBarActivity implements FragmentWork.WorkInFragment{

    ProgressDialog progressDialog;
    String codeString;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.second, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void Work(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Downloading");
        progressDialog.setMessage("Please wait");
        progressDialog.show();
        LoadPage loadPage = new LoadPage();
        loadPage.execute();
    }

    class LoadPage extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... voids) {
            String html = "";
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet("https://www.google.com.ua");
            HttpResponse response = null;
            try {
                response = client.execute(request);
                InputStream in = response.getEntity().getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder str = new StringBuilder();
                String line = null;
                while((line = reader.readLine()) != null){
                    str.append(line);
                }
                in.close();
                html = str.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return html;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            codeString = s;
            progressDialog.dismiss();
            Intent intent = new Intent();
            intent.putExtra(MainActivity.KEY_RESULT_TEXT, codeString);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

}
