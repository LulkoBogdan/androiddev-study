package com.example.homework.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by androiddev9 on 28.04.14.
 */
public class FragmentWork extends Fragment {

    public interface WorkInFragment {
        public void Work();
    }

    WorkInFragment workInFragment;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            workInFragment = (WorkInFragment) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()+"must implements FragmentWork");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment, null);
        Button button2 = (Button) v.findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                workInFragment.Work();
            }
        });
        return v;
    }
}
