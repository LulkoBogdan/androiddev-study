package com.example.task1.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.task1.app.MainActivity.InputData;

/**
 * Created by androiddev9 on 24.04.14.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Fragment2 extends Fragment implements InputData{

    static TextView tvResult;

    @Override
    public void incomingData(String s) {
         tvResult.setText(s);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment2, null);
        tvResult = (TextView) view.findViewById(R.id.textView);
        Button button2 = (Button) view.findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Activity activity = getActivity();
                Intent intent = new Intent(activity, SecondActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }

}
