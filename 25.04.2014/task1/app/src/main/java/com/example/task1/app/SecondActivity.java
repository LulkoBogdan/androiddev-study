package com.example.task1.app;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.task1.app.Fragment1.onEventListener;

public class SecondActivity extends ActionBarActivity implements onEventListener{

    Fragment1 fragment1;
    Fragment2 fragment2;
    FragmentTransaction fragmentTransaction;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        fragment1 = new Fragment1();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frl1, fragment1);
        fragmentTransaction.commit();

        fragment2 = new Fragment2();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frl2, fragment2);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.second, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void someEvent(String s) {
        Fragment fragment2 = getFragmentManager().findFragmentById(R.id.frl2);
        View v = fragment2.getView();
        TextView tv = (TextView) v.findViewById(R.id.textView);
        tv.setText(s);
        //((TextView)fragment2.getView().findViewById(R.id.textView)).setText(s);
    }

}
