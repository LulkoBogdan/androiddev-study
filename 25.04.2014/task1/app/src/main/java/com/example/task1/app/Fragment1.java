package com.example.task1.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by androiddev9 on 24.04.14.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Fragment1 extends Fragment {

    public interface onEventListener{
        public void someEvent(String s);
    }

    EditText etInText;
    String newText;
    onEventListener eventListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            eventListener = (onEventListener) activity;
        } catch (ClassCastException e){
            throw new ClassCastException(activity.toString()+"must implement onEventListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment1, null);
        etInText = (EditText) view.findViewById(R.id.editText);

        Button button1 = (Button) view.findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newText = etInText.getText().toString();
                eventListener.someEvent(newText);
            }
        });
        return view;
    }

}
