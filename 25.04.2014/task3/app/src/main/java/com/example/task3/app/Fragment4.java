package com.example.task3.app;

import android.annotation.TargetApi;
import android.support.v4.app.Fragment;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by androiddev9 on 24.04.14.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Fragment4 extends Fragment {

    int backColor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Random rndm = new Random();
        backColor = Color.argb(40, rndm.nextInt(256), rndm.nextInt(256), rndm.nextInt(256));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment4, null);
        TextView textView = (TextView) view.findViewById(R.id.textView4);
        textView.setBackgroundColor(backColor);
        return view;
    }
}
