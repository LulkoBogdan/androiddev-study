package com.example.task2.app;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.example.task2.app.Fragment1.listenerFragment1;
import com.example.task2.app.Fragment2.listenerFragment2;
import com.example.task2.app.Fragment3.listenerFragment3;
import com.example.task2.app.Fragment4.listenerFragment4;

public class MainActivity extends ActionBarActivity implements listenerFragment1,
        listenerFragment2, listenerFragment3, listenerFragment4 {

    Fragment1 fragment1;
    Fragment2 fragment2;
    Fragment3 fragment3;
    Fragment4 fragment4;
    FragmentTransaction ft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragment1 = new Fragment1();
        fragment2 = new Fragment2();
        fragment3 = new Fragment3();
        fragment4 = new Fragment4();
        ft = getFragmentManager().beginTransaction();
        ft.add(R.id.frameLayout, fragment1);
        ft.commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void eventFragment1() {
        removeAdd(fragment2);
    }

    @Override
    public void eventFragment2() {
        removeAdd(fragment3);
    }

    @Override
    public void eventFragment3() {
        removeAdd(fragment4);
    }

    @Override
    public void eventFragment4() {
        getFragmentManager().popBackStack(0, FragmentManager.POP_BACK_STACK_INCLUSIVE);

    }

    private void removeAdd(Fragment f1){
        ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, f1);
        ft.addToBackStack(null);
        ft.commit();
    }
}
