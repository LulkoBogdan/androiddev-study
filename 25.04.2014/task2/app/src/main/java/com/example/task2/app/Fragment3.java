package com.example.task2.app;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by androiddev9 on 24.04.14.
 */
public class Fragment3 extends Fragment{

    public interface listenerFragment3{
        public void eventFragment3();
    }

    listenerFragment3 listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            listener = (listenerFragment3)activity;
        } catch(ClassCastException e){
            throw new ClassCastException(activity.toString()+"must implement listenerFragment1");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment3, null);
        Button btn = (Button) view.findViewById(R.id.button3);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.eventFragment3();
            }
        });
        return view;
    }
}
