package com.example.task11.app.common;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;

import java.util.concurrent.ExecutionException;

/**
 * Created by androiddev9 on 07.05.14.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "databaseFotoInfo6";

    public static final String TABLE_NAME = "table_foto_info";

    public static final String KEY_ID = "_id";
    public static final String KEY_COORDINATES_LATITUDE = "coordinates_latitude";
    public static final String KEY_COORDINATES_LONGITUDE = "coordinates_longitude";
    public static final String KEY_UPLOADED = "uploaded";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_OBJECT_ID = "object_id";

    public static final String CREATE_TABLE_FOTO_INFO = "CREATE TABLE " + TABLE_NAME + "(" + KEY_ID +
            " INTEGER PRIMARY KEY, " + KEY_ADDRESS + " TEXT, " + KEY_COORDINATES_LATITUDE + " REAL, " + KEY_COORDINATES_LONGITUDE + " REAL, " +
            KEY_UPLOADED + " BLOB, " + KEY_OBJECT_ID + " TEXT" + ");";

    private static DBHelper instance;

    DBHelper dbFotosInfo;

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DBHelper getInstance(Context context){
        if(instance == null){
            instance = new DBHelper(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_FOTO_INFO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public long createFotoInfo(FotosInfo fotosInfo){
        long foto_id = -1;
        AsyncCreateFotos asyncCreateFotos = new AsyncCreateFotos();
        asyncCreateFotos.execute(fotosInfo);
        try {
            foto_id = asyncCreateFotos.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return foto_id;
    }

    public FotosInfo getFotosInfo(long foto_id){
        FotosInfo fotosInfo = new FotosInfo();
        AsyncGetFotos asyncGetFotos = new AsyncGetFotos();
        asyncGetFotos.execute(foto_id);
        try {
            fotosInfo = asyncGetFotos.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return fotosInfo;
    }

    public int updateFotosInfo(FotosInfo fotosInfo){
        int id = -1;
        AsyncUpdateFotos asyncUpdateFotos = new AsyncUpdateFotos();
        asyncUpdateFotos.execute(fotosInfo);
        try {
            id = asyncUpdateFotos.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return id;
    }

    public void deleteFotosInfo(long fotos_id){
        AsyncDeleteFotos asyncDeleteFotos = new AsyncDeleteFotos();
        asyncDeleteFotos.execute(fotos_id);
    }

    public Cursor getAllDataFotos() {
        AsyncGetAllDataFotos asyncGetAllDataFotos = new AsyncGetAllDataFotos();
        asyncGetAllDataFotos.execute();
        Cursor cursor = null;
        try {
            cursor = asyncGetAllDataFotos.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    class AsyncUpdateFotos extends AsyncTask<FotosInfo, Void, Integer> {
        @Override
        protected Integer doInBackground(FotosInfo... params) {
            FotosInfo fotosInfo;
            fotosInfo = params[0];
            dbFotosInfo = instance;
            SQLiteDatabase sqLiteDatabase = dbFotosInfo.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_UPLOADED, fotosInfo.isUploaded());
            contentValues.put(KEY_ADDRESS, fotosInfo.getAddress());
            contentValues.put(KEY_COORDINATES_LONGITUDE, fotosInfo.getLongitude());
            contentValues.put(KEY_COORDINATES_LATITUDE, fotosInfo.getLatitude());
            contentValues.put(KEY_OBJECT_ID, fotosInfo.getObjectId());
            return sqLiteDatabase.update(TABLE_NAME, contentValues, KEY_ID + " = ?",
                    new String[] { String.valueOf(fotosInfo.get_id()) });
        }
    }

    class AsyncGetFotos extends AsyncTask<Long, Void, FotosInfo>{
        @Override
        protected FotosInfo doInBackground(Long... params) {
            dbFotosInfo = instance;
            SQLiteDatabase sqLiteDatabase = dbFotosInfo.getWritableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_NAME + " WHERE " + KEY_ID + " = " + params[0];
            FotosInfo fotosInfo = new FotosInfo();
            Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
            if(cursor != null){
                if(cursor.moveToFirst()) {
                    fotosInfo.set_id(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                    fotosInfo.setUploaded(cursor.getInt(cursor.getColumnIndex(KEY_UPLOADED)) > 0);
                    fotosInfo.setLongitude(cursor.getFloat(cursor.getColumnIndex(KEY_COORDINATES_LONGITUDE)));
                    fotosInfo.setLatitude(cursor.getFloat(cursor.getColumnIndex(KEY_COORDINATES_LATITUDE)));
                    fotosInfo.setAddress(cursor.getString(cursor.getColumnIndex(KEY_ADDRESS)));
                    fotosInfo.setObjectId(cursor.getString(cursor.getColumnIndex(KEY_OBJECT_ID)));
                }
            }
            return fotosInfo;
        }
    }

    class AsyncCreateFotos extends AsyncTask<FotosInfo, Void, Long>{
        @Override
        protected Long doInBackground(FotosInfo... params) {
            dbFotosInfo = instance;
            FotosInfo fotosInfo;
            fotosInfo = params[0];
            ContentValues contentValues = new ContentValues();
            SQLiteDatabase sqLiteDatabase = dbFotosInfo.getWritableDatabase();
            contentValues.put(KEY_UPLOADED, fotosInfo.isUploaded());
            contentValues.put(KEY_ADDRESS, fotosInfo.getAddress());
            contentValues.put(KEY_COORDINATES_LONGITUDE, fotosInfo.getLongitude());
            contentValues.put(KEY_COORDINATES_LATITUDE, fotosInfo.getLatitude());
            contentValues.put(KEY_OBJECT_ID, fotosInfo.getObjectId());
            long foto_id = sqLiteDatabase.insert(TABLE_NAME, null, contentValues);
            return foto_id;
        }
    }

    class AsyncDeleteFotos extends AsyncTask<Long, Void, Void>{
        @Override
        protected Void doInBackground(Long... params) {
            dbFotosInfo = instance;
            long fotos_id = params[0];
            SQLiteDatabase sqLiteDatabase = dbFotosInfo.getWritableDatabase();
            sqLiteDatabase.delete(TABLE_NAME, KEY_ID + " = ?", new String[]{String.valueOf(fotos_id)});
            return null;
        }
    }

    class AsyncGetAllDataFotos extends AsyncTask<Void, Void, Cursor>{
        @Override
        protected Cursor doInBackground(Void... params) {
            dbFotosInfo = instance;
            SQLiteDatabase sqLiteDatabase = dbFotosInfo.getWritableDatabase();
            return sqLiteDatabase.query(TABLE_NAME, null, null, null, null, null, null);
        }
    }
}
