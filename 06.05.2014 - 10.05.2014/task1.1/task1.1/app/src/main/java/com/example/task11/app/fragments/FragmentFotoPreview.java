package com.example.task11.app.fragments;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.task11.app.R;
import com.example.task11.app.common.DBHelper;
import com.example.task11.app.common.FotosInfo;
import com.example.task11.app.common.MainActivity;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * Created by androiddev9 on 06.05.14.
 */
public class FragmentFotoPreview extends Fragment implements View.OnClickListener {

    final static int STATUS_END_DOWNLOADING = 1;
    final static int STATUS_ASSOCIATE_CLASS = 2;
    final static int STATUS_UPLOAD = 3;

    ImageView imageView;
    String path;
    Button btnUpload;
    long id;
    DBHelper dbFotosInfo;
    HttpPost request;
    double lon = 0;
    double lat = 0;
    ProgressDialog progressDialog;
    Handler handler;
    Bitmap myBitmap;
    String[] s = new String[2];
    String[] res = new String[2];

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int rotate = 0;
        View view = inflater.inflate(R.layout.foto_preview, container, false);
        ActionBar actionBar = getActivity().getActionBar();
        actionBar.setTitle("Foto preview");
        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what){
                    case STATUS_END_DOWNLOADING:
                        progressDialog.dismiss();
                        imageView.setImageBitmap(myBitmap);
                        break;
                    case STATUS_ASSOCIATE_CLASS:
                        if(getActivity()!=null) {
                            Toast.makeText(getActivity(), s[0], Toast.LENGTH_LONG).show();
                        }
                        FotosInfo fotosInfo = dbFotosInfo.getFotosInfo(id);
                        fotosInfo.setObjectId(s[1]);
                        dbFotosInfo.updateFotosInfo(fotosInfo);
                        break;
                    case STATUS_UPLOAD:
                        String b = "responseCode = 201; message: Created";
                        progressDialog.dismiss();
                        if(getActivity()!=null) {
                            Toast.makeText(getActivity(), res[0], Toast.LENGTH_LONG).show();
                        }
                        if(res[0].equals(b)){
                            btnUpload.setEnabled(false);
                            FotosInfo fotoInfo = dbFotosInfo.getFotosInfo(id);
                            fotoInfo.setUploaded(true);
                            dbFotosInfo.updateFotosInfo(fotoInfo);
                            AssociateClassThread associateClassThread = new AssociateClassThread();
                            associateClassThread.setNameFile(res[1]);
                            associateClassThread.start();
                        }
                        break;
                }
            }
        };
        dbFotosInfo = DBHelper.getInstance(getActivity());
        Bundle bundle = getArguments();
        id = bundle.getLong(FragmentMain.KEY_BUNDLE);
        FotosInfo fotosInfo;
        fotosInfo = dbFotosInfo.getFotosInfo(id);
        path = fotosInfo.getAddress();
        lat = fotosInfo.getLatitude();
        lon = fotosInfo.getLongitude();
        imageView = (ImageView) view.findViewById(R.id.imageViewFotoPreview);
        btnUpload = (Button) view.findViewById(R.id.buttonUpload);
        btnUpload.setOnClickListener(this);
        if(fotosInfo.isUploaded()){
            btnUpload.setEnabled(false);
        }
        ImageLoader imageLoader = ImageLoader.getInstance();
        if(imageLoader.isInited()) {
            imageLoader.destroy();
        }
        File cacheDir = StorageUtils.getCacheDirectory(getActivity());
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .discCacheExtraOptions(imageView.getWidth(), imageView.getHeight(), Bitmap.CompressFormat.JPEG, 2, null) // width, height, compress format, quality
                .threadPoolSize(5)
                .threadPriority(Thread.NORM_PRIORITY - 1)
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .denyCacheImageMultipleSizesInMemory()
                .discCache(new UnlimitedDiscCache(cacheDir)) // default
                .imageDownloader(new BaseImageDownloader(getActivity()))
                .imageDecoder(new BaseImageDecoder(true)) /** was empty in brackets **/
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .writeDebugLogs()
                .build();
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(android.R.drawable.button_onoff_indicator_on)
                .showImageForEmptyUri(android.R.drawable.ic_menu_close_clear_cancel)
                .showImageOnFail(android.R.drawable.btn_dialog)
                .resetViewBeforeLoading(true)
                .cacheInMemory(false)
                .cacheOnDisc(true)
                .considerExifParams(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new SimpleBitmapDisplayer())
                .handler(new Handler())
                .build();
        imageLoader.init(config);
        if(path != null) {
            imageLoader.displayImage(path, imageView);
        }
        return view;
    }

    @Override
    public void onClick(View v) {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Uploading file to Parse.com");
        progressDialog.setMessage("Please wait");
        progressDialog.show();
        UploadThread uploadThread = new UploadThread();
        uploadThread.start();
    }

    private static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    private String nameGenerator(){
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String output = sb.toString();
        return output;
    }

    class UploadThread extends Thread{
        @Override
        public void run() {
            HttpClient client = new DefaultHttpClient();
            HttpResponse httpResponse;
            try {
                String name = nameGenerator();
                String url = "https://api.parse.com/1/files/" + name + ".jpg";
                request = new HttpPost(url);
                String s = path.replace("file://", "");
                InputStream is = new FileInputStream(new File(s));//path));
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                byte[] b = new byte[1024];
                int bytesRead;
                while((bytesRead = is.read(b))!= -1){
                    bos.write(b, 0, bytesRead);
                }
                byte[] bytes = bos.toByteArray();

                request.setHeader(MainActivity.PARSE_APPLICATION_ID_HEADER, MainActivity.PARSE_APPLICATION_ID);
                request.setHeader(MainActivity.PARSE_REST_API_KEY_HEADER, MainActivity.PARSE_REST_API_KEY);
                request.setHeader(MainActivity.PARSE_CONTENT_TYPE_HEADER, "image/jpeg");
                request.setEntity(new ByteArrayEntity(bytes));
                httpResponse = client.execute(request);
                int responseCode = httpResponse.getStatusLine().getStatusCode();
                String message = httpResponse.getStatusLine().getReasonPhrase();
                Log.d("MyLogs", "responseCode = " + responseCode + "; message: " + message);
                String result = "responseCode = ";
                result = result.concat(String.valueOf(responseCode));
                result = result.concat("; message: ");
                result = result.concat(message);
                HttpEntity entity = httpResponse.getEntity();
                if (entity != null) {
                    InputStream instream = entity.getContent();
                    String response = convertStreamToString(instream);
                    Log.d("MyLogs", "response = " + response);
                    instream.close();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        res[1] = jsonObject.getString(MainActivity.JSON_HEADER_NAME);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                res[0] = result;
                handler.sendEmptyMessage(STATUS_UPLOAD);
            } catch (ClientProtocolException e)  {
                client.getConnectionManager().shutdown();
                e.printStackTrace();
            } catch (IOException e) {
                client.getConnectionManager().shutdown();
                e.printStackTrace();
            }
        }
    }

    class AssociateClassThread extends Thread{

        String name;

        public void setNameFile(String s){
            name = s;
        }

        @Override
        public void run() {
            String objectId;
            HttpClient client = new DefaultHttpClient();
            HttpResponse httpResponse;
            try {
                String url = "https://api.parse.com/1/classes/" + MainActivity.PARSE_NAME;
                request = new HttpPost(url);
                request.setHeader(MainActivity.PARSE_APPLICATION_ID_HEADER, MainActivity.PARSE_APPLICATION_ID);
                request.setHeader(MainActivity.PARSE_REST_API_KEY_HEADER, MainActivity.PARSE_REST_API_KEY);
                request.setHeader(MainActivity.PARSE_CONTENT_TYPE_HEADER, "application/json");
                JSONObject object = new JSONObject();
                JSONObject object1 = new JSONObject();
                object1.put(MainActivity.JSON_HEADER_NAME, name);
                object1.put("__type", "File");
                object.put(MainActivity.JSON_HEADER_LON, "" + lon);
                object.put(MainActivity.JSON_HEADER_LAT, "" + lat);
                object.put(MainActivity.JSON_HEADER_PICTURE, object1);
                byte[] bytes = object.toString().getBytes();
                request.setEntity(new ByteArrayEntity(bytes));
                httpResponse = client.execute(request);
                int responseCode = httpResponse.getStatusLine().getStatusCode();
                String message = httpResponse.getStatusLine().getReasonPhrase();
                Log.d("MyLogs", "A.responseCode = " + responseCode + "; message: " + message);
                String result = "A.responseCode = ";
                result = result.concat(String.valueOf(responseCode));
                result = result.concat("; message: ");
                result = result.concat(message);
                s[0] = result;
                HttpEntity entity = httpResponse.getEntity();
                if (entity != null) {
                    InputStream instream = entity.getContent();
                    String response = convertStreamToString(instream);
                    Log.d("MyLogs", "A.response = " + response);
                    JSONObject jsonObject = new JSONObject(response);
                    objectId = jsonObject.getString(MainActivity.JSON_HEADER_OBJECT_ID);
                    s[1] = objectId;
                    instream.close();
                }
                handler.sendEmptyMessage(STATUS_ASSOCIATE_CLASS);
            } catch (ClientProtocolException e)  {
                client.getConnectionManager().shutdown();
                e.printStackTrace();
            } catch (IOException e) {
                client.getConnectionManager().shutdown();
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
