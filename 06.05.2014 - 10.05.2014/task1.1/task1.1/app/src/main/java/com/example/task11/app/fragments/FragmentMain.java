package com.example.task11.app.fragments;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.task11.app.R;
import com.example.task11.app.common.DBHelper;
import com.example.task11.app.common.FotosInfo;
import com.example.task11.app.common.MainActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;


/**
 * Created by androiddev9 on 06.05.14.
 */
public class FragmentMain extends Fragment implements View.OnClickListener{

    final static String KEY_BUNDLE = "id";
    final int REQUEST_CODE_PHOTO = 1;
    final static int STATUS_GET_CLASS = 4;

    File directory;
    File fileSave;
    double longitude = 0;
    double latitude = 0;
    DBHelper dbFotoInfo;
    private LocationManager locationManager;
    long id;
    Handler handler;
    String s;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);
        ActionBar actionBar = getActivity().getActionBar();
        actionBar.setTitle("Main fragment");
        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what){
                    case STATUS_GET_CLASS:
                        JSONArray jsonArray;
                        JSONObject jsonObject;
                        try {
                            jsonObject = new JSONObject(s);
                            jsonArray = jsonObject.names();
                            jsonArray = jsonObject.toJSONArray(jsonArray);
                            jsonArray = jsonArray.getJSONArray(0);

                            for(int i = 0; i < jsonArray.length(); i++){
                                jsonObject = jsonArray.getJSONObject(i);
                                String objectId = jsonObject.getString(MainActivity.JSON_HEADER_OBJECT_ID);
                                String selection = DBHelper.KEY_OBJECT_ID + " = ?";
                                String[] selectionArgs = new String[]{objectId};
                                Cursor cursor = dbFotoInfo.getWritableDatabase().query(DBHelper.TABLE_NAME, null, selection, selectionArgs, null, null, null);
                                if((cursor.getCount() == 0)||(cursor==null)){
                                    Activity activity = getActivity();
                                    if(activity!=null)
                                        Toast.makeText(activity, "Found new photo on Parse.com", Toast.LENGTH_SHORT).show();
                                    FotosInfo fotosInfo = new FotosInfo();
                                    JSONObject obj = jsonObject.getJSONObject(MainActivity.JSON_HEADER_PICTURE);
                                    fotosInfo.setAddress(obj.getString(MainActivity.JSON_HEADER_URL));
                                    fotosInfo.setObjectId(jsonObject.getString(MainActivity.JSON_HEADER_OBJECT_ID));
                                    fotosInfo.setUploaded(true);
                                    fotosInfo.setLatitude(jsonObject.getDouble(MainActivity.JSON_HEADER_LAT));
                                    fotosInfo.setLongitude(jsonObject.getDouble(MainActivity.JSON_HEADER_LON));
                                    dbFotoInfo.createFotoInfo(fotosInfo);
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        };
        dbFotoInfo = DBHelper.getInstance(getActivity());
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Button btnMakeFoto = (Button) view.findViewById(R.id.buttonMakeFoto);
        Button btnLookFoto = (Button) view.findViewById(R.id.buttonLookAtFoto);
        Button btnLookMap = (Button) view.findViewById(R.id.buttonLookAtMap);
        btnMakeFoto.setOnClickListener(this);
        btnLookFoto.setOnClickListener(this);
        btnLookMap.setOnClickListener(this);
        GetClassThread getClassThread = new GetClassThread();
        getClassThread.start();
        createDirectory();
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE_PHOTO){
            if(resultCode == MainActivity.RESULT_OK){
                FotosInfo fotosInfo = new FotosInfo();
                String buf = "file://";
                String path = String.valueOf(fileSave);
                path = buf.concat(path);
                fotosInfo.setAddress(path);
                fotosInfo.setLatitude(latitude);
                fotosInfo.setLongitude(longitude);
                fotosInfo.setUploaded(false);
                id = dbFotoInfo.createFotoInfo(fotosInfo);
                checkEnabled();
                FragmentFotoPreview fragmentFotoPreview = newInstance(id);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.relative_layout, fragmentFotoPreview);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }else if(resultCode == MainActivity.RESULT_CANCELED){
                if( getActivity()!= null) {
                    Toast.makeText(getActivity(), "Canceled", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonMakeFoto:
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                fileSave = generateFileUri();
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileSave));
                startActivityForResult(intent, REQUEST_CODE_PHOTO);
                break;
            case R.id.buttonLookAtFoto:
                ImageLoaderFragment imageLoaderFragment = new ImageLoaderFragment();
                FragmentTransaction fragmentTransactions = getFragmentManager().beginTransaction();
                fragmentTransactions.replace(R.id.relative_layout, imageLoaderFragment);
                fragmentTransactions.addToBackStack(null);
                fragmentTransactions.commit();
                break;
            case R.id.buttonLookAtMap:
                MapsFragment mapsFragment = new MapsFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.relative_layout, mapsFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
        }
    }
    private File generateFileUri() {
        File file;
        file = new File(directory.getPath() + "/" + "photo_" + System.currentTimeMillis() + ".jpg");
        return file;
    }

    private void createDirectory() {
        directory = new File(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"MyFolder");
        if (!directory.exists())
            directory.mkdirs();
    }

    public static FragmentFotoPreview newInstance(long id){
        FragmentFotoPreview fragmentFotoPreview = new FragmentFotoPreview();

        Bundle bundle = new Bundle();
        bundle.putLong(KEY_BUNDLE, id);
        fragmentFotoPreview.setArguments(bundle);
        return fragmentFotoPreview;
    }

    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            showLocation(location);
            if(location != null){
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                if(getActivity()!=null) {
                    Toast.makeText(getActivity(), "Coordinates detected", Toast.LENGTH_SHORT).show();
                }
            } else {
                if(getActivity()!=null) {
                    Toast.makeText(getActivity(), "Coordinates not detected", Toast.LENGTH_SHORT).show();
                }
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            checkEnabled();
        }

        @Override
        public void onProviderEnabled(String provider) {
            checkEnabled();
            Location location = locationManager.getLastKnownLocation(provider);
            if(location != null){
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                if(getActivity()!=null) {
                    Toast.makeText(getActivity(), "Coordinates detected", Toast.LENGTH_SHORT).show();
                }
            } else {
                if(getActivity()!=null) {
                    Toast.makeText(getActivity(), "Coordinates not detected", Toast.LENGTH_SHORT).show();
                }
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (provider.equals(LocationManager.GPS_PROVIDER)) {
                Log.d("MyLogs", "Status GPS changed = " + status);
            } else if (provider.equals(LocationManager.NETWORK_PROVIDER)) {
                Log.d("MyLogs", "Status network changed = " + status);
            }
        }
    };

    private void showLocation(Location location) {
        if (location == null)
            return;
        if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
            Log.d("MyLogs", "Location GPS = " + formatLocation(location));
        } else if (location.getProvider().equals(LocationManager.NETWORK_PROVIDER)) {
            Log.d("MyLogs", "Location network = " + formatLocation(location));
        }
        locationManager.removeUpdates(locationListener);
    }

    private String formatLocation(Location location) {
        if (location == null)
            return "";
        return String.format("Coordinates: lat = %1$.4f, lon = %2$.4f, time = %3$tF %3$tT",
                location.getLatitude(), location.getLongitude(), new Date(location.getTime()));
    }

    private void checkEnabled() {
        Log.d("MyLogs", "Enabled GPS = " + locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));
        Log.d("MyLogs", "Enabled network = " + locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
    }

    @Override
    public void onPause() {
        super.onPause();
        ActionBar actionBar = getActivity().getActionBar();
        actionBar.setHomeButtonEnabled(true);
        locationManager.removeUpdates(locationListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = getActivity().getActionBar();
        actionBar.setHomeButtonEnabled(false);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000 * 2, 10, locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000 * 2, 10,locationListener);
    }

    public static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    class GetClassThread extends Thread{
        @Override
        public void run() {
            HttpClient client = new DefaultHttpClient();
            HttpResponse httpResponse;
            HttpGet request;
            try {
                String url = "https://api.parse.com/1/classes/" + MainActivity.PARSE_NAME;
                request = new HttpGet(url);
                request.setHeader(MainActivity.PARSE_APPLICATION_ID_HEADER, MainActivity.PARSE_APPLICATION_ID);
                request.setHeader(MainActivity.PARSE_REST_API_KEY_HEADER, MainActivity.PARSE_REST_API_KEY);
                httpResponse = client.execute(request);
                int responseCode = httpResponse.getStatusLine().getStatusCode();
                String message = httpResponse.getStatusLine().getReasonPhrase();
                Log.d("MyLogs", "B.responseCode = " + responseCode + "; message: " + message);
                HttpEntity entity = httpResponse.getEntity();
                if (entity != null) {
                    InputStream instream = entity.getContent();
                    String response = convertStreamToString(instream);
                    s = response;
//                    Log.d("MyLogs", "B.response = " + response);
                    instream.close();
                }
                handler.sendEmptyMessage(STATUS_GET_CLASS);
            } catch (ClientProtocolException e)  {
                client.getConnectionManager().shutdown();
                e.printStackTrace();
            } catch (IOException e) {
                client.getConnectionManager().shutdown();
                e.printStackTrace();
            }
        }
    }

}
