package com.example.task11.app.fragments;

import android.app.ActionBar;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.task11.app.R;
import com.example.task11.app.common.DBHelper;
import com.example.task11.app.common.FotosInfo;
import com.example.task11.app.common.MainActivity;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by androiddev9 on 08.05.14.
 */
public class ImageLoaderFragment extends Fragment{

    private final int CM_DELETE = 1;

    DBHelper dbFotosInfo;
    ListView listView;
    ImageLoader loader = ImageLoader.getInstance();
    DisplayImageOptions options;
    String objectId;
    private CustomCursorAdapter customAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_loader, container, false);
        ActionBar actionBar = getActivity().getActionBar();
        actionBar.setTitle("All images");
        dbFotosInfo = DBHelper.getInstance(getActivity());
        listView = (ListView) view.findViewById(R.id.listViewImageLoader);
        registerForContextMenu(listView);

        File cacheDir = StorageUtils.getCacheDirectory(getActivity());
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .discCacheExtraOptions(720, 1024, Bitmap.CompressFormat.JPEG, 75, null) // width, height, compress format, quality
                .threadPoolSize(5)
                .threadPriority(Thread.NORM_PRIORITY - 1)
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .denyCacheImageMultipleSizesInMemory()
                .discCache(new UnlimitedDiscCache(cacheDir)) // default
                .imageDownloader(new BaseImageDownloader(getActivity()))
                .imageDecoder(new BaseImageDecoder(true)) /** was empty in brackets **/
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .writeDebugLogs()
                .build();

        loader.init(config);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(android.R.drawable.button_onoff_indicator_on)
                .showImageForEmptyUri(android.R.drawable.ic_menu_close_clear_cancel)
                .showImageOnFail(android.R.drawable.btn_dialog)
                .resetViewBeforeLoading(false)
                .cacheInMemory(false)
                .cacheOnDisc(true)
                .considerExifParams(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new SimpleBitmapDisplayer())
                .handler(new Handler())
                .build();
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                customAdapter = new CustomCursorAdapter(getActivity(), dbFotosInfo.getAllDataFotos());
                listView.setAdapter(customAdapter);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                FragmentFotoPreview fragmentFotoPreview = FragmentMain.newInstance(id);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.relative_layout, fragmentFotoPreview);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        return view;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        switch (v.getId()){
            case R.id.listViewImageLoader:
                menu.add(0, CM_DELETE, 0, R.string.delete_record);
                break;
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getItemId() == CM_DELETE){
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();
            FotosInfo fotosInfo;
            fotosInfo = dbFotosInfo.getFotosInfo(acmi.id);
            dbFotosInfo.deleteFotosInfo(acmi.id);
            objectId = fotosInfo.getObjectId();
            if(fotosInfo.isUploaded()){
                AsyncDeleteFromParse asyncDeleteFromParse = new AsyncDeleteFromParse();
                asyncDeleteFromParse.start();
            }
            customAdapter.swapCursor(dbFotosInfo.getAllDataFotos());
            customAdapter.notifyDataSetChanged();
        }
        return super.onContextItemSelected(item);
    }

    class AsyncDeleteFromParse extends Thread{
        @Override
        public void run() {
            HttpClient client = new DefaultHttpClient();
            HttpResponse httpResponse;
            String url = "https://api.parse.com/1/classes/" + MainActivity.PARSE_NAME + "/" + objectId;
            HttpDelete request = new HttpDelete(url);
            request.setHeader(MainActivity.PARSE_APPLICATION_ID_HEADER, MainActivity.PARSE_APPLICATION_ID);
            request.setHeader(MainActivity.PARSE_REST_API_KEY_HEADER, MainActivity.PARSE_REST_API_KEY);
            try {
                httpResponse = client.execute(request);
                int responseCode = httpResponse.getStatusLine().getStatusCode();
                String message = httpResponse.getStatusLine().getReasonPhrase();
                Log.d("MyLogs", "C.responseCode = " + responseCode + "; message: " + message);
                HttpEntity entity = httpResponse.getEntity();
                if (entity != null) {
                    InputStream instream = entity.getContent();
                    String response = FragmentMain.convertStreamToString(instream);
                    Log.d("MyLogs", "C.response = " + response);
                    instream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public class CustomCursorAdapter extends CursorAdapter {

        private class ViewHolder {
            public TextView text;
            public ImageView image;
        }

        public CustomCursorAdapter(Context context, Cursor c) {
            super(context, c);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            // when the view will be created for first time,
            // we need to tell the adapters, how each item will look
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View retView = inflater.inflate(R.layout.list_view_item, parent, false);

            return retView;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {

            String objectId = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_OBJECT_ID));
            String addr = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_ADDRESS));
            final ViewHolder holder;
            holder = new ViewHolder();
            holder.text = (TextView) view.findViewById(R.id.text);
            holder.image = (ImageView) view.findViewById(R.id.image);
            view.setTag(holder);
            holder.text.setText("Id on Parse.com:  " + (objectId));
            loader.displayImage(addr, holder.image, options);
        }
    }
}
