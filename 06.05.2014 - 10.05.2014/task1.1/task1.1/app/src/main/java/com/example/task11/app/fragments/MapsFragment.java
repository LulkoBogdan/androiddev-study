package com.example.task11.app.fragments;

import android.app.ActionBar;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.task11.app.R;
import com.example.task11.app.common.DBHelper;
import com.example.task11.app.common.FotosInfo;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;

/**
 * Created by androiddev9 on 08.05.14.
 */
public class MapsFragment extends Fragment implements GoogleMap.OnInfoWindowClickListener{

    private static View view;

    ImageLoader imageLoader;
    File cacheDir;
    DBHelper dbFotoInfo;
    GoogleMap googleMap;
    DisplayImageOptions options;
    private Marker marker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view != null){
            ViewGroup parent = (ViewGroup) view.getParent();
            if(parent != null){
                parent.removeView(view);
            }
        }
        try{
            view = inflater.inflate(R.layout.fragment_google_maps, container, false);
        }catch (InflateException e){
        }
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(android.R.drawable.button_onoff_indicator_on)
                .showImageForEmptyUri(android.R.drawable.ic_menu_close_clear_cancel)
                .showImageOnFail(android.R.drawable.btn_dialog)
                .resetViewBeforeLoading(false)
                .cacheInMemory(true)
                .cacheOnDisc(false)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new SimpleBitmapDisplayer())
                .handler(new Handler()) /** maybe not correct import for Handler **/
                .build();
        imageLoader = ImageLoader.getInstance(); // Получили экземпляр
        cacheDir = StorageUtils.getCacheDirectory(getActivity());
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .memoryCacheExtraOptions(100, 100) // width, height
                .threadPoolSize(5)
                .threadPriority(Thread.NORM_PRIORITY - 1)
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .imageDownloader(new BaseImageDownloader(getActivity()))
                .imageDecoder(new BaseImageDecoder(true)) /** was empty in brackets **/
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .writeDebugLogs()
                .build();
        imageLoader.init(config);

        ActionBar actionBar = getActivity().getActionBar();
        actionBar.setTitle("Google maps");
        dbFotoInfo = DBHelper.getInstance(getActivity());
        SupportMapFragment supportMapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.mapView);
        googleMap = supportMapFragment.getMap();
        googleMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
        googleMap.setOnInfoWindowClickListener(this);
        Cursor cursor = dbFotoInfo.getAllDataFotos();
        if(cursor != null){
            if(cursor.moveToFirst()){
                do {
                    double lon = cursor.getDouble(cursor.getColumnIndex(DBHelper.KEY_COORDINATES_LONGITUDE));
                    double lat = cursor.getDouble(cursor.getColumnIndex(DBHelper.KEY_COORDINATES_LATITUDE));
                    int id = cursor.getInt(cursor.getColumnIndex(DBHelper.KEY_ID));
                    LatLng latLng = new LatLng(lat, lon);
                    Marker newMarker = googleMap.addMarker(new MarkerOptions().position(latLng).snippet(String.valueOf(id)));
                    newMarker.setTitle(newMarker.getId());
                }while(cursor.moveToNext());
            }
        }
        return view;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        String s_id = marker.getSnippet();
        int id = Integer.parseInt(s_id);
        FragmentFotoPreview fragmentFotoPreview = FragmentMain.newInstance(id);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.relative_layout, fragmentFotoPreview);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter(){
            myContentsView = getActivity().getLayoutInflater().inflate(R.layout.custom_info_contents, null);
        }

        @Override
        public View getInfoContents(Marker marker) {
            if (MapsFragment.this.marker != null
                    && MapsFragment.this.marker.isInfoWindowShown()) {
                MapsFragment.this.marker.hideInfoWindow();
                MapsFragment.this.marker.showInfoWindow();
            }
            return null;
        }

        @Override
        public View getInfoWindow(final Marker marker) {
            MapsFragment.this.marker = marker;
            String url;
            String s_id = marker.getSnippet();
            int id = Integer.parseInt(s_id);
            FotosInfo fotosInfo = dbFotoInfo.getFotosInfo(id);
            url = fotosInfo.getAddress();
            final ImageView image = ((ImageView) myContentsView.findViewById(R.id.icon));
            imageLoader.displayImage(url, image, options,new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri,View view, Bitmap loadedImage) {
                    super.onLoadingComplete(imageUri, view,loadedImage);
                    getInfoContents(marker);
                }
            });
            return myContentsView;
        }

    }
}
