package com.example.task11.app.common;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;

import com.example.task11.app.R;
import com.example.task11.app.fragments.FragmentMain;

public class MainActivity extends FragmentActivity {

    DBHelper dbFotoInfo;
    public static final String PARSE_NAME = "FotosInfo";

    public static final String PARSE_APPLICATION_ID_HEADER = "X-Parse-Application-Id";
    public static final String PARSE_REST_API_KEY_HEADER = "X-Parse-REST-API-Key";
    public static final String PARSE_APPLICATION_ID = "DwqmymsCwNWU0ket7PckBaoSiRVXpzLfnjDIH7T6";
    public static final String PARSE_REST_API_KEY = "k0AmDO9yTTaJhKEFX5cWV21FxRw8CYLoADmNm5c2";
    public static final String PARSE_CONTENT_TYPE_HEADER = "Content-Type";

    public static final String JSON_HEADER_NAME = "name";
    public static final String JSON_HEADER_OBJECT_ID = "objectId";
    public static final String JSON_HEADER_LON = "lon";
    public static final String JSON_HEADER_LAT = "lat";
    public static final String JSON_HEADER_PICTURE = "picture";
    public static final String JSON_HEADER_URL = "url";

    public static final int LOADER_ID = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbFotoInfo = DBHelper.getInstance(this);
        FragmentMain fragmentMain = new FragmentMain();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.relative_layout, fragmentMain);
        fragmentTransaction.commit();
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(dbFotoInfo!=null){
            dbFotoInfo.close();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if(android.R.id.home == id) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
