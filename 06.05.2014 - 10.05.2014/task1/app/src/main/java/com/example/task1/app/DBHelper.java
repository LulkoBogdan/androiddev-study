package com.example.task1.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by androiddev9 on 07.05.14.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "databaseFotoInfo";

    public static final String TABLE_NAME = "table_foto_info";

    public static final String KEY_ID = "_id";
    public static final String KEY_COORDINATES_LATITUDE = "coordinates_latitude";
    public static final String KEY_COORDINATES_LONGITUDE = "coordinates_longitude";
    public static final String KEY_UPLOADED = "uploaded";
    public static final String KEY_ADDRESS = "address";

    public static final String CREATE_TABLE_FOTO_INFO = "CREATE TABLE " + TABLE_NAME + "(" + KEY_ID +
            " INTEGER PRIMARY KEY, " + KEY_ADDRESS + " TEXT, " + KEY_COORDINATES_LATITUDE + " REAL, " + KEY_COORDINATES_LONGITUDE + " REAL, " +
            KEY_UPLOADED + " BLOB" + ");";

    private static DBHelper instance;

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DBHelper getInstance(Context context){
        if(instance == null){
            instance = new DBHelper(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_FOTO_INFO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public long createFotoInfo(FotosInfo fotosInfo){
        ContentValues contentValues = new ContentValues();
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        contentValues.put(KEY_UPLOADED, fotosInfo.isUploaded());
        contentValues.put(KEY_ADDRESS, fotosInfo.getAddress());
        contentValues.put(KEY_COORDINATES_LONGITUDE, fotosInfo.getLongitude());
        contentValues.put(KEY_COORDINATES_LATITUDE, fotosInfo.getLatitude());
        long foto_id = sqLiteDatabase.insert(TABLE_NAME, null, contentValues);
        return foto_id;
    }

    public FotosInfo getFotosInfo(long foto_id){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " WHERE " + KEY_ID + " = " + foto_id;
        FotosInfo fotosInfo = new FotosInfo();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if(cursor != null){
            cursor.moveToFirst();
            fotosInfo.set_id(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
            fotosInfo.setUploaded(cursor.getInt(cursor.getColumnIndex(KEY_UPLOADED))>0);
            fotosInfo.setLongitude(cursor.getFloat(cursor.getColumnIndex(KEY_COORDINATES_LONGITUDE)));
            fotosInfo.setLatitude(cursor.getFloat(cursor.getColumnIndex(KEY_COORDINATES_LATITUDE)));
            fotosInfo.setAddress(cursor.getString(cursor.getColumnIndex(KEY_ADDRESS)));
        }
        return fotosInfo;
    }

    public int updateFotosInfo(FotosInfo fotosInfo){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_UPLOADED, fotosInfo.isUploaded());
        contentValues.put(KEY_ADDRESS, fotosInfo.getAddress());
        contentValues.put(KEY_COORDINATES_LONGITUDE, fotosInfo.getLongitude());
        contentValues.put(KEY_COORDINATES_LATITUDE, fotosInfo.getLatitude());
        return sqLiteDatabase.update(TABLE_NAME, contentValues, KEY_ID + " = ?",
                new String[] { String.valueOf(fotosInfo.get_id()) });
    }

    public void deleteGoods(long fotos_id){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.delete(TABLE_NAME, KEY_ID + " = ?", new String[]{String.valueOf(fotos_id)});
    }

    public Cursor getAllDataGoods() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        return sqLiteDatabase.query(TABLE_NAME, null, null, null, null, null, null);
    }
}
