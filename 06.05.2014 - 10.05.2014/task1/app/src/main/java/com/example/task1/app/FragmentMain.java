package com.example.task1.app;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;

/**
 * Created by androiddev9 on 06.05.14.
 */
public class FragmentMain extends Fragment implements View.OnClickListener{

    final int REQUEST_CODE_PHOTO = 1;
    File directory;
    File fileSave;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);
        Button btnMakeFoto = (Button) view.findViewById(R.id.buttonMakeFoto);
        Button btnLookFoto = (Button) view.findViewById(R.id.buttonLookAtFoto);
        Button btnLookMap = (Button) view.findViewById(R.id.buttonLookAtMap);
        btnMakeFoto.setOnClickListener(this);
        btnLookFoto.setOnClickListener(this);
        btnLookMap.setOnClickListener(this);
        createDirectory();
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE_PHOTO){
            if(resultCode == MainActivity.RESULT_OK){
                FragmentFotoPreview fragmentFotoPreview = new FragmentFotoPreview(fileSave);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.relative_layout, fragmentFotoPreview);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }else if(resultCode == MainActivity.RESULT_CANCELED){
                Toast.makeText(getActivity(), "Canceled", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonMakeFoto:
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                fileSave = generateFileUri();
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileSave));
                startActivityForResult(intent, REQUEST_CODE_PHOTO);
                break;
            case R.id.buttonLookAtFoto:
                break;
            case R.id.buttonLookAtMap:
                break;
        }
    }
    private File generateFileUri() {
        File file;
        file = new File(directory.getPath() + "/" + "photo_" + System.currentTimeMillis() + ".jpg");
        Log.d("MyLogs", "FragmentMain" + file);
        return file;
    }

    private void createDirectory() {
        directory = new File(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"MyFolder");
        if (!directory.exists())
            directory.mkdirs();
    }
}
