package com.example.task1.app;

import android.annotation.TargetApi;

import android.support.v4.app.FragmentTransaction;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.PushService;

public class MainActivity extends ActionBarActivity {

    DBHelper dbFotoInfo;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Parse.initialize(this, "DwqmymsCwNWU0ket7PckBaoSiRVXpzLfnjDIH7T6", "TbQWU128bAfMTgFuo9drPkZDKY5E6DgWKg06izWc");
        PushService.setDefaultPushCallback(this, DefaultActivity.class);
        ParseInstallation.getCurrentInstallation().saveInBackground();
        dbFotoInfo = DBHelper.getInstance(this);
        FragmentMain fragmentMain = new FragmentMain();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.relative_layout, fragmentMain);
        fragmentTransaction.commit();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(dbFotoInfo!=null){
            dbFotoInfo.close();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
