package com.example.task1.app;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * Created by androiddev9 on 06.05.14.
 */
public class FragmentFotoPreview extends Fragment implements View.OnClickListener {

    private LocationManager locationManager;
    DBHelper dbFotoInfo;
    double longitude;
    double latitude;
    ImageView imageView;
    String path;
    Button btnUpload;
    Cursor cursor;

    @Override
    public void onResume() {
        super.onResume();
        String selection = DBHelper.KEY_ADDRESS + " = ?";
        String[] selectionArgs = new String[]{path};
        cursor = dbFotoInfo.getWritableDatabase().query(DBHelper.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        if(cursor == null){
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000 * 3, 10, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000 * 3, 10,locationListener);
            checkEnabled();
        } else {
            if(cursor.moveToFirst()){
                boolean upload = cursor.getInt(cursor.getColumnIndex(DBHelper.KEY_UPLOADED))>0;
                btnUpload.setEnabled(!upload);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        locationManager.removeUpdates(locationListener);
    }


    public FragmentFotoPreview(File uri) {
        path = String.valueOf(uri);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int rotate = 0;
        View view = inflater.inflate(R.layout.foto_preview, container, false);
        dbFotoInfo = DBHelper.getInstance(getActivity());
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        imageView = (ImageView) view.findViewById(R.id.imageViewFotoPreview);
        btnUpload = (Button) view.findViewById(R.id.buttonUpload);
        btnUpload.setOnClickListener(this);
        try {
            ExifInterface exif = new ExifInterface(path);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(getActivity(), "Error!",Toast.LENGTH_LONG).show();
        }

        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        File imgFile = new File(path);
        if(imgFile.exists()){
            Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap , 0, 0, bitmap .getWidth(), bitmap .getHeight(), matrix, true);
            imageView.setImageBitmap(rotatedBitmap);
        } else {
            Log.d("MyLogs", " Not found path " + path);
        }

        return view;
    }

    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            showLocation(location);
        }

        @Override
        public void onProviderDisabled(String provider) {
            checkEnabled();
        }

        @Override
        public void onProviderEnabled(String provider) {
            checkEnabled();
            Location location = locationManager.getLastKnownLocation(provider);
            if(location != null){
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                FotosInfo fotosInfo = new FotosInfo();
                fotosInfo.setLatitude(latitude);
                fotosInfo.setLongitude(longitude);
                fotosInfo.setAddress(path);
                fotosInfo.setUploaded(false);
                dbFotoInfo.createFotoInfo(fotosInfo);
                locationManager.removeUpdates(locationListener);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (provider.equals(LocationManager.GPS_PROVIDER)) {
                Log.d("MyLogs", "Status GPS changed = " + status);
            } else if (provider.equals(LocationManager.NETWORK_PROVIDER)) {
                Log.d("MyLogs", "Status network changed = " + status);
            }
        }
    };

    private void showLocation(Location location) {
        if (location == null)
            return;
        if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
            Log.d("MyLogs", "Location GPS = " + formatLocation(location));
        } else if (location.getProvider().equals(LocationManager.NETWORK_PROVIDER)) {
            Log.d("MyLogs", "Location network = " + formatLocation(location));
        }
        locationManager.removeUpdates(locationListener);
    }

    private String formatLocation(Location location) {
        if (location == null)
            return "";
        return String.format("Coordinates: lat = %1$.4f, lon = %2$.4f, time = %3$tF %3$tT",
                location.getLatitude(), location.getLongitude(), new Date(location.getTime()));
    }

    private void checkEnabled() {
        Log.d("MyLogs", "Enabled GPS = " + locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));
        Log.d("MyLogs", "Enabled network = " + locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
    }

    @Override
    public void onClick(View v) {
        FotosInfo fotosInfo = new FotosInfo();
        if(cursor != null){
            if(cursor.moveToFirst()){
                int id = cursor.getInt(cursor.getColumnIndex(DBHelper.KEY_ID));
                fotosInfo = dbFotoInfo.getFotosInfo(id);
                fotosInfo.setUploaded(true);
                dbFotoInfo.updateFotosInfo(fotosInfo);
            }
        }
    }
}
