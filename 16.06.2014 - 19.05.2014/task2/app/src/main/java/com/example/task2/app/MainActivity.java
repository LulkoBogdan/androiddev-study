package com.example.task2.app;

import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    public final static int ID_SERVICE = 1;
    public static boolean stop = true;

    Button btnStart;
    Button btnStop;
    NotificationManager notificationManager;
    TextView textView;
    MyService myService;
    ServiceConnection serviceConnection;
    boolean bound = false;
    static Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnStart = (Button) findViewById(R.id.buttonStart);
        btnStop = (Button) findViewById(R.id.buttonStop);
        btnStart.setOnClickListener(this);
        btnStop.setOnClickListener(this);
        textView = (TextView) findViewById(R.id.textViewCount);
        serviceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                myService = ((MyService.MyBinder) binder).getService();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name) {
                bound = false;
            }
        };
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        handler = new Handler(){
            public void handleMessage(Message msg){
                Log.d("MyLogs", "myService.getCount() = " + myService.getCount());
                textView.setText(String.valueOf(myService.getCount()));
            };
        };
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(!bound){
            return;
        }
        myService.stopSelf();
        unbindService(serviceConnection);
        bound = false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonStart:
                if(stop) {
                    Log.d("MyLogs", "click start");
                    stop = false;
                    Intent intent = new Intent(this, MyService.class);
                    bindService(intent, serviceConnection, 0);
                    startService(intent);
                }
                break;
            case R.id.buttonStop:
                Log.d("MyLogs", "click stop");
                stop = true;
                break;
        }
    }
}
