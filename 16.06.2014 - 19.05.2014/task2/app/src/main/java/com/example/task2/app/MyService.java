package com.example.task2.app;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import java.util.concurrent.TimeUnit;

/**
 * Created by androiddev9 on 19.05.14.
 */
public class MyService extends Service {
    int count = 0;
    PendingIntent pi;
    MyBinder myBinder = new MyBinder();

    NotificationCompat.Builder builder;
    NotificationManager notificationManager;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle("MyService")
                .setContentText("count = " + count);
        Intent i = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(i);
        pi = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pi);
        notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        startForeground(MainActivity.ID_SERVICE, builder.build());
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(!MainActivity.stop){
                    count++;
                    Log.d("MyLogs", "count = " + count);
                    builder.setContentText("count = " + count);
                    notificationManager.notify(MainActivity.ID_SERVICE, builder.build());
                    MainActivity.handler.sendEmptyMessage(MainActivity.ID_SERVICE);
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                stopForeground(true);
            }
        });
        thread.start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    public int getCount(){
        return count;
    }

    public class MyBinder extends Binder {
        public MyService getService() {
            return MyService.this;
        }
    }
}
