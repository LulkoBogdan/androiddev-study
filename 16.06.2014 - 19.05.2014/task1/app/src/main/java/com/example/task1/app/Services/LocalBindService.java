package com.example.task1.app.Services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by androiddev9 on 16.05.14.
 */
public class LocalBindService extends Service {

    ExecutorService es;
    MyBinder myBinder = new MyBinder();
    long res;
    boolean finish = false;

    public void onCreate() {
        super.onCreate();
        es = Executors.newFixedThreadPool(2);
    }

    @Override
    public IBinder onBind(Intent intent) {
        //count = intent.getIntExtra("data", 1);
        //Log.d("MyLogs", "Input data = " + count);
//        CalcFa(count);
        return myBinder;
    }

    public void CalcFa(int count){
        MyRun mr = new MyRun(count);
        es.execute(mr);
    }

    class MyRun implements Runnable {

        int count;

        public MyRun(int count) {
            this.count = count;
        }

        public void run() {
            long result = 1;
            for(int i = 1; i <= count; i++){
                result = result * i;
            }
            if(result < 0){
                result = 0;
            }
            res = result;
            finish = true;
        }
    }

    public long getRes(){
        Log.d("MyLogs", "result = " + res);
        return res;
    }

    public boolean isFinished(){
        return finish;
    }

    public class MyBinder extends Binder {
        public LocalBindService getService() {
            return LocalBindService.this;
        }
    }
}
