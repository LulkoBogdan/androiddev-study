package com.example.task1.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.task1.app.Services.LocalBindService;
import com.example.task1.app.Services.MyIntentService;
import com.example.task1.app.Services.PendingIntentService;

import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;


public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    final int TASK_CODE_PENDING = 1;

    public final static int STATUS_FINISH = 200;

    public final static String PARAM_DATA = "data";
    public final static String PARAM_PINTENT = "pendingIntent";
    public final static String PARAM_RESULT = "result";

    LocalBindService localBindService;
    Button btnStartPI;
    Button btnStartIS;
    Button btnStartAlarm;
    Button btnStartLBS;
    TextView tvResult;
    Intent intent;
    EditText etNumber;
    ServiceConnection serviceConnection;
    boolean bound = false;
    long resultPend;
    String[] data = {"LocalBindService", "IntentService", "Alarm", "PendingIntent"};
    Spinner spinner;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvResult = (TextView) findViewById(R.id.textViewResult);
        btnStartAlarm = (Button) findViewById(R.id.buttonStartAlarm);
        btnStartAlarm.setOnClickListener(this);
        btnStartIS = (Button) findViewById(R.id.buttonStartIntentService);
        btnStartIS.setOnClickListener(this);
        btnStartLBS = (Button) findViewById(R.id.buttonStartLocalBindService);
        btnStartLBS.setOnClickListener(this);
        btnStartPI = (Button) findViewById(R.id.buttonStartPendingIntent);
        btnStartPI.setOnClickListener(this);
        etNumber = (EditText) findViewById(R.id.editTextNumber);
        intent = new Intent(this, LocalBindService.class);//"com.example.task1.app.LocalBindService");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        serviceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                localBindService = ((LocalBindService.MyBinder) binder).getService();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name) {
                bound = false;
            }
        };
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        String buf = etNumber.getText().toString();
        try {
            int number = Integer.parseInt(buf);
            switch (v.getId()){
                case R.id.buttonStartAlarm:
                    Long time = new GregorianCalendar().getTimeInMillis()+10*1000;
                    Toast.makeText(this, "Alarm will start in 10 seconds", Toast.LENGTH_SHORT).show();
                    Intent intentAlarm = new Intent(this, AlarmReceiver.class);
                    intentAlarm.putExtra(PARAM_DATA, number);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP,time, PendingIntent.getBroadcast(this, 1, intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));
                    break;
                case R.id.buttonStartIntentService:
                    Intent intentService = new Intent(this, MyIntentService.class);
                    intentService.putExtra(PARAM_DATA, number);
                    startService(intentService);
                    break;
                case R.id.buttonStartLocalBindService:
                    localBindService.CalcFa(number);
                    while(!localBindService.isFinished()){

                    }
                    tvResult.setText(String.valueOf(localBindService.getRes()));
                    break;
                case R.id.buttonStartPendingIntent:
                    PendingIntent pi;
                    Intent intentPending = new Intent(this, PendingIntentService.class);
                    pi = createPendingResult(TASK_CODE_PENDING, intentPending, 0);
                    intentPending.putExtra(PARAM_DATA, number);
                    intentPending.putExtra(PARAM_PINTENT, pi);
                    startService(intentPending);
                    break;
            }
        } catch(NumberFormatException e){
            Toast.makeText(this, "Empty field for number", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == STATUS_FINISH) {
            long result = data.getLongExtra(PARAM_RESULT, 0);
            switch (requestCode) {
                case TASK_CODE_PENDING:
                    resultPend = result;
                    tvResult.setText(String.valueOf(resultPend));
                    if(result == 0){
                        Toast.makeText(this, "To big number, result is not correct", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(!bound){
            return;
        }
        localBindService.stopSelf();
        unbindService(serviceConnection);
        bound = false;
    }

    static public class AlarmReceiver extends BroadcastReceiver{

        public AlarmReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            int count = intent.getIntExtra(PARAM_DATA, 0);
            long result = 1;
            for(int i = 1; i <= count; i++){
                result = result * i;
            }
            if(result < 0){
                result = 0;
            }
            Toast.makeText(context, "Time is out! Alarm worked! Result = " + result, Toast.LENGTH_SHORT).show();
        }
    }

}
