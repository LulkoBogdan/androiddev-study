package com.example.task1.app.Services;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.example.task1.app.MainActivity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by androiddev9 on 16.05.14.
 */
public class PendingIntentService extends Service {
    ExecutorService es;

    public void onCreate() {
        super.onCreate();
        es = Executors.newFixedThreadPool(2);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        int count = intent.getIntExtra(MainActivity.PARAM_DATA, 1);
        PendingIntent pi = intent.getParcelableExtra(MainActivity.PARAM_PINTENT);

        MyRun mr = new MyRun(count, startId, pi);
        es.execute(mr);

        return super.onStartCommand(intent, flags, startId);
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    class MyRun implements Runnable {

        int count;
        int startId;
        PendingIntent pi;

        public MyRun(int count, int startId, PendingIntent pi) {
            this.count = count;
            this.startId = startId;
            this.pi = pi;
        }

        public void run() {
            long result = 1;
            try {
                for(int i = 1; i <= count; i++){
                    result = result * i;
                }
                if(result < 0){
                    result = 0;
                }
                Intent intent = new Intent().putExtra(MainActivity.PARAM_RESULT, result);
                pi.send(PendingIntentService.this, MainActivity.STATUS_FINISH, intent);

            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }
            stop();
        }

        void stop() {
            stopSelfResult(startId);
        }
    }
}
