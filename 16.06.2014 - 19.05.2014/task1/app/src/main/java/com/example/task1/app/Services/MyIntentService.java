package com.example.task1.app.Services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;

import com.example.task1.app.MainActivity;
import com.example.task1.app.R;

/**
 * Created by androiddev9 on 19.05.14.
 */
public class MyIntentService extends IntentService{

    NotificationManager notificationManager;

    public MyIntentService(){
        super("name");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        int count = intent.getIntExtra(MainActivity.PARAM_DATA, 0);
        long result = 1;
        for(int i = 1; i <= count; i++){
            result = result * i;
        }
        if(result < 0){
            result = 0;
        }

        Intent inten = new Intent(this, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, inten, 0);

        Notification notification = new Notification(R.drawable.ic_launcher, "Result of MyIntentService is " + result, System.currentTimeMillis());
        notification.setLatestEventInfo(this, "Result of MyIntentService work", "Result = " + result, pIntent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(1, notification);
    }
}
