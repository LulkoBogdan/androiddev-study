package com.example.task1.app.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.example.task1.app.MainActivity;
import com.example.task1.app.R;

import java.util.Random;

/**
 * Created by androiddev9 on 19.05.14.
 */
public class BootService extends Service {

    NotificationManager notificationManager;
    int count;

    public void onCreate() {
        super.onCreate();
        Log.d("MyLogs", "Something started");
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Random random = new Random();
        count = random.nextInt(30);
        long result = 1;
        for(int i = 1; i <= count; i++){
            result = result * i;
        }
        if(result < 0){
            result = 0;
        }
        Intent inten = new Intent(BootService.this, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(BootService.this, 0, inten, 0);

        Notification notification = new Notification(R.drawable.ic_launcher, "Result of BootService is " + result, System.currentTimeMillis());
        notification.setLatestEventInfo(BootService.this, "Result of BootService work", "Result = " + result, pIntent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(1, notification);
        return super.onStartCommand(intent, flags, startId);
    }
}
