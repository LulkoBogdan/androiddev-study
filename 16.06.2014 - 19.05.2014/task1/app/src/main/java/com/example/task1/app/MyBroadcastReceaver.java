package com.example.task1.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.task1.app.Services.BootService;

/**
 * Created by androiddev9 on 19.05.14.
 */
public class MyBroadcastReceaver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("MyLogs", "BroadcastReceiver works");
        Intent intentik = new Intent(context, BootService.class);
        context.startService(intentik);
    }

}
