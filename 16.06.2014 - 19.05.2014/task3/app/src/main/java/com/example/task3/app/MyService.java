package com.example.task3.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by androiddev9 on 20.05.14.
 */
public class MyService extends Service {

    public final static String KEY_RESULT = "result";
    NotificationManager mNM;
    ArrayList<Messenger> mClients = new ArrayList<Messenger>();
    int mValue = 0;
    static final int MSG_REGISTER_CLIENT = 1;
    static final int MSG_UNREGISTER_CLIENT = 2;

    static final int MSG_SET_VALUE = 3;

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    break;
                case MSG_SET_VALUE:
                    Bundle bundle = msg.getData();
                    mValue = bundle.getInt(ThirdActivity.KEY_DATA);
                    for (int i=mClients.size()-1; i>=0; i--) {
                        long result = 1;
                        for(int j = 1; j <= mValue; j++){
                            result = result * j;
                        }
                        if(result < 0){
                            result = 0;
                        }
                        try {
                            Bundle bndl = new Bundle();
                            bndl.putLong(KEY_RESULT, result);
                            Message m = Message.obtain(null, MSG_SET_VALUE);
                            m.setData(bndl);
                            mClients.get(i).send(m);
                        } catch (RemoteException e) {
                            mClients.remove(i);
                        }
                    }
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    final Messenger mMessenger = new Messenger(new IncomingHandler());

    @Override
    public void onCreate() {
        mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        showNotification();
    }

    @Override
    public void onDestroy() {
        mNM.cancel(R.string.remote_service_started);
        Toast.makeText(this, R.string.remote_service_stopped, Toast.LENGTH_SHORT).show();
    }

    @Override
    public IBinder onBind(Intent intent) {

        return mMessenger.getBinder();
    }

    private void showNotification() {
        CharSequence text = getText(R.string.remote_service_started);
        Notification notification = new Notification(R.drawable.ic_launcher, text,System.currentTimeMillis());
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,new Intent(this, MainActivity.class), 0);
        notification.setLatestEventInfo(this, getText(R.string.remote_service_label),text, contentIntent);
        mNM.notify(R.string.remote_service_started, notification);
    }

}
