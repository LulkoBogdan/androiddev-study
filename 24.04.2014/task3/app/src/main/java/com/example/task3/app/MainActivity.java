package com.example.task3.app;

import android.annotation.TargetApi;
import android.content.Loader;
import android.os.Build;
import android.support.v4.app.LoaderManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.app.LoaderManager.LoaderCallbacks;

import static java.lang.Integer.parseInt;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MainActivity extends ActionBarActivity implements LoaderCallbacks<Long>{

    static final int LOADER_CALC_ID = 1;

    TextView tvNumber;
    EditText etNumber;
    int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvNumber = (TextView) findViewById(R.id.textViewResult);
        etNumber = (EditText) findViewById(R.id.editTextNumber);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onClickStart(View v){
        Loader<Long> loader;
        try{
            count = parseInt((etNumber.getText()).toString());
        }catch(NumberFormatException e){
            Toast.makeText(this, "Write only numbers", Toast.LENGTH_SHORT);
        }
        Bundle bundle = new Bundle();
        bundle.putLong(Calculator.ARGS_LONG_NUMBER, count);
        loader = getLoaderManager().restartLoader(LOADER_CALC_ID, bundle, this);
        loader.forceLoad();
    }

    @Override
    public Loader<Long> onCreateLoader(int i, Bundle bundle) {
        Loader<Long> loader = null;
        if(i == LOADER_CALC_ID){
            loader = new Calculator(this, bundle);
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Long> longLoader, Long aLong) {
        String buf = String.valueOf(aLong);
        tvNumber.setText(buf);
    }

    @Override
    public void onLoaderReset(Loader<Long> longLoader) {

    }
}
