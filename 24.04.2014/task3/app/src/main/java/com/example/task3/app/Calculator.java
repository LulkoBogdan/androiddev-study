package com.example.task3.app;

import android.annotation.TargetApi;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

/**
 * Created by androiddev9 on 23.04.14.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Calculator extends AsyncTaskLoader<Long> {

    public final static String ARGS_LONG_NUMBER = "long_number";

    Long numberIn;
    long result;

    public Calculator(Context context, Bundle args) {
        super(context);
        numberIn = (long) 0;
        if(args != null){
            numberIn = args.getLong(ARGS_LONG_NUMBER);
        }
    }

    @Override
    public Long loadInBackground() {
        result = 1;
        for(int i = 1; i <= numberIn; i++){
            result = result * i;
        }
        if(result < 0){
            result = 0;
        }
        return result;
    }

}
