package com.example.homework.app;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.concurrent.TimeUnit;
import static java.lang.Integer.parseInt;

public class MainActivity extends Activity {

    final static int TIME_SLEEP = 1;
    static TextView tvResult;
    static EditText etNumber;
    private static int count = 1;
    private static boolean start = false;

    SimulateHardWork hardWork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvResult = (TextView) findViewById(R.id.textViewResult);
        etNumber = (EditText) findViewById(R.id.editTextNumber);
        hardWork = (SimulateHardWork) getLastNonConfigurationInstance();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClickStart(View v){
        try{
            count = parseInt((etNumber.getText()).toString());
        }catch(NumberFormatException e){
            Toast.makeText(this, "Write only numbers", Toast.LENGTH_SHORT);
        }
        if(!start){
            hardWork = new SimulateHardWork();
            hardWork.execute();
            start = true;
        }
        hardWork.link(this);
    }

    public Object onRetainNonConfigurationInstance(){
        hardWork.unLink();
        return hardWork;
    }

    static class SimulateHardWork extends AsyncTask <Void, Integer, Void>{

        MainActivity activity;

        void link(MainActivity act){
            activity = act;
        }

        void unLink(){
            activity = null;
        }

        @Override
        protected Void doInBackground(Void... objects) {
            for(int i = 0; i <= count; i++){
                try {
                    publishProgress(i);
                    TimeUnit.SECONDS.sleep(TIME_SLEEP);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            tvResult.setText("Finished");
            start = false;

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            tvResult.setText("Sleep for " + values[0] + " seconds");
        }
    }

}
