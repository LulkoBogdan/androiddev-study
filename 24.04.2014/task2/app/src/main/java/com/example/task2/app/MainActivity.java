package com.example.task2.app;

import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static java.lang.Integer.parseInt;

public class MainActivity extends ActionBarActivity {

    TextView tvNumber;
    EditText etNumber;
    int count;
    long result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvNumber = (TextView) findViewById(R.id.textViewResult);
        etNumber = (EditText) findViewById(R.id.editTextNumber);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClickStart(View v){
        result = 1;
        try{
            count = parseInt((etNumber.getText()).toString());
        }catch(NumberFormatException e){
            Toast.makeText(this, "Write only numbers", Toast.LENGTH_SHORT);
        }
        CalculateFactorial calc = new CalculateFactorial();
        calc.execute();
    }

    class CalculateFactorial extends AsyncTask<Void, Void, String>{

        @Override
        protected String doInBackground(Void... voids) {
            for(int i = 1; i <= count; i++){
                result = result * i;
            }
            if(result < 0){
                result = 0;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res){
            String buf = String.valueOf(result);
            tvNumber.setText(buf);
        }
    }

}

