package androiddev9.realcamera.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import androiddev9.realcamera.R;
import androiddev9.realcamera.activity.PreviewActivity;
import androiddev9.realcamera.common.Record;

/**
 * Created by androiddev9 on 26.06.14.
 */
public class VideoPreviewFragment extends Fragment {

    VideoView videoView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.video_preview_fragment, container, false);
        videoView = (VideoView) view.findViewById(R.id.videoViewPreview);
        Bundle bundle = getArguments();
        Record record = (Record) bundle.getSerializable(PreviewActivity.PARAM_RECORD);
        videoView.setVideoPath(record.getPath());
        videoView.start();
        return view;
    }
}
