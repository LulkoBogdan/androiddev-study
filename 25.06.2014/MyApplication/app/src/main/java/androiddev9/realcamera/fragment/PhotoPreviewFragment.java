package androiddev9.realcamera.fragment;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;

import androiddev9.realcamera.R;
import androiddev9.realcamera.activity.PreviewActivity;
import androiddev9.realcamera.common.Record;
import androiddev9.realcamera.helpers.ScreenHelper;

/**
 * Created by androiddev9 on 26.06.14.
 */
public class PhotoPreviewFragment extends Fragment {

    ImageView imageView;
    ImageLoader loader = ImageLoader.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.photo_preview_fragment, container, false);
        imageView = (ImageView) view.findViewById(R.id.imageViewPhotoPreview);
        Bundle bundle = getArguments();
        Record record = (Record) bundle.getSerializable(PreviewActivity.PARAM_RECORD);
        Point size = ScreenHelper.getScreenSize(getActivity());
        File cacheDir = StorageUtils.getCacheDirectory(getActivity());
        if(loader.isInited()){
            loader.destroy();
        }
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .discCacheExtraOptions(size.x, size.y, Bitmap.CompressFormat.JPEG, 75, null) // width, height, compress format, quality
                .threadPoolSize(5)
                .threadPriority(Thread.NORM_PRIORITY - 1)
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .denyCacheImageMultipleSizesInMemory()
                .discCache(new UnlimitedDiscCache(cacheDir))
                .imageDownloader(new BaseImageDownloader(getActivity()))
                .imageDecoder(new BaseImageDecoder(true))
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .writeDebugLogs()
                .build();
        loader.init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(android.R.drawable.ic_popup_sync)
                .showImageForEmptyUri(android.R.drawable.ic_menu_close_clear_cancel)
                .showImageOnFail(android.R.drawable.btn_dialog)
                .resetViewBeforeLoading(false)
                .cacheInMemory(false)
                .cacheOnDisc(true)
                .considerExifParams(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new SimpleBitmapDisplayer())
                .handler(new Handler())
                .build();
        String buf = "file://";
        buf = buf.concat(record.getPath());
        loader.displayImage(buf ,imageView ,options);
        return view;
    }
}
