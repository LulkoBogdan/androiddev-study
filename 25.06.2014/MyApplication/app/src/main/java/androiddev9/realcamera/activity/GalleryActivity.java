package androiddev9.realcamera.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;

import androiddev9.realcamera.R;
import androiddev9.realcamera.common.DBHelper;
import androiddev9.realcamera.common.Record;
import androiddev9.realcamera.helpers.ScreenHelper;

public class GalleryActivity extends ActionBarActivity {

    private final int CM_DELETE = 1;

    private CustomCursorAdapter customAdapter;
    GridView gridView;
    ImageLoader loader = ImageLoader.getInstance();
    DisplayImageOptions options;
    DBHelper dbGallery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.gallery_title);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        dbGallery = DBHelper.getInstance(this);
        Point size = ScreenHelper.getScreenSize(this);
        int columnCount = size.x/400;
        gridView = (GridView) findViewById(R.id.gridViewGallery);
        gridView.setNumColumns(columnCount);
        registerForContextMenu(gridView);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                new AsyncTask<Long, Void, Record>(){
                    @Override
                    protected Record doInBackground(Long... params) {
                        long id = params[0];
                        return dbGallery.getRecord(id);
                    }
                    @Override
                    protected void onPostExecute(Record record) {
                        super.onPostExecute(record);
                        Intent intent = new Intent(GalleryActivity.this, PreviewActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(PreviewActivity.PARAM_RECORD, record);
                        intent.putExtra(PreviewActivity.PARAM_RECORD, bundle);
                        startActivity(intent);
                    }
                }.execute(id);
            }
        });
        gridView.setEmptyView(findViewById(android.R.id.empty));
        File cacheDir = StorageUtils.getCacheDirectory(this);
        size = ScreenHelper.getScreenSize(this);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .discCacheExtraOptions(size.x, size.y, Bitmap.CompressFormat.JPEG, 75, null) // width, height, compress format, quality
                .threadPoolSize(5)
                .threadPriority(Thread.NORM_PRIORITY - 1)
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .denyCacheImageMultipleSizesInMemory()
                .discCache(new UnlimitedDiscCache(cacheDir))
                .imageDownloader(new BaseImageDownloader(this))
                .imageDecoder(new BaseImageDecoder(true))
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .writeDebugLogs()
                .build();
        loader.init(config);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(android.R.drawable.ic_popup_sync)
                .showImageForEmptyUri(android.R.drawable.ic_menu_close_clear_cancel)
                .showImageOnFail(android.R.drawable.btn_dialog)
                .resetViewBeforeLoading(false)
                .cacheInMemory(false)
                .cacheOnDisc(true)
                .considerExifParams(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new SimpleBitmapDisplayer())
                .handler(new Handler())
                .build();

        new AsyncTask<Void, Void, Cursor>(){
            @Override
            protected Cursor doInBackground(Void... params) {
                //return dbGallery.getAllRecords();
                return dbGallery.getAllRecordsSort();
            }
            @Override
            protected void onPostExecute(Cursor cursor) {
                super.onPostExecute(cursor);
                if(cursor != null) {
                    customAdapter = new CustomCursorAdapter(GalleryActivity.this, cursor);
                    gridView.setAdapter(customAdapter);
                }
            }
        }.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.gallery, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(android.R.id.home == id){
            onBackPressed();
            return  true;
        }
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, CM_DELETE, 0, R.string.delete);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        long id = acmi.id;
        if(item.getItemId() == CM_DELETE){
            dbGallery.deleteRecord(id);
            new AsyncTask<Void, Void, Cursor>(){
                @Override
                protected Cursor doInBackground(Void... params) {
//                    return dbGallery.getAllRecords();
                    return dbGallery.getAllRecordsSort();
                }
                @Override
                protected void onPostExecute(Cursor cursor) {
                    super.onPostExecute(cursor);
                    customAdapter.swapCursor(cursor);
                    customAdapter.notifyDataSetChanged();
                }
            }.execute();
        }
        return super.onContextItemSelected(item);
    }

    public class CustomCursorAdapter extends CursorAdapter {

        private class ViewHolder {
            public ImageView imageMain;
            public ImageView imageOverlay;
        }

        public CustomCursorAdapter(Context context, Cursor c) {
            super(context, c);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(context);
            return inflater.inflate(R.layout.grid_item, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {

            boolean type = cursor.getInt(cursor.getColumnIndex(DBHelper.KEY_TYPE))>0;
            String path = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_PATH));
            final ViewHolder holder;

            holder = new ViewHolder();
            holder.imageMain = (ImageView) view.findViewById(R.id.imageViewMain);
            holder.imageOverlay = (ImageView) view.findViewById(R.id.imageViewOverlay);
            view.setTag(holder);
            if(type) {
                String buf = "file://";
                buf = buf.concat(path);
                loader.displayImage(buf, holder.imageMain, options);
                holder.imageOverlay.setVisibility(View.INVISIBLE);
            }else{
                new AsyncTask<String, Void, Bitmap>(){

                    @Override
                    protected Bitmap doInBackground(String... params) {
                        String path = params[0];
                        return ThumbnailUtils.createVideoThumbnail(path, MediaStore.Images.Thumbnails.MINI_KIND);
                    }

                    @Override
                    protected void onPostExecute(Bitmap bitmap) {
                        super.onPostExecute(bitmap);
                        holder.imageMain.setImageBitmap(bitmap);
                        holder.imageOverlay.setVisibility(View.VISIBLE);
                    }
                }.execute(path);
            }
        }
    }
}
