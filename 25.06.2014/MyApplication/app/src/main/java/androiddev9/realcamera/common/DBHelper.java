package androiddev9.realcamera.common;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;

/**
 * Created by androiddev9 on 25.06.14.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "photo_video_base";
    public static final String TABLE_NAME = "table_photo_video";

    public static final String KEY_ID = "_id";
    public static final String KEY_PATH = "path";
    public static final String KEY_TYPE = "type";

    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + KEY_ID
            + " INTEGER PRIMARY KEY, " + KEY_PATH + " TEXT, " + KEY_TYPE + " INTEGER" + ");";

    private static DBHelper instance;
    DBHelper dbGallery;

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DBHelper getInstance(Context context){
        if(instance == null){
            instance = new DBHelper(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public long createRecord(Record record){
        dbGallery = instance;
        ContentValues contentValues = new ContentValues();
        SQLiteDatabase sqLiteDatabase = dbGallery.getWritableDatabase();
        contentValues.put(KEY_TYPE, record.isType());
        contentValues.put(KEY_PATH, record.getPath());
        return sqLiteDatabase.insert(TABLE_NAME, null, contentValues);
    }

    public Record getRecord(long record_id){
        dbGallery = instance;
        SQLiteDatabase sqLiteDatabase = dbGallery.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " WHERE " + KEY_ID + " = " + record_id;
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        Record record = new Record();
        if(cursor != null){
            if(cursor.moveToFirst()) {
                record.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                record.setType(cursor.getInt(cursor.getColumnIndex(KEY_TYPE)) > 0);
                record.setPath(cursor.getString(cursor.getColumnIndex(KEY_PATH)));
            }
        }
        return record;
    }

    public int updateRecord(Record record){
        dbGallery = instance;
        SQLiteDatabase sqLiteDatabase = dbGallery.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_PATH, record.getPath());
        contentValues.put(KEY_TYPE, record.isType());
        return sqLiteDatabase.update(TABLE_NAME, contentValues, KEY_ID + " = ?",
                new String[] { String.valueOf(record.getId()) });
    }

    public void deleteRecord(long record_id){
        AsyncDeleteRecord asyncDeleteRecord = new AsyncDeleteRecord();
        asyncDeleteRecord.execute(record_id);
    }

    public Cursor getAllRecords() {
        dbGallery = instance;
        SQLiteDatabase sqLiteDatabase = dbGallery.getReadableDatabase();
        return sqLiteDatabase.query(TABLE_NAME, null, null, null, null, null, null);
    }

    public Cursor getAllRecordsSort(){
        dbGallery = instance;
        SQLiteDatabase sqLiteDatabase = dbGallery.getReadableDatabase();
//        return sqLiteDatabase.query("DateTableName", null, null, null, null, null, "path DESC LIMIT 1");
        return sqLiteDatabase.rawQuery("SELECT * from " + TABLE_NAME + " ORDER BY path DESC" , null);
    }

    class AsyncDeleteRecord extends AsyncTask<Long, Void, Void>{
        @Override
        protected Void doInBackground(Long... params) {
            dbGallery = instance;
            long record_id = params[0];
            SQLiteDatabase sqLiteDatabase = dbGallery.getWritableDatabase();
            sqLiteDatabase.delete(TABLE_NAME, KEY_ID + " = ?", new String[]{String.valueOf(record_id)});
            return null;
        }
    }


}
