package androiddev9.realcamera.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import androiddev9.realcamera.R;
import androiddev9.realcamera.common.Record;
import androiddev9.realcamera.fragment.PhotoPreviewFragment;
import androiddev9.realcamera.fragment.VideoPreviewFragment;

public class PreviewActivity extends ActionBarActivity {

    public static final String PARAM_RECORD = "record";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.preview_title);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra(PARAM_RECORD);
        Record record = (Record) bundle.getSerializable(PARAM_RECORD);
        bundle = new Bundle();
        bundle.putSerializable(PARAM_RECORD, record);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if(record.isType()){
            PhotoPreviewFragment photoPreviewFragment = new PhotoPreviewFragment();
            photoPreviewFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.main_window, photoPreviewFragment);
        }else{
            VideoPreviewFragment videoPreviewFragment = new VideoPreviewFragment();
            videoPreviewFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.main_window, videoPreviewFragment);
        }
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.preview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
