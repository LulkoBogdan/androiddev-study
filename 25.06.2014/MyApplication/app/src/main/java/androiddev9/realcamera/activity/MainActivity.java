package androiddev9.realcamera.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.Display;
import android.view.Menu;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import androiddev9.realcamera.R;
import androiddev9.realcamera.common.DBHelper;
import androiddev9.realcamera.common.Record;

import static android.hardware.Camera.getNumberOfCameras;


public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    private final String KEY_CAMERA_ID = "camera_id";
    private final String KEY_BUTTON_FLASHLIGHT = "flashlight";

    SurfaceView cameraSurface;
    SurfaceHolder holder;
    HolderCallback holderCallback;
    Camera camera;
    MediaRecorder mediaRecorder;
    boolean record;
    boolean flashLight = false;
    int stateFlashLight = 0;
    int global;
    DBHelper dbGallery;
    ImageButton ibPhoto;
    ImageButton ibVideo;
    ImageButton ibCameraChange;
    ImageButton ibGallery;
    ImageButton ibFlashLight;
    TextView tvButton;

    File pictures;
    File photoFile;
    File videoFile;

    int CAMERA_ID = 0;
    int CAMERA_FRONT_ID = -1;
    int CAMERA_BACK_ID = -1;
    final boolean FULL_SCREEN = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        pictures = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if(TextUtils.isEmpty(pictures.getAbsolutePath())){
            pictures = Environment.getDataDirectory();
        }

        cameraSurface = (SurfaceView) findViewById(R.id.cameraSurface);

        holder = cameraSurface.getHolder();
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        holderCallback = new HolderCallback();
        holder.addCallback(holderCallback);
        dbGallery = DBHelper.getInstance(this);

        ibCameraChange = (ImageButton) findViewById(R.id.imageButtonCameraChange);
        ibGallery = (ImageButton) findViewById(R.id.imageButtonGallery);
        ibPhoto = (ImageButton) findViewById(R.id.imageButtonPhoto);
        ibVideo = (ImageButton) findViewById(R.id.imageButtonVideo);
        ibFlashLight = (ImageButton) findViewById(R.id.imageButtonLight);
        tvButton = (TextView) findViewById(R.id.textViewBTState);

        ibCameraChange.setOnClickListener(this);
        ibVideo.setOnClickListener(this);
        ibPhoto.setOnClickListener(this);
        ibGallery.setOnClickListener(this);
        ibFlashLight.setOnClickListener(this);
        if(!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)){
            ibCameraChange.setVisibility(View.INVISIBLE);
            ibCameraChange.setEnabled(false);
        }else{
            int countOfCameras = getNumberOfCameras();
            for (int i = 0; i < countOfCameras; i++){
                Camera.CameraInfo info = new Camera.CameraInfo();
                Camera.getCameraInfo(i, info);
                if(CAMERA_FRONT_ID == -1 && info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT){
                    CAMERA_FRONT_ID = i;
                }else if(CAMERA_BACK_ID == -1 && info.facing == Camera.CameraInfo.CAMERA_FACING_BACK){
                    CAMERA_BACK_ID = i;
                }
            }
            CAMERA_ID = CAMERA_BACK_ID;
            if(CAMERA_FRONT_ID == -1){
                ibCameraChange.setVisibility(View.INVISIBLE);
                ibCameraChange.setEnabled(false);
            }
        }
        if(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH) || hasFlash()){
            flashLight = true;
            ibFlashLight.setVisibility(View.VISIBLE);
        }else{
            flashLight = false;
            ibFlashLight.setVisibility(View.INVISIBLE);
            tvButton.setVisibility(View.INVISIBLE);
            ibFlashLight.setEnabled(false);
        }
        MyOrientationListener myOrientationListener = new MyOrientationListener(this);
        myOrientationListener.enable();
    }

    @Override
    protected void onResume() {
        super.onResume();
        camera = Camera.open(CAMERA_ID);
        if(!flashLight){
            if(hasFlash()){
                flashLight = true;
                ibFlashLight.setVisibility(View.VISIBLE);
                tvButton.setVisibility(View.VISIBLE);
                ibFlashLight.setEnabled(true);
            }
        }
        cameraSurface.setVisibility(View.INVISIBLE);
        cameraSurface.setVisibility(View.VISIBLE);
        setPreviewSize(FULL_SCREEN);
        if(flashLight) {
            if(stateFlashLight == 0){
                Camera.Parameters p = camera.getParameters();
                p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                camera.setParameters(p);
                ibFlashLight.setImageResource(android.R.drawable.btn_star_big_off);
                tvButton.setText(R.string.flashlight_off);
            }else if(stateFlashLight == 1){
                ibFlashLight.setImageResource(android.R.drawable.btn_star_big_on);
                tvButton.setText(R.string.flashlight_on);
            }else if(stateFlashLight == 2){
                Camera.Parameters p = camera.getParameters();
                p.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                camera.setParameters(p);
                ibFlashLight.setImageResource(android.R.drawable.btn_star_big_on);
                tvButton.setText(R.string.flashlight_auto);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseMediaRecorder();
        if (camera != null)
            camera.release();
        camera = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        stopRecord();
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageButtonCameraChange:
                stopRecord();
                if(CAMERA_FRONT_ID != -1 && CAMERA_ID == CAMERA_BACK_ID){
                    CAMERA_ID = CAMERA_FRONT_ID;
                }else if(CAMERA_ID == CAMERA_FRONT_ID){
                    CAMERA_ID = CAMERA_BACK_ID;
                }
                camera.release();
                camera = Camera.open(CAMERA_ID);
                cameraSurface.setVisibility(View.INVISIBLE);
                cameraSurface.setVisibility(View.VISIBLE);
                setPreviewSize(FULL_SCREEN);
                break;
            case R.id.imageButtonGallery:
                stopRecord();
                Intent intent = new Intent(this, GalleryActivity.class);
                startActivity(intent);
                break;
            case R.id.imageButtonPhoto:
                stopRecord();
                if(flashLight && stateFlashLight == 1){
                    Camera.Parameters p = camera.getParameters();
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    camera.setParameters(p);
                }
                camera.takePicture(null, null, new Camera.PictureCallback() {
                    @Override
                    public void onPictureTaken(byte[] data, Camera camera) {
                        new AsyncTask<byte[], Void, Void>(){
                            @Override
                            protected Void doInBackground(byte[]... params) {
                                byte[] data = params[0];
                                try {
                                    Record rec = new Record();
                                    photoFile = new File(pictures, "photo_" + nameGenerator() + ".jpg");
                                    rec.setPath(photoFile.getPath());
                                    rec.setType(true);
                                    dbGallery.createRecord(rec);
                                    FileOutputStream fos = new FileOutputStream(photoFile.getPath());
                                    fos.write(data);
                                    fos.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }
                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);
                                Toast.makeText(MainActivity.this, R.string.photo_created ,Toast.LENGTH_SHORT).show();
                            }
                        }.execute(data);
                        if(flashLight && stateFlashLight == 1){
                            Camera.Parameters p = camera.getParameters();
                            p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                            camera.setParameters(p);
                        }
                        camera.startPreview();
                    }
                });
                break;
            case R.id.imageButtonVideo:
                record = !record;
                if(record) {
                    Record rec = new Record();
                    rec.setType(false);
                    ibVideo.setImageResource(android.R.drawable.presence_video_busy);
                    videoFile = new File(pictures, "video_" + nameGenerator() + ".3gp");
                    rec.setPath(videoFile.getPath());
                    new AsyncTask<Record, Void, Void>(){
                        @Override
                        protected Void doInBackground(Record... params) {
                            Record rec = params[0];
                            dbGallery.createRecord(rec);
                            return null;
                        }
                    }.execute(rec);
                    if (prepareVideoRecorder()) {
                        mediaRecorder.start();
                    } else {
                        releaseMediaRecorder();
                    }
                }else{
                    if (mediaRecorder != null) {
                        mediaRecorder.stop();
                        releaseMediaRecorder();
                        Toast.makeText(MainActivity.this, R.string.video_created ,Toast.LENGTH_SHORT).show();
                    }
                    ibVideo.setImageResource(android.R.drawable.presence_video_online);
                }
                break;
            case R.id.imageButtonLight:

                if(stateFlashLight == 0){
                    stateFlashLight = 1;
                    ibFlashLight.setImageResource(android.R.drawable.btn_star_big_on);
                    tvButton.setText(R.string.flashlight_on);
                }else if(stateFlashLight == 1){
                    stateFlashLight = 2;
                    ibFlashLight.setImageResource(android.R.drawable.btn_star_big_on);
                    tvButton.setText(R.string.flashlight_auto);
                    Camera.Parameters p = camera.getParameters();
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                    camera.setParameters(p);
                }else if(stateFlashLight == 2){
                    stateFlashLight = 0;
                    tvButton.setText(R.string.flashlight_off);
                    ibFlashLight.setImageResource(android.R.drawable.btn_star_big_off);
                    Camera.Parameters p = camera.getParameters();
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    camera.setParameters(p);
                }
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_CAMERA_ID, CAMERA_ID);
        outState.putInt(KEY_BUTTON_FLASHLIGHT, stateFlashLight);
        stopRecord();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        CAMERA_ID = savedInstanceState.getInt(KEY_CAMERA_ID);
        stateFlashLight = savedInstanceState.getInt(KEY_BUTTON_FLASHLIGHT);
    }

    class HolderCallback implements SurfaceHolder.Callback {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width,int height) {
            camera.stopPreview();
            setCameraDisplayOrientation(CAMERA_ID);
            try {
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
        }
    }

    void setPreviewSize(boolean fullScreen) {
        Display display = getWindowManager().getDefaultDisplay();
        boolean widthIsMax = display.getWidth() > display.getHeight();
        Camera.Size size = camera.getParameters().getPreviewSize();
        RectF rectDisplay = new RectF();
        RectF rectPreview = new RectF();
        rectDisplay.set(0, 0, display.getWidth(), display.getHeight());
        if (widthIsMax) {
            rectPreview.set(0, 0, size.width, size.height);
        } else {
            rectPreview.set(0, 0, size.height, size.width);
        }
        Matrix matrix = new Matrix();
        if (!fullScreen) {
            matrix.setRectToRect(rectPreview, rectDisplay,Matrix.ScaleToFit.START);
        } else {
            matrix.setRectToRect(rectDisplay, rectPreview,Matrix.ScaleToFit.START);
            matrix.invert(matrix);
        }
        matrix.mapRect(rectPreview);
        cameraSurface.getLayoutParams().height = (int) (rectPreview.bottom);
        cameraSurface.getLayoutParams().width = (int) (rectPreview.right);
    }

    void setCameraDisplayOrientation(int cameraId) {
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result = 0;
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
            result = ((360 - degrees) + info.orientation);
        } else
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = ((360 - degrees) - info.orientation);
                result += 360;
            }
        result = result % 360;
        camera.setDisplayOrientation(result);
    }

    private String nameGenerator(){
        long time = System.currentTimeMillis();
        return String.valueOf(time);
    }

    private boolean prepareVideoRecorder() {
        camera.unlock();

        mediaRecorder = new MediaRecorder();
        mediaRecorder.setOrientationHint(global);
        if(CAMERA_ID == CAMERA_BACK_ID) {
            mediaRecorder.setCamera(camera);
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
            mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
            mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
            mediaRecorder.setOutputFile(videoFile.getAbsolutePath());
            mediaRecorder.setPreviewDisplay(cameraSurface.getHolder().getSurface());
        }else if(CAMERA_ID == CAMERA_FRONT_ID){
            mediaRecorder.setCamera(camera);
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
            mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
            mediaRecorder.setProfile(CamcorderProfile.get(CAMERA_FRONT_ID, CamcorderProfile.QUALITY_HIGH));
            mediaRecorder.setOutputFile(videoFile.getAbsolutePath());
            mediaRecorder.setPreviewDisplay(cameraSurface.getHolder().getSurface());
        }

        try {
            mediaRecorder.prepare();
        } catch (Exception e) {
            e.printStackTrace();
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.reset();
            mediaRecorder.release();
            mediaRecorder = null;
            camera.lock();
        }
    }

    private void stopRecord(){
        if(record){
            record = !record;
            if (mediaRecorder != null) {
                mediaRecorder.stop();
                releaseMediaRecorder();
                Toast.makeText(MainActivity.this, R.string.video_created ,Toast.LENGTH_SHORT).show();
                ibVideo.setImageResource(android.R.drawable.presence_video_online);
            }
        }
    }

    public boolean hasFlash() {
        if (camera == null) {
            return false;
        }
        Camera.Parameters parameters = camera.getParameters();
        if (parameters.getFlashMode() == null) {
            return false;
        }
        List<String> supportedFlashModes = parameters.getSupportedFlashModes();
        if (supportedFlashModes == null || supportedFlashModes.isEmpty() || supportedFlashModes.size() == 1 && supportedFlashModes.get(0).equals(Camera.Parameters.FLASH_MODE_OFF)) {
            return false;
        }
        return true;
    }

    private class MyOrientationListener extends OrientationEventListener{

        public MyOrientationListener(Context context) {
            super(context);
        }

        @Override
        public void onOrientationChanged(int orientation) {
            if (orientation == ORIENTATION_UNKNOWN) return;
            android.hardware.Camera.CameraInfo info =
                    new android.hardware.Camera.CameraInfo();
            android.hardware.Camera.getCameraInfo(CAMERA_ID, info);
            orientation = (orientation + 45) / 90 * 90;
            int rotation = 0;
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                rotation = (info.orientation - orientation + 360) % 360;
            } else {  // back-facing camera
                rotation = (info.orientation + orientation) % 360;
            }
            if(camera != null) {
                Camera.Parameters mParameters = camera.getParameters();
                global = rotation;
                mParameters.setRotation(rotation);
                camera.setParameters(mParameters);
            }
        }
    }
}
