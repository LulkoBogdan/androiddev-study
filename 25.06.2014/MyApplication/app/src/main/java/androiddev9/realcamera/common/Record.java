package androiddev9.realcamera.common;

import java.io.Serializable;

/**
 * Created by androiddev9 on 25.06.14.
 */
public class Record implements Serializable{
    private String path;
    private boolean type;
    private int id;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
